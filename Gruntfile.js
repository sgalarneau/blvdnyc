module.exports = function(grunt) {
	// require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
	var path = require('path');
	var root = path.normalize(__dirname);
	grunt.initConfig({
		less : {
			development : {
				options : {
					paths : ["less"]
				},
				files : {
					'./css/styles.css' : "./less/boot.less"
				}
			}
		},
		uglify : {
			my_target : {
				files : {
					'js/blvd.min.js' : ["js/vendor/filter-stabilize.js", "js/vendor/partition.js", "js/vendor/jquery.animateSprite.min.js", "js/vendor/jquery.animateSprite.jm.js", "js/vendor/fastclick.js", "js/debounce.js", "js/animdata.js", "js/blvd.annotate.js"]
				}
			},
		},
		watch : {
			less : {
				files : ['./less/*.less', './less/*/*.less'],
				tasks : ['less', 'cssmin'],
			},
			livereload : {
				files : ['./css/styles.css', './css/styles.min.css'],
				options : {
					livereload : 35729,
					nospawn : true,
				},
			},
		},
		cssmin : {
			minify : {
				expand : true,
				cwd : './css/',
				src : ['*.css', '!*.min.css'],
				dest : './css',
				ext : '.min.css'
			}
		},
		ngAnnotate : {
			options : {
				singleQuotes : true,
			},
			app1 : {
				files : {
					'js/blvd.annotate.js' : ["js/app.js", "js/api.js", "js/skipreload.js", "js/controllers.js", "js/directives.js", "js/translations.js"],
				},
			},
		},
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-ng-annotate');

	grunt.registerTask('build', ['ngAnnotate', 'uglify', 'less', 'cssmin']);

};