<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','RequestHandler');


    
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('logout','login');
    }

/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $slug
 * @return void
 */
	public function admin_view($slug = null) {
		$options = array('conditions' => array('User.slug'=>$slug));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			/*if($this->request->data['User']['photo']['error']!=4 &&
                        (
                        $this->request->data['User']['photo']['type']=='image/jpg' ||
                        $this->request->data['User']['photo']['type']=='image/jpeg' ||
                        $this->request->data['User']['photo']['type']=='image/gif' ||
                        $this->request->data['User']['photo']['type']=='image/png' ||
                        $this->request->data['User']['photo']['type']=='image/pjpeg'
                        )
            ){
                App::import('vendor', 'imageResize', array('file' =>'imageResize.class.php'));
                $image = new SimpleImage(); 
                $image->load($this->request->data['User']['photo']['tmp_name']);
                
                
                //300x169
                if($image->getWidth() != 300){
                    $image->resizeToWidth(300);
                }
                $rand_name = md5(uniqid(rand(), true));
                $image->save('img/u/'.$rand_name.'.jpg');
                $this->request->data['User']['picture'] = $rand_name.'.jpg';
            }*/
			
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($slug = null) {
		
		if ($this->request->is(array('post', 'put'))) {
			    
            /*if(isset($this->request->data['User']['photo']) && $this->request->data['User']['photo']['error']!=4 &&
                        (
                        $this->request->data['User']['photo']['type']=='image/jpg' ||
                        $this->request->data['User']['photo']['type']=='image/jpeg' ||
                        $this->request->data['User']['photo']['type']=='image/gif' ||
                        $this->request->data['User']['photo']['type']=='image/png' ||
                        $this->request->data['User']['photo']['type']=='image/pjpeg'
                        )
            ){
                App::import('vendor', 'imageResize', array('file' =>'imageResize.class.php'));
                $image = new SimpleImage(); 
                $image->load($this->request->data['User']['photo']['tmp_name']);
                
                //300x169
                if($image->getWidth() != 300){
                    $image->resizeToWidth(300);
                }
                $rand_name = md5(uniqid(rand(), true));
                $image->save('img/u/'.$rand_name.'.jpg');
                $this->request->data['User']['picture'] = $rand_name.'.jpg';
              
                $old_picture = $this->User->field('picture');
                if($old_picture!=''){ @unlink('img/u/'.$old_picture); }
                
            }*/   
                           
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
		    $options = array('conditions' => array('User.slug'=> $slug));
            $user = $this->User->find('first', $options);
		    if (!$user) {
                throw new NotFoundException(__('Invalid user'));
            }
			$this->request->data = $user;
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * Login
 */
    public function login() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirect());
            }
            $this->Session->setFlash(__('Invalid username or password, try again'));
        }
    }
    
/**
 * Logout
 */
    public function logout() {
        $this->Auth->logout();
        return $this->redirect('/');
    }

}
