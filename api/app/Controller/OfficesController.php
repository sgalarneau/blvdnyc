<?php
App::uses('AppController', 'Controller');
/**
 * Offices Controller
 *
 * @property Office $Office
 * @property PaginatorComponent $Paginator
 */
class OfficesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','RequestHandler');


/**
 * index method
 *
 * @return void
 */
    public function index() {
        $this->Office->recursive = 0;
        $options = array(
            'fields' =>array(
                'name_eng',
                'name_fre',
                'id'
            ),
            'contain'=>array(
                'Artist.name',
                'Artist.slug'
            )
        );
        $this->set('offices', $this->Office->find('all',$options));
    }



/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Office->recursive = 0;
		$this->set('offices', $this->Office->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Office->exists($id)) {
			throw new NotFoundException(__('Invalid office'));
		}
		$options = array('conditions' => array('Office.' . $this->Office->primaryKey => $id));
		$this->set('office', $this->Office->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Office->create();
			if ($this->Office->save($this->request->data)) {
				$this->Session->setFlash(__('The office has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The office could not be saved. Please, try again.'));
			}
		}
		$artists = $this->Office->Artist->find('list');
		$projects = $this->Office->Project->find('list');
		$this->set(compact('artists', 'projects'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Office->exists($id)) {
			throw new NotFoundException(__('Invalid office'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Office->save($this->request->data)) {
				$this->Session->setFlash(__('The office has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The office could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Office.' . $this->Office->primaryKey => $id));
			$this->request->data = $this->Office->find('first', $options);
		}
		$artists = $this->Office->Artist->find('list');
		$projects = $this->Office->Project->find('list');
		$this->set(compact('artists', 'projects'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Office->id = $id;
		if (!$this->Office->exists()) {
			throw new NotFoundException(__('Invalid office'));
		}
		if ($this->Office->delete()) {
			$this->Session->setFlash(__('The office has been deleted.'));
		} else {
			$this->Session->setFlash(__('The office could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * redefines order within the offices
 */
    public function admin_order(){
        foreach($this->request->query as $officeId=>$newOrder):
            $this->Office->id = $officeId;
            $this->Office->saveField('order',$newOrder); 
        endforeach;
        
        $this->autoRender = false;
    }

}
