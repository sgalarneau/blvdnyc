<?php
App::uses('AppController', 'Controller');
/**
 * Projects Controller
 *
 * @property Project $Project
 * @property PaginatorComponent $Paginator
 */
class ProjectsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','RequestHandler','Vimeo');
    
    
/**
 * index method
 *
 * @return void
 */
    public function index() {
        $page = 1;
        $limit = 7;
        $news = true;
        $only_new = false;
        
        if(isset($this->request->query['page'])){
            $page = $this->request->query['page'];
        }
        if(isset($this->request->query['limit'])){
            $limit = $this->request->query['limit'];
        }
        //debug($this->request->query['news']);
        if(isset($this->request->query['news'])){
            if($this->request->query['news']=="false"){
                $news = false;
            }
        }
        
        if(isset($this->request->query['only_new'])){
            if($this->request->query['only_new']=="true"){
                $only_new = true;
            }
        }
        
        
        $this->Project->recursive = 0;
        
        
        $conditions = array();
        
        //ONLY PROJECT that are not only for artists
        $conditions['AND'] = array('Project.only_on_artists_page'=>'0');
        if(!$news){
            $conditions["AND"]["Project.isNews"] = 0;
        }
        if($only_new){
            $conditions["AND"]["Project.new"] = 1;
        }
        $conditions['OR'] = array();
        
        $contain = array(
            'Client'=>array("fields"=>array("id","name")),
            'Agency'=>array("fields"=>array("id","name")),
            'Category'=>array("fields"=>array("id","name_eng","name_fre"))
        );
        
        if(isset($this->request->query['artists'])){
            $this->Project->bindModel(array('hasOne' => array('ProjectsArtists')), false);
            array_push($conditions["OR"],array('ProjectsArtists.artist_id'=>$this->request->query['artists']));
            array_push($contain,'ProjectsArtists.artist_id');
        }
        if(isset($this->request->query['offices'])){
            $this->Project->bindModel(array('hasOne' => array('ProjectsOffices')), false);
            array_push($conditions["OR"],array('ProjectsOffices.office_id'=>$this->request->query['offices']));
            array_push($contain,'ProjectsOffices.office_id'); 
        }
        if(isset($this->request->query['categories'])){
            $this->Project->bindModel(array('hasOne' => array('ProjectsCategories')), false);
            array_push($conditions["OR"],array('ProjectsCategories.category_id'=>$this->request->query['categories']));
            array_push($contain,'ProjectsCategories.category_id');   
        }
                
        $this->Paginator->settings['limit'] = $limit;
        $this->Paginator->settings['page'] = $page;
        $this->Paginator->settings['group'] = array('Project.id'); //important to destroy duplicates in the return
        $this->Paginator->settings['contain'] = $contain;
        $this->Paginator->settings['fields'] = array(
                'name_eng','name_fre',
                'thumbnail_url',
                'background_url',
                'headline_eng','headline_fre',
                'description_eng','description_fre',
                'awards_eng','awards_fre',
                'credits_eng','credits_fre',
                'vimeo_link_eng',
                'vimeo_link_fre',
                'date',
                'vimeo_staff_pick',
                'the_mill',
                'mp4_url',
                'ogv_url',
                'mp4',
                'ogv',
                'new',
                'isNews',
                'slug'
            );
        
        $projects = $this->Paginator->paginate($conditions);
         
         
        //transformer for news !
        foreach($projects as $key => $field){
                
            if($projects[$key]['Project']['mp4']==''){
                unset($projects[$key]['Project']['mp4_url']);
            }
            if($projects[$key]['Project']['ogv']==''){
                unset($projects[$key]['Project']['ogv_url']);
            }
            unset($projects[$key]['Project']['mp4']);
            unset($projects[$key]['Project']['ogv']);
                
            if($projects[$key]['Project']['isNews']){
                unset($projects[$key]['Client']);
                unset($projects[$key]['Agency']);
                unset($projects[$key]['Category']);
                $projects[$key]['News']['name_eng'] = $projects[$key]['Project']['name_eng'];
                $projects[$key]['News']['name_fre'] = $projects[$key]['Project']['name_fre'];
                $projects[$key]['News']['description_eng'] = $projects[$key]['Project']['description_eng'];
                $projects[$key]['News']['description_fre'] = $projects[$key]['Project']['description_fre'];
                $projects[$key]['News']['thumbnail_url'] = $projects[$key]['Project']['thumbnail_url'];
                $projects[$key]['News']['date'] = $projects[$key]['Project']['date'];
                 
                unset($projects[$key]['Project']);   
            }else{
                unset($projects[$key]['Project']['isNews']); 
            }
            
            
            
        } 
        //end transformer
                
        $this->set('projects', $projects);
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($slug = null) {
        $options = array(
            'conditions' => array('Project.slug'=>$slug),
            'fields'=>array(
                'name_eng','name_fre',
                'thumbnail_url',
                'background_url',
                'headline_eng','headline_fre',
                'description_eng','description_fre',
                'awards_eng','awards_fre',
                'credits_eng','credits_fre',
                'vimeo_link_eng',
                'vimeo_link_fre',
                'vimeo_staff_pick',
                'the_mill',
                'new',
                'date',
                'isNews',
                'slug'
            ),
            'contain'=>array(
                'Client.name',
                'Agency.name',
                'ProjectImage.image_square_small',
                'ProjectImage.image_square_large',
                'ProjectImage.image_wide',
                'ProjectImage.size',
                'Category.name_eng',
                'Category.name_fre',
                'Artist.name',
                'Artist.slug',
                'Office.name_eng',
                'Office.name_fre'
                
            )
        );
        
        $project = $this->Project->find('first', $options);
        
        if($project['Project']['isNews'] == 1){
            throw new NotFoundException(__('Invalid project'));
        }else{
            unset($project['Project']['isNews']);
            foreach($project['ProjectImage'] as $key => $field){
                if($field['size']=='square'){
                    unset($project['ProjectImage'][$key]['image_wide']);
                }elseif($field['size']=='wide'){
                    unset($project['ProjectImage'][$key]['image_square_small']);
                    unset($project['ProjectImage'][$key]['image_square_large']);
                }
            }
                   
        }
        $this->set('project', $project);
    }


/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$conditions = array('Project.isNews'=>'0');
		$this->Project->recursive = 0;
		$this->set('projects', $this->Paginator->paginate($conditions));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
		$this->set('project', $this->Project->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {            
			$this->Project->create();
			
            if($this->request->data['Project']['thumbImg']!='' && $this->request->data['Project']['photo']['error']!=4 &&
                        (
                        $this->request->data['Project']['photo']['type']=='image/jpg' ||
                        $this->request->data['Project']['photo']['type']=='image/jpeg' ||
                        $this->request->data['Project']['photo']['type']=='image/gif' ||
                        $this->request->data['Project']['photo']['type']=='image/png' ||
                        $this->request->data['Project']['photo']['type']=='image/pjpeg'
                        )
                        
            ){
                App::import('vendor', 'imageResize', array('file' =>'imageResize.class.php'));
                $image = new SimpleImage(); 
                $image->load($this->request->data['Project']['photo']['tmp_name']);
                
                
                $image->resize(1500,538);
                
                $rand_name = md5(uniqid(rand(), true));
                $image->save(APP.'webroot/img/p/background_'.$rand_name.'.jpg');
                $this->request->data['Project']['picture'] = $rand_name.'.jpg';
                
                
                //Thumbnail
                $rand_name_thumb = md5(uniqid(rand(), true));
                $thumbnail = new SimpleImage(); 
                $thumbnail->load($this->request->data['Project']['thumbImg']);
                $thumbnail->crop($this->request->data['Project']['x1'],$this->request->data['Project']['y1'],$this->request->data['Project']['x2'],$this->request->data['Project']['y2']);
                $thumbnail->resize(716,716);
                $thumbnail->save(APP.'webroot/img/p/'.$rand_name_thumb.'.jpg');
                $this->request->data['Project']['thumbnail'] = $rand_name_thumb.'.jpg';
                               
                
                /*if($this->request->data['Project']['vimeo_link']){
                    $videoInfo = $this->Vimeo->getVimeoInfo($this->request->data['Project']['vimeo_link']);
                    $this->request->data['Project']['vimeo_image'] = $videoInfo['thumbnail_large'];
                }*/
                
                if ($this->Project->save($this->request->data)) {
                    $this->Session->setFlash(__('The project has been saved.'));
                    return $this->redirect(array('action' => 'edit',$this->Project->getLastInsertId()));
                } else {
                    @unlink(APP.'webroot/img/p/'.$rand_name_thumb.'.jpg');    
                    @unlink(APP.'webroot/img/p/background_'.$rand_name.'.jpg');   
                    $this->Session->setFlash(__('The project could not be saved. Please, try again.'));
                }
                
            }else{
                $this->Session->setFlash(__('Invalid image for background or invalid thumbnail'));
            }
           
		}
        $artists = $this->Project->Artist->find('list',array('fields'=>array('id','name')));
		$clients = $this->Project->Client->find('list',array('order'=>'name ASC'));
        $agencies = $this->Project->Agency->find('list',array('order'=>'name ASC'));
		$this->set(compact('clients','artists','agencies'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Project->exists($id)) {
			throw new NotFoundException(__('Invalid project'));
		}
		if ($this->request->is(array('post', 'put'))) {
		   
           $image_valid = true;
           //debug($this->request->data['Project']);
           if($this->request->data['Project']['photo']['error']!=4 &&
                        (
                        $this->request->data['Project']['photo']['type']=='image/jpg' ||
                        $this->request->data['Project']['photo']['type']=='image/jpeg' ||
                        $this->request->data['Project']['photo']['type']=='image/gif' ||
                        $this->request->data['Project']['photo']['type']=='image/png' ||
                        $this->request->data['Project']['photo']['type']=='image/pjpeg'
                        )
            ){
                App::import('vendor', 'imageResize', array('file' =>'imageResize.class.php'));
                $image = new SimpleImage(); 
                $image->load($this->request->data['Project']['photo']['tmp_name']);
                                     
                
                $image->resize(1500,538);
                
                $rand_name = md5(uniqid(rand(), true));
                $image->save('img/p/background_'.$rand_name.'.jpg');
                
                $this->Project->id = $id;
                $filename = $this->Project->field('picture');
                @unlink(APP.'webroot/img/p/background_'.$filename); 
                
                
                $this->request->data['Project']['picture'] = $rand_name.'.jpg';
            }
      
            $this->Project->id = $id;
            if($this->request->data['Project']['mp4_file']['error']!=4){   
                $filenamemp4 = $this->Project->field('mp4');
                if($filenamemp4!=''){
                    @unlink(APP.'webroot/img/p/'.$filenamemp4);
                }
                
                $tmp_name = $this->request->data['Project']['mp4_file']["tmp_name"];
                $rand_name_mp4 = md5(uniqid(rand(), true));
                move_uploaded_file($tmp_name, APP.'webroot/img/p/'.$rand_name_mp4.'.mp4');
                $this->request->data['Project']['mp4'] = $rand_name_mp4.'.mp4';
            }
              
            if($this->request->data['Project']['ogv_file']['error']!=4){
                $filenameogv = $this->Project->field('ogv');
                if($filenameogv!=''){
                    @unlink(APP.'webroot/img/p/'.$filenameogv);
                }
                
                $tmp_name = $this->request->data['Project']['ogv_file']["tmp_name"];
                $rand_name_ogv = md5(uniqid(rand(), true));
                move_uploaded_file($tmp_name, APP.'webroot/img/p/'.$rand_name_ogv.'.ogv');
                $this->request->data['Project']['ogv'] = $rand_name_ogv.'.ogv';
            }
            
			/*if($this->request->data['Project']['vimeo_link']){
                $videoInfo = $this->Vimeo->getVimeoInfo($this->request->data['Project']['vimeo_link']);
                $this->request->data['Project']['vimeo_image'] = $videoInfo['thumbnail_large'];
            } */
                
           
			if ($this->Project->save($this->request->data)) {
			 
			    $this->Session->setFlash(__('The project has been saved.'));
            
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project could not be saved. Please, try again.'));
                $this->request->data = $this->Project->find('first',array('conditions'=>array('Project.id'=>$id)));
			}
		} else {
			$options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
			$this->request->data = $this->Project->find('first', $options);
		}
		$clients = $this->Project->Client->find('list');
        $categories = $this->Project->Category->find('list',array('fields'=>array('id','name')));
        $artists = $this->Project->Artist->find('list',array('fields'=>array('id','name')));
        $offices = $this->Project->Office->find('list',array('fields'=>array('id','name_eng')));
        $agencies = $this->Project->Agency->find('list',array('order'=>'name ASC'));
		$this->set(compact('clients','categories','artists','offices','agencies'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Project->id = $id;
		if (!$this->Project->exists()) {
			throw new NotFoundException(__('Invalid project'));
		}
        
        $options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
        $project = $this->Project->find('first', $options);
        
		if ($this->Project->delete()) {
			$this->Project->ProjectImage->deleteAll(array('ProjectImage.project_id'=>$id));
            foreach($project['ProjectImage'] as $image){
                @unlink(APP.'webroot/img/p/'.$image["filename"]);    
                @unlink(APP.'webroot/img/p/small_'.$image["filename"]);    
                @unlink(APP.'webroot/img/p/large_'.$image["filename"]);    
                @unlink(APP.'webroot/img/p/original_'.$image["filename"]); 
                @unlink(APP.'webroot/img/p/thumbnail_'.$image["filename"]); 
            }
            @unlink(APP.'webroot/img/p/background_'.$project['Project']["picture"]); 
            @unlink(APP.'webroot/img/p/'.$project['Project']["thumbnail"]);
            @unlink(APP.'webroot/img/p/'.$project['Project']["mp4"]); 
            @unlink(APP.'webroot/img/p/'.$project['Project']["ogv"]); 
                
			$this->Session->setFlash(__('The project has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}


/**
 * redefines order of images in project
 */
    public function admin_order($id = null){
        if (!$this->Project->exists($id)) {
            throw new NotFoundException(__('Invalid project'));
        }
        $options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
        $project = $this->Project->find('first', $options);
        
        foreach($this->request->query as $imageId=>$newOrder):
            $this->Project->ProjectImage->id = $imageId;
            $this->Project->ProjectImage->saveField('order',$newOrder); 
        endforeach;
        
        $this->autoRender = false;
          
    }
    
    
/**
 * Edit thumbnails page
 */    
    public function admin_edit_thumbnail($id = null) {
        if (!$this->Project->exists($id)) {
            throw new NotFoundException(__('Invalid project'));
        }
        
        if ($this->request->is(array('post', 'put'))) {
            $this->Project->id = $id;
            $old_thumb = $this->Project->field('thumbnail');
            
            if($this->request->data['Project']['thumbImg']!=''){
                
                App::import('vendor', 'imageResize', array('file' =>'imageResize.class.php'));
                //Thumbnail
                $rand_name_thumb = md5(uniqid(rand(), true));
                $thumbnail = new SimpleImage(); 
                $thumbnail->load($this->request->data['Project']['thumbImg']);
                $thumbnail->crop($this->request->data['Project']['x1'],$this->request->data['Project']['y1'],$this->request->data['Project']['x2'],$this->request->data['Project']['y2']);
                $thumbnail->resize(716,716);
                $thumbnail->save(APP.'webroot/img/p/'.$rand_name_thumb.'.jpg');
                $this->Project->saveField('thumbnail',$rand_name_thumb.'.jpg');
                
                @unlink(APP.'webroot/img/p/'.$old_thumb); 
            
                $this->Session->setFlash(__('Thumbnail has been edited sucessfully'));
                return $this->redirect(array('controller'=>'projects','action' => 'edit',$id));
                
            }else{
                $this->Session->setFlash(__('Invalid thumbnail'));
            }
            
        }
        
        $this->set('project_id',$id);
        
    }




    // public function admin_news(){
        // $conditions = array('Project.isNews'=>'1');
        // $this->Project->recursive = 0;
        // $this->set('projects', $this->Paginator->paginate($conditions));
    // }
//     
//     
    // public function admin_add_news(){
        // if ($this->request->is('post')) {            
            // $this->Project->create();
//             
            // if($this->request->data['Project']['thumbImg']!=''){
                // App::import('vendor', 'imageResize', array('file' =>'imageResize.class.php'));
                // //Thumbnail
                // $rand_name_thumb = md5(uniqid(rand(), true));
                // $thumbnail = new SimpleImage(); 
                // $thumbnail->load($this->request->data['Project']['thumbImg']);
                // $thumbnail->crop($this->request->data['Project']['x1'],$this->request->data['Project']['y1'],$this->request->data['Project']['x2'],$this->request->data['Project']['y2']);
                // $thumbnail->resize(716,716);
                // $thumbnail->save(APP.'webroot/img/p/'.$rand_name_thumb.'.jpg');
                // $this->request->data['Project']['thumbnail'] = $rand_name_thumb.'.jpg';
//                                
                // $this->request->data['Project']['isNews'] = 1;
//                 
                // if ($this->Project->save($this->request->data)) {
                    // $this->Session->setFlash(__('The news has been saved.'));
                    // return $this->redirect(array('action' => 'news'));
                // } else {
                    // $this->Session->setFlash(__('The news could not be saved. Please, try again.'));
                // }
//                 
            // }else{
                // $this->Session->setFlash(__('Invalid image for thumbnail'));
            // }
//            
        // }
    // }

/**
 * Edit news
 */
    // public function admin_edit_news($id = null) {
        // if (!$this->Project->exists($id)) {
            // throw new NotFoundException(__('Invalid news'));
        // }
        // if ($this->request->is(array('post', 'put'))) {
// 
             // if($this->request->data['Project']['thumbImg']!=''){
                // $this->Project->id = $id;
                // $old_thumb = $this->Project->field('thumbnail');
//                 
                // App::import('vendor', 'imageResize', array('file' =>'imageResize.class.php'));
                // //Thumbnail
                // $rand_name_thumb = md5(uniqid(rand(), true));
                // $thumbnail = new SimpleImage(); 
                // $thumbnail->load($this->request->data['Project']['thumbImg']);
                // $thumbnail->crop($this->request->data['Project']['x1'],$this->request->data['Project']['y1'],$this->request->data['Project']['x2'],$this->request->data['Project']['y2']);
                // $thumbnail->resize(716,716);
                // $thumbnail->save(APP.'webroot/img/p/'.$rand_name_thumb.'.jpg');
//                 
                // $this->request->data['Project']['thumbnail'] = $rand_name_thumb.'.jpg';
//                 
                // @unlink(APP.'webroot/img/p/'.$old_thumb);
            // }
//             
//             
            // if ($this->Project->save($this->request->data)) {
//              
                // $this->Session->setFlash(__('The news has been saved.'));
//             
                // return $this->redirect(array('action' => 'news'));
            // } else {
                // $this->Session->setFlash(__('The new could not be saved. Please, try again.'));
                // $this->request->data = $this->Project->find('first',array('conditions'=>array('Project.id'=>$id)));
            // }
        // } else {
            // $options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id));
            // $this->request->data = $this->Project->find('first', $options);
        // }
//         
    // }
// 
// 
    // public function admin_delete_news($id = null) {
        // $this->Project->id = $id;
        // if (!$this->Project->exists()) {
            // throw new NotFoundException(__('Invalid news'));
        // }
//         
        // $options = array('conditions' => array('Project.' . $this->Project->primaryKey => $id,'Project.isNews'=>1));
        // $project = $this->Project->find('first', $options);
//         
        // if ($this->Project->delete()) {
//             
            // @unlink(APP.'webroot/img/p/'.$project['Project']["thumbnail"]); 
//                 
            // $this->Session->setFlash(__('The news has been deleted.'));
        // } else {
            // $this->Session->setFlash(__('The news could not be deleted. Please, try again.'));
        // }
        // return $this->redirect(array('action' => 'news'));
    // }

}

