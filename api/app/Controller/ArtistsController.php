<?php
App::uses('AppController', 'Controller');
/**
 * Artists Controller
 *
 * @property Artist $Artist
 * @property PaginatorComponent $Paginator
 */
class ArtistsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','RequestHandler');


/**
 * index method
 *
 * @return void
 */
    public function index() {
        $this->Artist->recursive = 0;
        $this->set('artists', $this->Artist->find('all',array('fields'=>array(
            'id',
            'name',
            'slug'
        ))));
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
    public function view($slug = null) {
        $this->Artist->recursive = 0;
        $options = array(
            'conditions' => array('Artist.slug'=>$slug),
            'fields'=>array(
                'name',
                'description_eng','description_fre',
                'slug',
                'id'
            ),
            'contain'=>array(
                'Project.name_eng',
                'Project.name_fre',
                'Project.thumbnail_url',
                'Project.vimeo_link_eng',
                'Project.vimeo_link_fre',
                'Project.vimeo_staff_pick',
                'Project.id',
                'Project.slug',
                'Project.Category'=>array('fields'=>array('name_fre','name_eng')),
                'Project.Client',
                'Project.Agency',
                'Project.mp4',
                'Project.ogv',
                'Project.mp4_url',
                'Project.ogv_url',
                'Office.name_eng',
                'Office.name_fre'
            )
        );
        
        
        $artist = $this->Artist->find('first', $options);

        foreach($artist['Project'] as $key => $field){
            if($artist['Project'][$key]['mp4']==''){
                unset($artist['Project'][$key]['mp4_url']);
            }
            if($artist['Project'][$key]['ogv']==''){
                unset($artist['Project'][$key]['ogv_url']);
            }
    
            unset($artist['Project'][$key]['ogv']);
            unset($artist['Project'][$key]['mp4']);
        }

        $this->set('artist', $artist);
    }





/**
 * index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Artist->recursive = 0;
		$this->set('artists', $this->Artist->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Artist->exists($id)) {
			throw new NotFoundException(__('Invalid artist'));
		}
		$options = array('conditions' => array('Artist.' . $this->Artist->primaryKey => $id));
		$this->set('artist', $this->Artist->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Artist->create();
            $this->request->data["Office"]["Office"][0]= '1';
			if ($this->Artist->save($this->request->data)) {
				$this->Session->setFlash(__('The artist has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The artist could not be saved. Please, try again.'));
			}
		}
        $offices = $this->Artist->Office->find('list',array('fields'=>array('id','name_eng')));
        $this->set(compact('offices'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Artist->exists($id)) {
			throw new NotFoundException(__('Invalid artist'));
		}
		if ($this->request->is(array('post', 'put'))) {
		    $this->request->data["Office"]["Office"][0]= '1';
			if ($this->Artist->save($this->request->data)) {
				$this->Session->setFlash(__('The artist has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The artist could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Artist.' . $this->Artist->primaryKey => $id));
			$this->request->data = $this->Artist->find('first', $options);
		}
        
        $offices = $this->Artist->Office->find('list',array('fields'=>array('id','name_eng')));
        $this->set(compact('offices'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Artist->id = $id;
		if (!$this->Artist->exists()) {
			throw new NotFoundException(__('Invalid artist'));
		}
		if ($this->Artist->delete()) {                
			$this->Session->setFlash(__('The artist has been deleted.'));
		} else {
			$this->Session->setFlash(__('The artist could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
    
/**
 * redefines order within the artists
 */
    public function admin_order(){
        foreach($this->request->query as $artistId=>$newOrder):
            $this->Artist->id = $artistId;
            $this->Artist->saveField('order',$newOrder); 
        endforeach;
        
        $this->autoRender = false;
    }
}
