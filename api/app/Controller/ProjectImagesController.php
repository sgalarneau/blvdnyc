<?php
App::uses('AppController', 'Controller');
/**
 * ProjectImages Controller
 *
 * @property ProjectImage $ProjectImage
 * @property PaginatorComponent $Paginator
 */
class ProjectImagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','RequestHandler');


/**
 * add method
 *
 * @return void
 */
	public function admin_add($projectId=null) {
		
		if ($this->request->is('post')) {
			
			$this->ProjectImage->create();
            
		    
            App::import('vendor', 'imageResize', array('file' =>'imageResize.class.php'));
            
            
            $rand_name = md5(uniqid(rand(), true));
            
        //original
            $image = new SimpleImage(); 
            $image->load($this->request->data['ProjectImage']['img']);
            $image->save(APP.'webroot/img/p/original_'.$rand_name.'.jpg');
            if($this->request->data['ProjectImage']['size']=="square"){
                $image->crop($this->request->data['ProjectImage']['x1'],$this->request->data['ProjectImage']['y1'],$this->request->data['ProjectImage']['x2'],$this->request->data['ProjectImage']['y2']);
                $image->resize(716,716);
                $image->save(APP.'webroot/img/p/large_'.$rand_name.'.jpg');
                
                $image->resize(470,470);
                $image->save(APP.'webroot/img/p/small_'.$rand_name.'.jpg');
            }else if($this->request->data['ProjectImage']['size']=="wide"){
                    
                $image->crop($this->request->data['ProjectImage']['x1'],$this->request->data['ProjectImage']['y1'],$this->request->data['ProjectImage']['x2'],$this->request->data['ProjectImage']['y2']);
                $image->resize(960,470);
                $image->save(APP.'webroot/img/p/'.$rand_name.'.jpg');
            }
            $image->saveThumbnail(APP.'webroot/img/p/thumbnail_'.$rand_name.'.jpg',100,100);   
            
            
            
            $this->request->data['ProjectImage']['filename'] = $rand_name.'.jpg';
            
            if ($this->ProjectImage->save($this->request->data)) {
                $this->Session->setFlash(__('The project image has been saved.'));
                return $this->redirect(array('controller'=>'projects','action' => 'edit',$this->request->data['ProjectImage']['project_id']));
            } else {
                $this->Session->setFlash(__('The project image could not be saved. Please, try again.'));
            }
        
        
            
		}else{
		    $this->set('project_id',$projectId);
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	/*public function admin_edit($id = null) {
		if (!$this->ProjectImage->exists($id)) {
			throw new NotFoundException(__('Invalid project image'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->ProjectImage->save($this->request->data)) {
				$this->Session->setFlash(__('The project image has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The project image could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('ProjectImage.' . $this->ProjectImage->primaryKey => $id));
			$this->request->data = $this->ProjectImage->find('first', $options);
		}
		$projects = $this->ProjectImage->Project->find('list');
		$this->set(compact('projects'));
	}*/

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->ProjectImage->id = $id;
		if (!$this->ProjectImage->exists()) {
			throw new NotFoundException(__('Invalid project image'));
		}
        $filename = $this->ProjectImage->field('filename');
        $project_id = $this->ProjectImage->field('project_id');
		if ($this->ProjectImage->delete()) {
		  	
            @unlink(APP.'webroot/img/p/'.$filename);    
            @unlink(APP.'webroot/img/p/small_'.$filename);    
            @unlink(APP.'webroot/img/p/large_'.$filename);    
            @unlink(APP.'webroot/img/p/original_'.$filename); 
            @unlink(APP.'webroot/img/p/thumbnail_'.$filename); 
                
		  	$this->Session->setFlash(__('The project image has been deleted.'));
		} else {
			$this->Session->setFlash(__('The project image could not be deleted. Please, try again.'));
		}
		$this->redirect(array('controller'=>'projects','action' => 'edit',$project_id));
	}

}

