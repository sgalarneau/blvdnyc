<?php
App::uses('AppController', 'Controller');
class OtherPagesController extends AppController {
        
    /**
     * Components
     *
     * @var array
     */
        public $components = array('Paginator','RequestHandler');
        
        
    public function admin_index() {
        $this->OtherPage->recursive = 0;
        $this->set('pages', $this->OtherPage->find('all'));
    }
    
    
    public function admin_edit($slug=null) {
        $this_page = $this->OtherPage->find('first',array('conditions'=>array('OtherPage.slug'=>$slug)));
        if ($this->request->is(array('post', 'put'))) {
            $this->OtherPage->id = $this_page['OtherPage']['id'];    
            // if(isset($this->request->data['OtherPage']['mp4_file']) && $this->request->data['OtherPage']['mp4_file']['error']!=4){
//                  
                // $filenamemp4 = $this->OtherPage->field('home_mp4_video');
                // if($filenamemp4!=''){
                    // @unlink(APP.'webroot/files/'.$filenamemp4);
                // }
//                 
                // $tmp_name = $this->request->data['OtherPage']['mp4_file']["tmp_name"];
                // $rand_name_mp4 = md5(uniqid(rand(), true));
                // move_uploaded_file($tmp_name, APP.'webroot/files/'.$rand_name_mp4.'.mp4');
                // $this->request->data['OtherPage']['home_mp4_video'] = $rand_name_mp4.'.mp4';
            // }
//             
            // if(isset($this->request->data['OtherPage']['ogv_file']) && $this->request->data['OtherPage']['ogv_file']['error']!=4){
//                
                // $filenameogv = $this->OtherPage->field('home_ogv_video');
                // if($filenameogv!=''){
                    // @unlink(APP.'webroot/files/'.$filenameogv);
                // }
//                 
                // $tmp_name = $this->request->data['OtherPage']['ogv_file']["tmp_name"];
                // $rand_name_ogv = md5(uniqid(rand(), true));
                // move_uploaded_file($tmp_name, APP.'webroot/files/'.$rand_name_ogv.'.ogv');
                // $this->request->data['OtherPage']['home_ogv_video'] = $rand_name_ogv.'.ogv';
            // }
//             
            // if(isset($this->request->data['OtherPage']['background_image']) && $this->request->data['OtherPage']['background_image']['error']!=4){
                // $filenamebackground = $this->OtherPage->field('home_background');
                // if($filenamebackground!=''){
                    // @unlink(APP.'webroot/files/'.$filenamebackground);
                // }    
//                     
                // App::import('vendor', 'imageResize', array('file' =>'imageResize.class.php'));
                // $image = new SimpleImage(); 
                // $image->load($this->request->data['OtherPage']['background_image']['tmp_name']);
//                 
                // $rand_name_background = md5(uniqid(rand(), true));
                // $image->save(APP.'webroot/files/background_'.$rand_name_background.'.jpg');
                // $this->request->data['OtherPage']['home_background'] = 'background_'.$rand_name_background.'.jpg';
            // }
            
                
            if ($this->OtherPage->save($this->request->data)) {
                 
                $this->Session->setFlash(__('The page has been saved.'));
            
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The page could not be saved. Please, try again.'));
                $this->request->data = $this_page;
            }  
        } else {
            $this->request->data = $this_page;
        }
    }
    
    public function view($slug = null) {
         if(isset($slug)){    
             $page = $this->OtherPage->find('first',array('conditions'=>array('OtherPage.slug'=>$slug)));
             if($page){
                 if($page['OtherPage']['slug']=='home'){
                     $page['home']['home_headline_eng'] = $page['OtherPage']['home_headline_eng'];
                     $page['home']['home_headline_fre'] = $page['OtherPage']['home_headline_fre'];
                     $page['home']['mp4_url'] = $page['OtherPage']['mp4_url'];
                     $page['home']['ogv_url'] = $page['OtherPage']['ogv_url'];
                     $page['home']['background_url'] = $page['OtherPage']['background_url'];
                     unset($page['OtherPage']);
                 }else if($page['OtherPage']['slug']=='interstate'){
                     $page['blvd']['content_text_eng'] = $page['OtherPage']['content_text_eng'];
                     $page['blvd']['content_text_fre'] = $page['OtherPage']['content_text_fre'];
                     unset($page['OtherPage']);
                 }else if($page['OtherPage']['slug']=='contact'){
                     $page['contact']['content_text_eng'] = $page['OtherPage']['content_text_eng'];
                     $page['contact']['content_text_fre'] = $page['OtherPage']['content_text_fre'];
                     unset($page['OtherPage']);
                 }
             
             
             
                $this->set('page', $page);
             }else{
                 throw new NotFoundException(__('Invalid page'));
             }
         }
    }
    
}