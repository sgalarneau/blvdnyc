<?php

class VimeoComponent extends Component {
   
	/**
	 * @param object $id
	 * @return 
	 * $VideoInfo = getVimeoInfo($id);
	 * $thumb = $VideoInfo['thumbnail_medium'];
	 * $tags = $VideoInfo ['tags'];
	 * $longdesc = $VideoInfo ['description']
	 */
	function getVimeoInfo($id) {
				if (!function_exists('curl_init')) die('CURL is not installed!');
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, "http://vimeo.com/api/v2/video/$id.php");
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_TIMEOUT, 10);
				$output = unserialize(curl_exec($ch));
				$output = $output[0];
				curl_close($ch);
				return $output;
	}
}

?>