<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        'Cookie',
        'Session',
        'Auth'
    );
    
    //set an alias for the newly created helper: Html<->MyHtml
    public $helpers = array(
        'Html' => array('className' => 'MyHtml'),
        'Session'
    );
    
    public function beforeFilter() {
        
        //debug($this->params);
        
        //if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
            $this->layout = 'admin';
        //}
        
        
        /**
         * Authientication json allow
         */
        $this->Auth->allow('index','view');
            
        /**
         * Languages
         */    
       /* $this->_setLanguage();
        $locale = Configure::read('Config.language');
           
        if ($locale && file_exists('VIEWS' . $locale . DS . $this->viewPath)) {
            // e.g. use /app/View/fre/Pages/tos.ctp instead of /app/View/Pages/tos.ctp
            $this->viewPath = $locale . DS . $this->viewPath;
        }*/
        
        $this->Auth->loginRedirect = array( 'controller' => 'projects', 'action' => 'index','admin'=>true);
        $this->Auth->loginAction = array( 'controller'=>'users', 'action'=>'login'/*, 'language'=>$this->Session->read('Config.language')*/,'admin'=>false);
        
    }
    
    
    
    private function _setLanguage() {
        //if the cookie was previously set, and Config.language has not been set
        //write the Config.language with the value from the Cookie
        if ($this->Cookie->read('lang') && !$this->Session->check('Config.language')) {
            $this->Session->write('Config.language', $this->Cookie->read('lang'));
        }
        //if the user clicked the language URL
        else if (isset($this->params['language']) &&
                    ($this->params['language'] !=  $this->Session->read('Config.language'))
        ) {
           //then update the value in Session and the one in Cookie
           $this->Session->write('Config.language', $this->params['language']);
           $this->Cookie->write('lang', $this->params['language'], false, '20 days');
        }else if($this->Session->check('Config.language')){
            Configure::write('Config.language', $this->Session->read('Config.language'));
        }
        
        //if redirect on '/' then will be redirect to according language
        if (empty($this->request->params['language']) && !$this->params['admin'] && $this->params['ext']!='json'){
            $defaultLanguageCode = Configure::read('Config.language');
            $newURL = '/' . $defaultLanguageCode;
            $this->redirect($newURL);
        }
    }
    
    
    //override redirect
    /*public function redirect( $url, $status = NULL, $exit = true ) {
        if (!isset($url['language']) && $this->Session->check('Config.language')) {
            $url['language'] = $this->Session->read('Config.language');
        }
        parent::redirect($url,$status,$exit);
    }*/
    
    
}

