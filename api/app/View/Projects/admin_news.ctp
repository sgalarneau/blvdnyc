<script type="text/javascript">
    $(function(){
        $(document).tooltip({
            track:true,
            content: function () {
                return $(this).prop('title');
            }
        });
    });    
</script>



<div class="projects index">
	<h2><?php echo __('News'); ?></h2>
	
	<?php echo $this->Html->link(__('Add News'), array('action' => 'add_news'),array('class'=>'pure-button pure-button-primary')); ?>
	
	<br />
	<br />
	
	<table cellpadding="0" cellspacing="0" class="pure-table pure-table-striped">
	<thead>
	<tr>
			
			<th><?php echo $this->Paginator->sort('name_eng'); ?></th>
			<th><?php echo $this->Paginator->sort('name_fre'); ?></th>
			<th>Thumbnail</th>
			<th><?php echo __('Description Eng'); ?></th>
			<th><?php echo __('Description Fre'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projects as $project): ?>
	<tr>
		
		<td><?php echo h($project['Project']['name_eng']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['name_fre']); ?>&nbsp;</td>
        <td><img width="100" src="<?php echo $this->Html->url('/'); ?>img/p/<?php echo $project['Project']['thumbnail']; ?>" /></td>
		<td title="<?php echo h($project['Project']['description_eng']); ?>"><?php echo substr($project['Project']['description_eng'],0,120); ?>...</td>
		<td title="<?php echo h($project['Project']['description_fre']); ?>"><?php echo substr($project['Project']['description_fre'],0,120); ?>...</td>
		
		<td class="actions">
			<?php //echo $this->Html->link(__('View'), array('action' => 'view', $project['Project']['id'])); ?><br />
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit_news', $project['Project']['id'])); ?><br />
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete_news', $project['Project']['id']), array(), __('Are you sure you want to delete %s?', $project['Project']['name_eng']."/".$project['Project']['name_fre'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous').' ', array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(' '.__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
