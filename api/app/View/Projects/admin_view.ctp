<div class="projects view">
<h2><?php echo __('Project'); ?></h2>

    <div class="pure-g">
        <div class="pure-u-1-2">
        	<dl>
        		<dt><?php echo __('Id'); ?></dt>
        		<dd>
        			<?php echo h($project['Project']['id']); ?>
        			&nbsp;
        		</dd>
        		<dt><?php echo __('Client'); ?></dt>
        		<dd>
        			<?php echo $this->Html->link($project['Client']['name'], array('controller' => 'clients', 'action' => 'view', $project['Client']['id'])); ?>
        			&nbsp;
        		</dd>
        		<!-- <dt><?php echo __('Agency'); ?></dt>
                <dd>
                    <?php echo $this->Html->link($project['Agency']['name'], array('controller' => 'agencies', 'action' => 'view', $project['Agency']['id'])); ?>
                    &nbsp;
                </dd> -->
        		<dt><?php echo __('Name'); ?></dt>
        		<dd>
        			<?php echo h($project['Project']['name_eng']); ?>
        			&nbsp;
        		</dd>
        		<!-- <dt><?php echo __('Name Fre'); ?></dt>
        		<dd>
        			<?php echo h($project['Project']['name_fre']); ?>
        			&nbsp;
        		</dd> -->
        		<dt><?php echo __('Headline'); ?></dt>
        		<dd>
        			<?php echo h($project['Project']['headline_eng']); ?>
        			&nbsp;
        		</dd>
        		<!-- <dt><?php echo __('Headline Fre'); ?></dt>
        		<dd>
        			<?php echo h($project['Project']['headline_fre']); ?>
        			&nbsp;
        		</dd> -->
        		<dt><?php echo __('Description'); ?></dt>
        		<dd>
        			<?php echo html_entity_decode($project['Project']['description_eng']); ?>
        			&nbsp;
        		</dd>
        		<!-- <dt><?php echo __('Description Fre'); ?></dt>
        		<dd>
        			<?php echo html_entity_decode($project['Project']['description_fre']); ?>
        			&nbsp;
        		</dd> -->
        		<dt><?php echo __('Date'); ?></dt>
                <dd>
                    <?php echo date('d/m/y',$project['Project']['date']/1000); ?>
                    &nbsp;
                </dd>
        		<!-- <dt><?php echo __('Awards Eng'); ?></dt>
                <dd>
                    <?php echo html_entity_decode($project['Project']['awards_eng']); ?>
                    &nbsp;
                </dd>
                <dt><?php echo __('Awards Fre'); ?></dt>
                <dd>
                    <?php echo html_entity_decode($project['Project']['awards_fre']); ?>
                    &nbsp;
                </dd>
                <dt><?php echo __('Credits Eng'); ?></dt>
                <dd>
                    <?php echo html_entity_decode($project['Project']['credits_eng']); ?>
                    &nbsp;
                </dd>
                <dt><?php echo __('Credits Fre'); ?></dt>
                <dd>
                    <?php echo html_entity_decode($project['Project']['credits_fre']); ?>
                    &nbsp;
                </dd> -->
        		
        		<dt><?php echo __('Modified'); ?></dt>
        		<dd>
        			<?php echo h($project['Project']['modified']); ?>
        			&nbsp;
        		</dd>
        		<dt><?php echo __('Created'); ?></dt>
        		<dd>
        			<?php echo h($project['Project']['created']); ?>
        			&nbsp;
        		</dd>
        	</dl>
        </div>
        <div class="pure-u-1-2">
            <dl>
                <dt><?php echo __('Vimeo Link Eng'); ?></dt>
                <dd>
                    <?php echo $this->Vimeo->getEmbedCode($project['Project']['vimeo_link_eng'],array('height'=>'300px;')); ?>
                </dd>
                
                
                
                <dt><?php echo __('Background'); ?></dt>
                <dd>
                    <img width="400" src="<?php echo $this->Html->url('/'); ?>img/p/background_<?php echo $project['Project']['picture']; ?>" />
                </dd>
            </dl> 
        </div>
    </div>
</div>

<div class="related">
	<h3><?php echo __('Related Project Images'); ?></h3>
	<?php if (!empty($project['ProjectImage'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="pure-table pure-table-striped">
    <thead>
    	<tr>
    		<th>&nbsp;</th>
    	    <!--th><?php echo __('Name Eng'); ?></th>
    		<th><?php echo __('Name Fre'); ?></th>
    		<th><?php echo __('Description Eng'); ?></th>
    		<th><?php echo __('Description Fre'); ?></th>
    		<th><?php echo __('Created'); ?></th-->
    	</tr>
    </thead>
	<?php foreach ($project['ProjectImage'] as $projectImage): ?>
		<tr>
			<td><img src="<?php echo $this->Html->url('/'); ?>img/p/thumbnail_<?php echo $projectImage['filename']; ?>" /></td>
			<!--td><?php echo $projectImage['name_eng']; ?></td>
			<td><?php echo $projectImage['name_fre']; ?></td>
			<td><?php echo $projectImage['description_eng']; ?></td>
			<td><?php echo $projectImage['description_fre']; ?></td>
			
			<td><?php echo $projectImage['created']; ?></td-->
		</tr>
	<?php endforeach; ?>
	</table>
    <?php endif; ?>
</div>

<div class="related">
	<h3><?php echo __('Related Categories'); ?></h3>
	<?php if (!empty($project['Category'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="pure-table pure-table-striped">
    <thead>	
    	<tr>
    		<th><?php echo __('Name'); ?></th>
    		<!-- <th><?php echo __('Name Fre'); ?></th> -->
    		<th><?php echo __('Modified'); ?></th>
    		<th><?php echo __('Created'); ?></th>
    	</tr>
    </thead>
	<?php foreach ($project['Category'] as $category): ?>
		<tr>
			<td><?php echo $category['name_eng']; ?></td>
			<!-- <td><?php echo $category['name_fre']; ?></td> -->
			<td><?php echo $category['modified']; ?></td>
			<td><?php echo $category['created']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
    <?php endif; ?>
</div>

<div class="related">
	<h3><?php echo __('Related Directors'); ?></h3>
	<?php if (!empty($project['Artist'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="pure-table pure-table-striped">
    <thead>
    	<tr>
    		<th><?php echo __('Name'); ?></th>
    		<th><?php echo __('Description'); ?></th>
    		<!-- <th><?php echo __('Description Fre'); ?></th> -->
    		<th><?php echo __('Modified'); ?></th>
    		<th><?php echo __('Created'); ?></th>
    	</tr>
    </thead>
	<?php foreach ($project['Artist'] as $artist): ?>
		<tr>
			<td><?php echo $artist['name']; ?></td>
			<td><?php echo $artist['description_eng']; ?></td>
			<!-- <td><?php echo $artist['description_fre']; ?></td> -->
			<td><?php echo $artist['modified']; ?></td>
			<td><?php echo $artist['created']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
    <?php endif; ?>
</div>

<!-- <div class="related">
	<h3><?php echo __('Related Offices'); ?></h3>
	<?php if (!empty($project['Office'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="pure-table pure-table-striped">
    <thead>
    	<tr>
    		<th><?php echo __('Name Eng'); ?></th>
    		<th><?php echo __('Name Fre'); ?></th>
    		<th><?php echo __('Modified'); ?></th>
    		<th><?php echo __('Created'); ?></th>
    	</tr>
    </thead>
	<?php foreach ($project['Office'] as $office): ?>
		<tr>
			<td><?php echo $office['name_eng']; ?></td>
			<td><?php echo $office['name_fre']; ?></td>
			<td><?php echo $office['modified']; ?></td>
			<td><?php echo $office['created']; ?></td>
		</tr>
	<?php endforeach; ?>
	</table>
    <?php endif; ?>
</div> -->