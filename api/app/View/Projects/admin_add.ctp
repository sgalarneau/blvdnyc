<div class="projects form">
    <?php echo $this->Form->create('Project',array('type'=>'file',"class"=>'pure-form pure-form-stacked')); ?>
	<h2><?php echo __('Add Project'); ?><!-- &nbsp;<small><a id="help" href="#">help ?</a></small> --></h2>
	
	
	
	<?php
	echo $this->Form->input('client_id',array("empty"=>true,"required"=>false));
    ?>
    <small id="addClientBtn" style="text-decoration: underline; cursor:pointer;" class="red">Add Client</small>
    <!-- <?php 
    echo $this->Form->input('agency_id',array('div'=>array('class'=>'pure-control-group'),"empty"=>true,"required"=>false));
    ?>
    <small id="addAgencyBtn" style="text-decoration: underline; cursor:pointer;" class="red">Add Agency</small> -->
    <?php 
	echo $this->Form->input('name_eng',array('label'=>'Name'));
	//echo $this->Form->input('name_fre');
    
    echo $this->Form->input('photo',array('validate'=>'notEmpty','type'=>'file','label'=>'Background image'));
    ?>
    <small><em>( Background must be 1500x538 or will be stretch )</em></small>
    <br />
    <br />
    
    
    
    
    <label>Project Thumbnail</label>
    <div id="imageError" style="color:#ca0813;font-weight:bold;"></div>
    <div id="holder" style="border:5px dashed #ccc; width:400px; height:150px; background:#eeeeee;"><img src="" style="display:none;" /></div>
    <small>( Drop image here )</small>
    <br />
    

    <small id="status">File API & FileReader API not supported on this browser use a modern browser... ... yes, do it !</small>
    <?php echo $this->Form->input('x1',array('type'=>'hidden')); ?>
    <?php echo $this->Form->input('x2',array('type'=>'hidden')); ?>
    <?php echo $this->Form->input('y1',array('type'=>'hidden')); ?>
    <?php echo $this->Form->input('y2',array('type'=>'hidden')); ?>
    <?php echo $this->Form->input('thumbImg',array('type'=>'hidden')); ?>
    <br />
    <br />
    
    
    
    <?php
    echo $this->Form->input('headline_eng',array('label'=>'Headline'));
    // echo $this->Form->input('headline_fre');
    echo $this->Form->input('dte',array('label'=>'Date','class'=>'datepicker','div'=>array('class'=>'pure-control-group')));
    ?>
    <small><em>( Note that projects will be ordered by date )</em></small>
    <br />
    <br />
    <?php
    echo $this->Form->input('date',array('type'=>'hidden'));
	echo $this->Form->input('description_eng',array('label'=>'Description'));
	// echo $this->Form->input('description_fre');
	echo $this->Form->input('vimeo_link_eng',array('label'=>__('Vimeo Id')));
    // echo $this->Form->input('vimeo_link_fre',array('label'=>__('Vimeo Id Fre')));
    ?>
        <br />
        <?php
        echo $this->Form->input('vimeo_staff_pick');
        echo $this->Form->input('the_mill',array('type'=>'checkbox'));
        echo $this->Form->input('new',array('label'=>'New Project (featured on home page)'));
    ?>  
    <h2>Directors</h2>
    <?php
    echo $this->Form->input('Artist',array('label'=>'Directors<br />'));
    echo $this->Form->input('only_on_artists_page',array('label'=>'Only Directors page'));
    ?>
    
    <small>( If checked, this project won't show on the <em>Projects</em> page, it will only show in the director portfolio )</small>
    <br />
    <br />
    
    <?php
    echo $this->Form->submit(__('Add and Edit Project'),array('class'=>'pure-button pure-button-primary')); 

    echo $this->Form->end();
    ?>
</div>

<div id="addClient" title="Add Client" class="clients form">
<?php echo $this->Form->create('Client',array('controller'=>'clients','action'=>'add','class'=>'pure-form pure-form-stacked')); ?>
    <h2><?php echo __('Add Client'); ?></h2>
    <?php
        echo $this->Form->input('name');
        echo $this->Form->input('redirect',array('type'=>'hidden','value'=>'projects'));
    ?>
    <br />
    <?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<div id="addAgency" title="Add Agency" class="agencies form">
<?php echo $this->Form->create('Agency',array('controller'=>'Agencies','action'=>'add','class'=>'pure-form pure-form-stacked')); ?>
    <h2><?php echo __('Add Agency'); ?></h2>
    <?php
        echo $this->Form->input('name');
        echo $this->Form->input('redirect',array('type'=>'hidden','value'=>'projects'));
    ?>
    <br />
    <?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<!-- <div id="helpDialog">
    <img src="<?php echo $this->Html->url('/',true); ?>img/project_example.jpg" />
</div> -->


<script type="text/javascript">
$(function() {
    var dialog = $( "#addClient" ).dialog({
        autoOpen: false,
        height: 200,
        width: 250,
        modal:true
    });
    
    $( "#addClientBtn" ).on( "click", function() {
        dialog.dialog( "open" );
    });
    
    
    var dialogAgency = $( "#addAgency" ).dialog({
        autoOpen: false,
        height: 200,
        width: 250,
        modal:true
    });
    $( "#addAgencyBtn" ).on( "click", function() {
        dialogAgency.dialog( "open" );
    });
    
    $("#ProjectClientId,#ArtistArtist,#ProjectAgencyId").select2({
        width:'400px'
    });
    
    $("#ProjectDte").datepicker({
        dateFormat: "dd/mm/yy",
        altFormat: '@',
        altField:$('#ProjectDate')
    });
    
    var myDate = new Date();
    var currentDay = myDate.getDate();
    var currentMonth = myDate.getMonth();
    var currentYear = myDate.getFullYear();
    
    var dateToShow = new Date(currentYear,currentMonth,currentDay);
    $('#ProjectDte').datepicker('setDate', dateToShow);
     
     
     
     
     
    //help dialog
    var dialogHelp = $( "#helpDialog" ).dialog({
        autoOpen: false,
        height: 1184,
        width: 670,
        modal:true
    });
    $( "#help" ).on( "click", function() {
        dialogHelp.dialog( "open" );
    });
     
     
     
    //project thumbnail 
     
    var square = {width:716,height:716};
        
    var holder = document.getElementById('holder'),
        state = document.getElementById('status');
    
    if (typeof window.FileReader === 'undefined') {
      state.className = 'fail';
    } else {
      state.className = 'success';
      state.innerHTML = '';
    }
     
    holder.ondragover = function () { this.className = 'hover'; return false; };
    holder.ondragend = function () { this.className = ''; return false; };
    holder.ondrop = function (e) {
        this.className = '';
        e.preventDefault();
    
        var file = e.dataTransfer.files[0],
        reader = new FileReader();
        reader.onload = function (event) {
            
            
            var image = new Image();
            var imageType = /image.*/;
            if(event.target.result.match(imageType)){
                image.src = event.target.result;
                
                image.onload = function() {
                    // access image size here 
                    //console.log(this.width);
                    //console.log(this);
                    var imgLoaded = this;
                    
                  
                    
                  
                    if(imgLoaded.height<716){
                       $("#imageError").text('Image too small');   
                    }else{             
                        $("#imageError").text('');
                        var xOne,xTwo,yOne,yTwo,minWidth,minHeight;
                    
                        xOne = (imgLoaded.width/2) - (square.width/2);
                        yOne = (imgLoaded.height/2) - (square.height/2);
                        xTwo = xOne + square.width;
                        yTwo = yOne + square.height;
                        minWidth = square.width;
                        minHeight = square.height;
                    
                        
                        $('#holder img').attr('src', imgLoaded.src);
                        $("#holder").height(imgLoaded.height);
                        $("#holder").width(imgLoaded.width);
                        $('#holder img').show();
                        
                        var imgArea = $('#holder img').imgAreaSelect({
                            handles: true,
                            x1: xOne, y1: yOne, x2: xTwo, y2: yTwo,
                            minWidth:minWidth,
                            minHeight:minHeight,
                            aspectRatio: minWidth+":"+minHeight,
                            instance: true,
                            onSelectEnd: function(img, selection){
                                updateCoordinates(imgLoaded,selection);
                            }
                        });
                        
                        updateCoordinates(imgLoaded,imgArea.getSelection());
                        
                       
                    }
                    
                };
            }else{
                $("#imageError").text('Invalid image type.');      
            }
        };
        reader.readAsDataURL(file);
    
        return false;
    }; 
     
    var updateCoordinates = function(img,selection){
        console.log(selection);
        $("#ProjectX1").val(selection.x1);
        $("#ProjectX2").val(selection.x2);
        $("#ProjectY1").val(selection.y1);
        $("#ProjectY2").val(selection.y2);
        $("#ProjectThumbImg").val(img.src);
        
    };
}); 
</script>