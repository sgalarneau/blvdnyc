<script type="text/javascript">
    $(function(){
        $(document).tooltip({
            track:true,
            content: function () {
                return $(this).prop('title');
            }
        });
    });    
</script>



<div class="projects index">
	<h2><?php echo __('Projects'); ?></h2>
	
	<?php echo $this->Html->link(__('New Project'), array('action' => 'add'),array('class'=>'pure-button pure-button-primary')); ?>
	
	<br />
	<br />
	
	<table cellpadding="0" cellspacing="0" class="pure-table pure-table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('client_id'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('agency_id'); ?></th> -->
			<th><?php echo $this->Paginator->sort('name_eng',"Name"); ?></th>
			<th><?php echo $this->Paginator->sort('new',"Slideshow"); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('name_fre'); ?></th> -->
			<th><?php echo __('Background'); ?></th>
			<th><?php echo $this->Paginator->sort('headline_eng','Headline'); ?></th>
            <!-- <th><?php echo $this->Paginator->sort('headline_fre'); ?></th> -->
			<th><?php echo __("Description"); ?></th>
			<!-- <th><?php echo __('Description Fre'); ?></th> -->
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projects as $project): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($project['Client']['name'], array('controller' => 'clients', 'action' => 'view', $project['Client']['id'])); ?>
		</td>
		<!-- <td>
            <?php echo $this->Html->link($project['Agency']['name'], array('controller' => 'agencies', 'action' => 'view', $project['Agency']['id'])); ?>
        </td> -->
		<td><?php echo h($project['Project']['name_eng']); ?>&nbsp;</td>
		<td><?php if($project['Project']['new']){echo "Yes";}else{echo "No";} ?>&nbsp;</td>
		<!-- <td><?php echo h($project['Project']['name_fre']); ?>&nbsp;</td> -->
		<td><img width="100" src="<?php echo $this->Html->url('/'); ?>img/p/background_<?php echo $project['Project']['picture']; ?>" /></td>
		<td><?php echo h($project['Project']['headline_eng']); ?>&nbsp;</td>
        <!-- <td><?php echo h($project['Project']['headline_fre']); ?>&nbsp;</td> -->
		<td title="<?php echo h($project['Project']['description_eng']); ?>"><?php echo substr($project['Project']['description_eng'],0,120); ?>...</td>
		<!-- <td title="<?php echo h($project['Project']['description_fre']); ?>"><?php echo substr($project['Project']['description_fre'],0,120); ?>...</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $project['Project']['id'])); ?><br />
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['Project']['id'])); ?><br />
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $project['Project']['id']), array(), __('Are you sure you want to delete %s?', $project['Project']['name_eng']."/".$project['Project']['name_fre'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous').' ', array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(' '.__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
