<div class="projects form pure-g">
    <div class="pure-u-1-2">
        <?php echo $this->Form->create('Project',array('type'=>'file',"class"=>'pure-form pure-form-stacked')); ?>
        	<h2><?php echo __('Edit Project'); ?><!-- &nbsp;<small><a id="help" href="#">help ?</a></small> --></h2>
        	<?php
        		echo $this->Form->input('id');
        		echo $this->Form->input('client_id',array('div'=>array('class'=>'pure-control-group'),"empty"=>true,"required"=>false));
                // echo $this->Form->input('agency_id',array('div'=>array('class'=>'pure-control-group'),"empty"=>true,"required"=>false));
        		echo $this->Form->input('name_eng',array('label'=>'Name','div'=>array('class'=>'pure-control-group')));
        		// echo $this->Form->input('name_fre',array('div'=>array('class'=>'pure-control-group')));
                ?>
                <label for="ProjectPhot">Background image</label>
                <img width="200" src="<?php echo $this->Html->url('/'); ?>img/p/background_<?php echo $this->request->data['Project']['picture']; ?>" />
                <?php                 
                echo $this->Form->input('photo',array('type'=>'file','label'=>'Change Image'));                
                ?>
                <small><em>( Background must be 1500x538 or will be stretch )</em></small>
                <br />
                <br />
                
                <?php                 
                echo $this->Form->input('ogv_file',array('type'=>'file','label'=>'Change hover OGV'));                
                ?>
                <small><em>( ogv must by 716px x 716px )</em></small>
                <br />
                <br />
                
                <?php                 
                echo $this->Form->input('mp4_file',array('type'=>'file','label'=>'Change hover MP4'));                
                ?>
                <small><em>( mp4 must by 716px x 716px )</em></small>
                <br />
                <br />
               
                <label for="ProjectPhot">Thumbnail</label>
                <img width="200" src="<?php echo $this->Html->url('/'); ?>img/p/<?php echo $this->request->data['Project']['thumbnail']; ?>" />
                <br />
                <small><?php echo $this->Html->link('Change Thumbnail',array('controller'=>'projects','action'=>'edit_thumbnail',$this->request->data['Project']['id']),array('class'=>'small')); ?></small>
                <br />
                <br />
                
                <?php
                echo $this->Form->input('headline_eng',array('div'=>array('label'=>'Headline','class'=>'pure-control-group')));
                // echo $this->Form->input('headline_fre',array('div'=>array('class'=>'pure-control-group')));
                echo $this->Form->input('dte',array('label'=>'Date','class'=>'datepicker','div'=>array('class'=>'pure-control-group')));
                ?>
                <small><em>( Note that projects will be ordered by date )</em></small>
                <br />
                <br />
                <?php
                echo $this->Form->input('date',array('type'=>'hidden'));
        		echo $this->Form->input('description_eng',array('label'=>'Description'));
        		// echo $this->Form->input('description_fre');
                // echo $this->Form->input('awards_eng');
                // echo $this->Form->input('awards_fre');
                echo $this->Form->input('credits_eng',array('label'=>'Credits'));
                //echo $this->Form->input('credits_fre');
                ?>
                <br />
                <br />
                <?php
        		echo $this->Form->input('vimeo_link_eng',array('div'=>array('class'=>'pure-control-group'),'label'=>__('Vimeo Id')));
                // echo $this->Form->input('vimeo_link_fre',array('div'=>array('class'=>'pure-control-group'),'label'=>__('Vimeo Id Fre')));
                ?>
                <br />
                <?php
                echo $this->Form->input('vimeo_staff_pick');
                echo $this->Form->input('the_mill',array('type'=>'checkbox'));
                echo $this->Form->input('new',array('label'=>'New Project (featured on home page)'));
            ?>
            
            
            <h2>Categories</h2>
            <?php
        	    echo $this->Form->input('Category',array('label'=>'Categories'));
            ?>
            
            <!-- <h2>Offices</h2>
            <?php
                echo $this->Form->input('Office',array('label'=>'Offices<br />'));
            ?> -->
            
            <h2>Directors</h2>
            <?php
                echo $this->Form->input('Artist',array('label'=>'Directors<br />'));
                echo $this->Form->input('only_on_artists_page',array('label'=>'Only Director\'s page'));
            ?>
            
            <small>( If checked, this project won't show on the <em>Projects</em> page, it will only show in the director's portfolio )</small>
            <br />
            <br />
            
            <?php
    	    echo $this->Form->submit(__('Save Project Info'),array('class'=>'pure-button pure-button-primary')); 
            
        echo $this->Form->end(); ?>
    </div>
    <div class="pure-u-1-2">
        <h2><?php echo __('Add Images'); ?></h2>
        <?php echo $this->Html->link('Add an Image',array('controller'=>'project_images','action'=>'add',$this->request->data['Project']['id']),array('class'=>'pure-button pure-button-primary')); ?>
        
        <?php if(count($this->request->data['ProjectImage'])>1){ ?>
        <br />
        <small>( drag images for order of appearance in the page )</small>
        <?php } ?>
        
        <br />
        <br />
        
        <ul id="sortable">
            <?php foreach ($this->request->data['ProjectImage'] as $key => $image): ?>
                <li class="drag" data-projectimageid="<?php echo $image['id']; ?>">
                    <img width="100%" src="<?php echo $this->Html->url('/'); ?>img/p/thumbnail_<?php echo $image['filename']; ?>" />
                    <small><em>( <?php echo $image['size']; ?> format )</em></small>
                    <div class="actions">
                        <?php echo $this->Html->link(__('Delete'), array('controller'=>'project_images','action' => 'delete', $image['id']), array(), __('Are you sure you want to delete this image')); ?>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>
<!-- 
<div id="helpDialog" cladd>
    <img src="<?php echo $this->Html->url('/',true); ?>img/project_example.jpg" />
</div> -->

<script type="text/javascript">
$(function() {    
    var areaSelect = null;
    
    $( "#addImageBtn" ).button().on( "click", function() {
        dialog.dialog( "open" );
    });
    
    $( "#sortable" ).sortable({
        stop: function() {
            var string = '?';                    
            $("ul#sortable li").each(function(index,li){
                string = string + this.getAttribute('data-projectimageid')+'='+index+'&';
            });
            string = string.slice(0, -1);
            
            var url = '<?php echo $this->Html->url('/'); ?>'+"admin/projects/order/"+'<?php echo $this->request->data['Project']['id']; ?>/'+string;
            
            $.ajax({
                type: "POST",
                url: url
            });
        }
    });
    
    
    $("#CategoryCategory,#ArtistArtist,#OfficeOffice").select2({
        width:'90%'
    });
    
    $("#ProjectClientId,#ProjectAgencyId").select2({
        width:'200px'
    });  
    
    $("#ProjectDte").datepicker({
        dateFormat: "dd/mm/yy",
        altFormat: '@',
        altField:$('#ProjectDate')
    });    
    var timestamp = Number("<?php echo $this->request->data['Project']['date']; ?>");
    var myDate = new Date(timestamp);
    var currentDay = myDate.getDate();
    var currentMonth = myDate.getMonth();
    var currentYear = myDate.getFullYear();
    
    var dateToShow = new Date(currentYear,currentMonth,currentDay);
   
    
    $('#ProjectDte').datepicker('setDate', dateToShow);
    
    //help dialog
    var dialogHelp = $( "#helpDialog" ).dialog({
        autoOpen: false,
        height: 1184,
        width: 670,
        modal:true
    });
    $( "#help" ).on( "click", function() {
        dialogHelp.dialog( "open" );
    });
    
}); 
</script>