<div class="projects form pure-g">
    <div class="pure-u-1-2">
        <?php echo $this->Form->create('Project',array('action'=>'edit_news','type'=>'file',"class"=>'pure-form pure-form-stacked')); ?>
        	<h2><?php echo __('Edit News'); ?></h2>
        	<?php
        		echo $this->Form->input('id');
        		echo $this->Form->input('name_eng',array('div'=>array('class'=>'pure-control-group')));
        		echo $this->Form->input('name_fre',array('div'=>array('class'=>'pure-control-group')));
                echo $this->Form->input('new',array('type'=>'hidden',"value"=>true));
                ?>
                
                <label for="ProjectPhot">Current Thumbnail</label>
                <img width="200" src="<?php echo $this->Html->url('/'); ?>img/p/<?php echo $this->request->data['Project']['thumbnail']; ?>" />
                <br />
                
                
                <label>Change Thumbnail</label>
                <div id="imageError" style="color:#ca0813;font-weight:bold;"></div>
                <div id="holder" style="border:5px dashed #ccc; width:400px; height:150px; background:#eeeeee;"><img src="" style="display:none;" /></div>
                <small>( Drop image here )</small>
                <br />
                
                
                <small id="status">File API & FileReader API not supported on this browser use a modern browser... ... yes, do it !</small>
                <?php echo $this->Form->input('x1',array('type'=>'hidden')); ?>
                <?php echo $this->Form->input('x2',array('type'=>'hidden')); ?>
                <?php echo $this->Form->input('y1',array('type'=>'hidden')); ?>
                <?php echo $this->Form->input('y2',array('type'=>'hidden')); ?>
                <?php echo $this->Form->input('thumbImg',array('type'=>'hidden')); ?>
                
                
                
                <?php
                echo $this->Form->input('dte',array('label'=>'Date','class'=>'datepicker','div'=>array('class'=>'pure-control-group')));
                ?>
                <small><em>( Note that projects will be ordered by date )</em></small>
                <br />
                <br />
                <?php
                echo $this->Form->input('date',array('type'=>'hidden'));
        		echo $this->Form->input('description_eng');
        		echo $this->Form->input('description_fre');
            ?>
            <br />
            <?php
    	    echo $this->Form->submit(__('Save News'),array('class'=>'pure-button pure-button-primary')); 
            
        echo $this->Form->end(); ?>
    </div>
</div>



<script type="text/javascript">
$(function() { 
    
    $("#ProjectDte").datepicker({
        dateFormat: "dd/mm/yy",
        altFormat: '@',
        altField:$('#ProjectDate')
    });    
    var timestamp = Number("<?php echo $this->request->data['Project']['date']; ?>");
    var myDate = new Date(timestamp);
    var currentDay = myDate.getDate();
    var currentMonth = myDate.getMonth();
    var currentYear = myDate.getFullYear();
    
    var dateToShow = new Date(currentYear,currentMonth,currentDay);
   
    
    $('#ProjectDte').datepicker('setDate', dateToShow);
    
    
    
    
    //project thumbnail 
     
    var square = {width:716,height:716};
        
    var holder = document.getElementById('holder'),
        state = document.getElementById('status');
    
    if (typeof window.FileReader === 'undefined') {
      state.className = 'fail';
    } else {
      state.className = 'success';
      state.innerHTML = '';
    }
     
    holder.ondragover = function () { this.className = 'hover'; return false; };
    holder.ondragend = function () { this.className = ''; return false; };
    holder.ondrop = function (e) {
        this.className = '';
        e.preventDefault();
    
        var file = e.dataTransfer.files[0],
        reader = new FileReader();
        reader.onload = function (event) {
            
            
            var image = new Image();
            var imageType = /image.*/;
            if(event.target.result.match(imageType)){
                image.src = event.target.result;
                
                image.onload = function() {
                    // access image size here 
                    //console.log(this.width);
                    //console.log(this);
                    var imgLoaded = this;
                    
                  
                    
                  
                    if(imgLoaded.height<716){
                       $("#imageError").text('Image too small');   
                    }else{             
                        $("#imageError").text('');
                        var xOne,xTwo,yOne,yTwo,minWidth,minHeight;
                    
                        xOne = (imgLoaded.width/2) - (square.width/2);
                        yOne = (imgLoaded.height/2) - (square.height/2);
                        xTwo = xOne + square.width;
                        yTwo = yOne + square.height;
                        minWidth = square.width;
                        minHeight = square.height;
                    
                        
                        $('#holder img').attr('src', imgLoaded.src);
                        $("#holder").height(imgLoaded.height);
                        $("#holder").width(imgLoaded.width);
                        $('#holder img').show();
                        
                        var imgArea = $('#holder img').imgAreaSelect({
                            handles: true,
                            x1: xOne, y1: yOne, x2: xTwo, y2: yTwo,
                            minWidth:minWidth,
                            minHeight:minHeight,
                            aspectRatio: minWidth+":"+minHeight,
                            instance: true,
                            onSelectEnd: function(img, selection){
                                updateCoordinates(imgLoaded,selection);
                            }
                        });
                        
                        updateCoordinates(imgLoaded,imgArea.getSelection());
                        
                       
                    }
                    
                };
            }else{
                $("#imageError").text('Invalid image type.');      
            }
        };
        reader.readAsDataURL(file);
    
        return false;
    }; 
     
    var updateCoordinates = function(img,selection){
        console.log(selection);
        $("#ProjectX1").val(selection.x1);
        $("#ProjectX2").val(selection.x2);
        $("#ProjectY1").val(selection.y1);
        $("#ProjectY2").val(selection.y2);
        $("#ProjectThumbImg").val(img.src);
        
    };
    
    
    
    
    
}); 
</script>