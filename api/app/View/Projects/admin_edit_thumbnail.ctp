<div class="projects form">
    
    <?php echo $this->Form->create('Project',array('url'=>array('action'=>'edit_thumbnail',$project_id),'class'=>'pure-form pure-form-stacked')); ?>
    
    <br />
    
    <h2>Project Thumbnail</h2>
    <div id="imageError" style="color:#ca0813;font-weight:bold;"></div>
    <div id="holder" style="border:5px dashed #ccc; width:400px; height:150px; background:#eeeeee;"><img src="" style="display:none;" /></div>
    <small>( Drop image here )</small>
    <br />
    
    
    <small id="status">File API & FileReader API not supported on this browser use a modern browser... ... yes, do it !</small>
    <?php echo $this->Form->input('x1',array('type'=>'hidden')); ?>
    <?php echo $this->Form->input('x2',array('type'=>'hidden')); ?>
    <?php echo $this->Form->input('y1',array('type'=>'hidden')); ?>
    <?php echo $this->Form->input('y2',array('type'=>'hidden')); ?>
    <?php echo $this->Form->input('thumbImg',array('type'=>'hidden')); ?>
    
    
    <br />
    <br />
    <?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
<script type="text/javascript">
$(function() {
    //project thumbnail 
     
    var square = {width:716,height:716};
        
    var holder = document.getElementById('holder'),
        state = document.getElementById('status');
    
    if (typeof window.FileReader === 'undefined') {
      state.className = 'fail';
    } else {
      state.className = 'success';
      state.innerHTML = '';
    }
     
    holder.ondragover = function () { this.className = 'hover'; return false; };
    holder.ondragend = function () { this.className = ''; return false; };
    holder.ondrop = function (e) {
        this.className = '';
        e.preventDefault();
    
        var file = e.dataTransfer.files[0],
        reader = new FileReader();
        reader.onload = function (event) {
            
            
            var image = new Image();
            var imageType = /image.*/;
            if(event.target.result.match(imageType)){
                image.src = event.target.result;
                
                image.onload = function() {
                    // access image size here 
                    //console.log(this.width);
                    //console.log(this);
                    var imgLoaded = this;
                    
                  
                    
                  
                    if(imgLoaded.height<716){
                       $("#imageError").text('Image too small');   
                    }else{             
                        $("#imageError").text('');
                        var xOne,xTwo,yOne,yTwo,minWidth,minHeight;
                    
                        xOne = (imgLoaded.width/2) - (square.width/2);
                        yOne = (imgLoaded.height/2) - (square.height/2);
                        xTwo = xOne + square.width;
                        yTwo = yOne + square.height;
                        minWidth = square.width;
                        minHeight = square.height;
                    
                        
                        $('#holder img').attr('src', imgLoaded.src);
                        $("#holder").height(imgLoaded.height);
                        $("#holder").width(imgLoaded.width);
                        $('#holder img').show();
                        
                        var imgArea = $('#holder img').imgAreaSelect({
                            handles: true,
                            x1: xOne, y1: yOne, x2: xTwo, y2: yTwo,
                            minWidth:minWidth,
                            minHeight:minHeight,
                            aspectRatio: minWidth+":"+minHeight,
                            instance: true,
                            onSelectEnd: function(img, selection){
                                updateCoordinates(imgLoaded,selection);
                            }
                        });
                        
                        updateCoordinates(imgLoaded,imgArea.getSelection());
                        
                       
                    }
                    
                };
            }else{
                $("#imageError").text('Invalid image type.');      
            }
        };
        reader.readAsDataURL(file);
    
        return false;
    }; 
     
    var updateCoordinates = function(img,selection){
        console.log(selection);
        $("#ProjectX1").val(selection.x1);
        $("#ProjectX2").val(selection.x2);
        $("#ProjectY1").val(selection.y1);
        $("#ProjectY2").val(selection.y2);
        $("#ProjectThumbImg").val(img.src);
        
    };
}); 
</script>