<div class="clients index">
	<h2><?php echo __('Clients'); ?></h2>
	
	<?php echo $this->Html->link(__('New Client'), array('action' => 'add'),array('class'=>'pure-button pure-button-primary')); ?>
	
	<br />
	<br />
	
	<table cellpadding="0" cellspacing="0" class="pure-table pure-table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($clients as $client): ?>
	<tr>
		<td><?php echo h($client['Client']['name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $client['Client']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $client['Client']['id'])); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $client['Client']['id']), array(), __('Are you sure you want to delete %s?', $client['Client']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous').' ', array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(' '.__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
