<script type="text/javascript">
    $(function(){
        $(document).tooltip({track:true});
    });    
</script>

<div class="clients view">
<h2><?php echo __('Client'); ?></h2>
	<dl>
		<!-- <dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($client['Client']['id']); ?>
			&nbsp;
		</dd> -->
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($client['Client']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php if (!empty($client['Project'])): ?>
<div class="related">
	<h3><?php echo __('Related Projects'); ?></h3>
	
	
	
	
    <table cellpadding="0" cellspacing="0" class="pure-table pure-table-striped">
        <thead>
        <tr>
            <th><?php echo __('Name'); ?></th>
           <!--  <th><?php echo __('Name Fre'); ?></th> -->
            <th><?php echo __('Headline'); ?></th>
            <!-- <th><?php echo __('Headline Fre'); ?></th> -->
            <th><?php echo __('Description'); ?></th>
           <!--  <th><?php echo __('Description Fre'); ?></th> -->
            <th style="min-width:150px;"><?php echo __('Vimeo'); ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($client['Project'] as $project): ?>
        <tr>
            
            <td><?php echo h($project['name_eng']); ?>&nbsp;</td>
            <!-- <td><?php echo h($project['name_fre']); ?>&nbsp;</td> -->
            <td><?php echo h($project['headline_eng']); ?>&nbsp;</td>
            <!-- <td><?php echo h($project['headline_fre']); ?>&nbsp;</td> -->
            <td title="<?php echo h($project['description_eng']); ?>"><?php echo substr($project['description_eng'],0,120); ?>...</td>
            <!-- <td title="<?php echo h($project['description_fre']); ?>"><?php echo substr($project['description_fre'],0,120); ?>...</td> -->
            <td><?php echo $this->Vimeo->getEmbedCode($project['vimeo_link_eng']); ?></td>
        </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    	


</div>
<?php endif; ?>