<div class="clients form">
<?php echo $this->Form->create('Client',array('class'=>'pure-form pure-form-stacked')); ?>
	<h2><?php echo __('Edit Client'); ?></h2>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
	?>
    <br />
    <?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>