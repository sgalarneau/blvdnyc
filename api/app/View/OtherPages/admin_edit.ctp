<div class="clients form">
<?php echo $this->Form->create('OtherPage',array('type'=>'file','class'=>'pure-form pure-form-stacked')); ?>
	<h2><?php echo __('Edit Page'); ?>&nbsp;<?php echo $this->request->data['OtherPage']['slug']; ?></h2>
	<?php
		echo $this->Form->input('id');
	?>
	
	<!-- <?php
	/**
     * PAGE HOME !
     */ 
	if($this->request->data['OtherPage']['slug']=="home"){
        ?>
            <label>Current OGV</label>
            <video width="320" height="240" controls>
                <source src="<?php echo $this->Html->url('/files/',true).$this->request->data['OtherPage']['home_ogv_video']; ?>" type="video/ogg">
                Your browser does not support the video tag.
            </video>
            
            <label>Current MP4</label>
            <video width="320" height="240" controls>
                <source src="<?php echo $this->Html->url('/files/',true).$this->request->data['OtherPage']['home_mp4_video']; ?>" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        <?php
        echo $this->Form->input('mp4_file',array('type'=>'file','label'=>'Change Video MP4'));
        echo $this->Form->input('ogv_file',array('type'=>'file','label'=>'Change Video OGV'));
        ?>
        <br />
        <?php
        echo $this->Form->input('home_headline_eng');  
        echo $this->Form->input('home_headline_fre');  
        ?>
        <br />
        
        <label>Current Image</label>
        <img width="400" src="<?php echo $this->Html->url('/files/',true).$this->request->data['OtherPage']['home_background']; ?>" />
        
        <?php
        echo $this->Form->input('background_image',array('type'=>'file','label'=>'Change Background Image'));  
    	
	} ?> -->
	
	
	<?php
    /**
     * PAGE BLVD !
     */ 
    if($this->request->data['OtherPage']['slug']=="interstate"){
	
        echo $this->Form->input('content_text_eng',array('label'=>'Content Text'));  
        //echo $this->Form->input('content_text_fre');  
        
    } ?>
    
    
    
    <?php
    /**
     * PAGE CONTACT !
     */ 
    if($this->request->data['OtherPage']['slug']=="contact"){
    
        echo $this->Form->input('content_text_eng',array('label'=>'Content'));  
        //echo $this->Form->input('content_text_fre');  
        
    } ?>
    
    
    
    <br />
    <?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>