<div class="pages index">
    <h2><?php echo __('Pages'); ?></h2>
    
    <br />
    <br />
    
    <table cellpadding="0" cellspacing="0" class="pure-table pure-table-striped">
    <thead>
    <tr>
            <th>Name</th>
            <th class="actions"><?php echo __('Actions'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($pages as $page): ?>
    <tr>
        <td><?php echo $page['OtherPage']['slug']; ?></td>
        <td><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $page['OtherPage']['slug'])); ?></td>
    </tr>
    <?php endforeach; ?>
    </tbody>
    </table>
</div>