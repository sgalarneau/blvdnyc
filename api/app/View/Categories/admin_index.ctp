<div class="tags index">
	<h2><?php echo __('Categories'); ?></h2>
	
	<?php echo $this->Html->link(__('New Category'), array('action' => 'add'),array('class'=>'pure-button pure-button-primary')); ?>
	
	<br />
    <br />
    
    <table cellpadding="0" cellspacing="0" class="pure-table pure-table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('name_eng','Name'); ?></th>
			<!-- <th><?php echo $this->Paginator->sort('name_fre'); ?></th> -->
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($categories as $category): ?>
	<tr>
		<td><?php echo h($category['Category']['name_eng']); ?>&nbsp;</td>
		<!-- <td><?php echo h($category['Category']['name_fre']); ?>&nbsp;</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $category['Category']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $category['Category']['id'])); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $category['Category']['id']), array(), __('Are you sure you want to delete %s?', $category['Category']['name_eng'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous').' ', array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(' '.__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
