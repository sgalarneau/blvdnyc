<div class="categories form">
<?php echo $this->Form->create('Category',array('class'=>'pure-form pure-form-stacked')); ?>
	<h2><?php echo __('Add Category'); ?></h2>
	<?php
		echo $this->Form->input('name_eng',array('label'=>'Name'));
		// echo $this->Form->input('name_fre');
	?>
    <br />
    <?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>