<?php 
class VimeoHelper extends AppHelper 
{ 

    /** 
     * Creates Vimeo Embed Code from a given Vimeo Video. 
     * 
     *    @param String $vimeo_id URL or ID of Video on Vimeo.com 
     *    @param Array $usr_options VimeoHelper Options Array (see below) 
     *    @return String HTML output. 
    */ 
    function getEmbedCode($vimeo_id, $usr_options = array()) 
    { 
        // Default options. 
        $options = array 
        ( 
            'width' => '100%', 
            'height' => '100%', 
            'show_title' => 1, 
            'show_byline' => 1, 
            'show_portrait' => 0, 
            'color' => 'ca0813',
            'autoplay' => 0 
        ); 
        $options = array_merge($options, $usr_options); 
         
        // Extract Vimeo.id from URL. 
        if (substr($vimeo_id, 0, 21) == 'http://www.vimeo.com/') { 
            $vimeo_id = substr($vimeo_id, 21); 
        } 
        
		$output = '<iframe src="http://player.vimeo.com/video/'. $vimeo_id . '?title=0&amp;autoplay='. $options['autoplay'] .'&amp;byline=0&amp;portrait=0&amp;color='. $options['color'] .'" width="'. $options['width'] . '" height="'. $options['height'] . '" frameborder="0"></iframe><p></p>';
		return $output; 
        
    } 
	
		
	
	
} 
?> 
