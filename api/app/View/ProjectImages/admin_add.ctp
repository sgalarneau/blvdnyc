<div id="addImage" class="projectImages form" title="Add an Image">
    
    <h2><?php echo __('Add Image'); ?></h2>
    
    <?php echo $this->Form->create('ProjectImage',array('controller'=>'project_images','action'=>'add','type'=>'file','class'=>'pure-form pure-form-stacked')); ?>
        <?php
            //echo $this->Form->input('name_eng',array('div'=>array('class'=>'pure-control-group')));
            //echo $this->Form->input('name_fre',array('div'=>array('class'=>'pure-control-group')));
            //echo $this->Form->input('description_eng',array('div'=>array('class'=>'pure-control-group')));
            //echo $this->Form->input('description_fre',array('div'=>array('class'=>'pure-control-group')));
            //echo $this->Form->input('image',array('type'=>'file','div'=>array('class'=>'pure-control-group')));
        ?>
        
        <div id="sizeSelection" class="input radio">
            <input checked disabled name="data[ProjectImage][size]" type="radio" value="square" id="ProjectImageSize1" />
            <label for="UserField1">Square</label>
            <input disabled name="data[ProjectImage][size]" type="radio" value="wide" id="ProjectImageSize2" />
            <label for="UserField2">Wide</label>
        </div>
        
        <div id="imageError" style="color:#ca0813;font-weight:bold;"></div>
        <div id="holder" style="border:5px dashed #ccc; width:400px; height:150px; background:#eeeeee;"><img src="" style="display:none;" /></div>
        <small>( Drop image here )</small>
        <br />
        

        <small id="status">File API & FileReader API not supported on this browser use a modern browser... ... yes, do it !</small>
        <br />
        <?php echo $this->Form->input('project_id',array('type'=>'hidden','value'=>$project_id)); ?>
        <?php echo $this->Form->input('x1',array('type'=>'hidden')); ?>
        <?php echo $this->Form->input('x2',array('type'=>'hidden')); ?>
        <?php echo $this->Form->input('y1',array('type'=>'hidden')); ?>
        <?php echo $this->Form->input('y2',array('type'=>'hidden')); ?>
        <?php echo $this->Form->input('img',array('type'=>'hidden')); ?>

        <?php echo $this->Form->submit(__('Add Image'),array('id'=>'submitBtn','disabled'=>'disabled',"class"=>'pure-button pure-button-primary')); ?>

    <?php echo $this->Form->end(); ?>
</div>

<script>
$(function(){
    
    var square = {width:716,height:716};
    var wide = {width:960,height:470};
        
    var holder = document.getElementById('holder'),
        state = document.getElementById('status');
    
    if (typeof window.FileReader === 'undefined') {
      state.className = 'fail';
    } else {
      state.className = 'success';
      state.innerHTML = '';
    }
     
    holder.ondragover = function () { this.className = 'hover'; return false; };
    holder.ondragend = function () { this.className = ''; return false; };
    holder.ondrop = function (e) {
        this.className = '';
        e.preventDefault();
    
        var file = e.dataTransfer.files[0],
        reader = new FileReader();
        reader.onload = function (event) {
            var image = new Image();
            
            var imageType = /image.*/;
            if(event.target.result.match(imageType)){
                image.src = event.target.result;
            
                image.onload = function() {
                    // access image size here 
                    //console.log(this.width);
                    //console.log(this);
                    var imgLoaded = this;
                    
                    var sizeSelected = $('input[name="data[ProjectImage][size]"]:checked', '#ProjectImageAddForm').val();
                    
                  
                    if(imgLoaded.width<960 || imgLoaded.height<716){
                       $("#imageError").text('Image too small');   
                    }else{             
                        $("#imageError").text('');
                        var xOne,xTwo,yOne,yTwo,minWidth,minHeight;
                        if(sizeSelected=='wide'){
                            xOne = (imgLoaded.width/2) - (wide.width/2);
                            yOne = (imgLoaded.height/2) - (wide.height/2);
                            xTwo = xOne + wide.width;
                            yTwo = yOne + wide.height;
                            minWidth = wide.width;
                            minHeight = wide.height;
                        }else if(sizeSelected=='square'){
                            xOne = (imgLoaded.width/2) - (square.width/2);
                            yOne = (imgLoaded.height/2) - (square.height/2);
                            xTwo = xOne + square.width;
                            yTwo = yOne + square.height;
                            minWidth = square.width;
                            minHeight = square.height;
                        }
                        
                        $('#holder img').attr('src', imgLoaded.src);
                        $("#holder").height(imgLoaded.height);
                        $("#holder").width(imgLoaded.width);
                        $('#holder img').show();
                        
                        var imgArea = $('#holder img').imgAreaSelect({
                            handles: true,
                            x1: xOne, y1: yOne, x2: xTwo, y2: yTwo,
                            minWidth:minWidth,
                            minHeight:minHeight,
                            aspectRatio: minWidth+":"+minHeight,
                            instance: true,
                            onSelectEnd: function(img, selection){
                                updateCoordinates(imgLoaded,selection);
                            }
                        });
                        
                        updateCoordinates(imgLoaded,imgArea.getSelection());
                        
                        $('#sizeSelection input').each(function(){
                           $(this).removeAttr( "disabled" ); 
                           $(this).on('change',function(){
                               sizeSelected = $('input[name="data[ProjectImage][size]"]:checked', '#ProjectImageAddForm').val();
                               
                               if(sizeSelected=='wide'){
                                    xOne = (imgLoaded.width/2) - (wide.width/2);
                                    yOne = (imgLoaded.height/2) - (wide.height/2);
                                    xTwo = xOne + wide.width;
                                    yTwo = yOne + wide.height;
                                    minWidth = wide.width;
                                    minHeight = wide.height;
                               }else if(sizeSelected=='square'){
                                    xOne = (imgLoaded.width/2) - (square.width/2);
                                    yOne = (imgLoaded.height/2) - (square.height/2);
                                    xTwo = xOne + square.width;
                                    yTwo = yOne + square.height;
                                    minWidth = square.width;
                                    minHeight = square.height;
                               }
                               
                               imgArea.setOptions({
                                   x1:xOne,y1:yOne,x2:xTwo,y2:yTwo,
                                   minWidth:minWidth,minHeight:minHeight,
                                   aspectRatio: minWidth+":"+minHeight,
                                   onSelectEnd: function(img, selection){
                                        updateCoordinates(imgLoaded,selection);
                                   }
                               });
                            
                               
                               updateCoordinates(imgLoaded,imgArea.getSelection());
                           }); 
                        });
                        
                        $('#submitBtn').removeAttr( "disabled" ); 
                        
                    }
                    
                };
            }else{
                $("#imageError").text('Invalid image type');   
            }
        };
        reader.readAsDataURL(file);
    
        return false;
    };
    
    var updateCoordinates = function(img,selection){
        console.log(selection);
        $("#ProjectImageX1").val(selection.x1);
        $("#ProjectImageX2").val(selection.x2);
        $("#ProjectImageY1").val(selection.y1);
        $("#ProjectImageY2").val(selection.y2);
        $("#ProjectImageImg").val(img.src);
        
    };
 
});
</script>