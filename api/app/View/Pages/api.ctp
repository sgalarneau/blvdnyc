<style>
    h3 input{
        font-size:80%;
        width:240px;
    }
    
    pre{
        color:#666;
        font-size:80%;
    }
    
    h2{
        border-bottom:2px solid black;
    }
</style>


<h1>API</h1>

<h2>Projects</h2>
<ul>
    <li>
        <h3>GET /api/projects/index.json <input id="queryString1" placeholder="{queryString}" /></h3>
<pre>
/**
 * Returns all projects filtered by optional params as a query string
 *
 * ex. /api/projects/index.json<strong>?limit=3&page=2&category[]=1&category[]=2&artists[]=4&offices[]=2</strong>
 *
 * <strong>@opt_param int page</strong> - what page you want default is 1 
 * <strong>@opt_param int limit</strong> - number of results per page default is 7
 * <strong>@opt_param boolean news</strong> - if we want news with project default is true
 * <strong>@opt_param boolean only_new</strong> - if we want only new project with news
 * <strong>@opt_param array categories[]</strong> - project by categories ids for filtering projects
 * <strong>@opt_param array artists[]</strong> - project by artists ids for filtering artists
 * <strong>@opt_param array offices[]</strong> - project by offices ids for filtering artists
 */
</pre>
        <button class="pure-button pure-button-primary" id="ex1btn">Call it !</button> <button class="pure-button pure-button-primary clear">Clear !</button>
        <pre id="example1" style="font-size:90%; padding:10px 15px; margin:10px 0; background:rgb(230,230,230);"></pre>
    </li>
    <li>
        <h3>GET /api/projects/view/<input id="projectId" placeholder="{Project.slug}" />.json</h3>
<pre>
/**
 * Returns a project
 *
 * ex. /api/projects/view/<strong>name-slug</strong>.json
 */
</pre>
        <button class="pure-button pure-button-primary" id="ex2btn">Call it !</button> <button class="pure-button pure-button-primary clear">Clear !</button>
        <pre id="example2" style="font-size:90%; padding:10px 15px; margin:10px 0; background:rgb(230,230,230);"></pre>
    </li>
</ul>

<h2>Artists</h2>
<ul>
    <li>
        <h3>GET /api/artists/index.json</h3>
<pre>
/**
 * Returns all artists
 *
 * ex. /api/artists/index.json
 */
</pre>
        <button class="pure-button pure-button-primary" id="ex3btn">Call it !</button> <button class="pure-button pure-button-primary clear">Clear !</button>
        <pre id="example3" style="font-size:90%; padding:10px 15px; margin:10px 0; background:rgb(230,230,230);"></pre>
    </li>
    <li>
        <h3>GET /api/artists/view/<input id="artistSlug" placeholder="{Aritst.slug}" />.json</h3>
<pre>
/**
 * Returns an artist
 *
 * ex. /api/artists/view/<strong>john-doe</strong>.json
 */
</pre>
        <button class="pure-button pure-button-primary" id="ex4btn">Call it !</button> <button class="pure-button pure-button-primary clear">Clear !</button>
        <pre id="example4" style="font-size:90%; padding:10px 15px; margin:10px 0; background:rgb(230,230,230);"></pre>
    </li>
</ul>
 
   
<h2>Categories</h2>
<ul>
    <li>
        <h3>GET /api/categories/index.json</h3>
<pre>
/**
 * Returns all categories
 *
 * ex. /api/categories/index.json
 */
</pre>
        <button class="pure-button pure-button-primary" id="ex5btn">Call it !</button> <button class="pure-button pure-button-primary clear">Clear !</button>
        <pre id="example5" style="font-size:90%; padding:10px 15px; margin:10px 0; background:rgb(230,230,230);"></pre>
    </li>
</ul>



<h2>Offices</h2>
<ul>
    <li>
        <h3>GET /api/offices/index.json</h3>
<pre>
/**
 * Returns all offices
 *
 * ex. /api/offices/index.json
 */
</pre>
        <button class="pure-button pure-button-primary" id="ex6btn">Call it !</button> <button class="pure-button pure-button-primary clear">Clear !</button>
        <pre id="example6" style="font-size:90%; padding:10px 15px; margin:10px 0; background:rgb(230,230,230);"></pre>
    </li>
</ul>



<h2>Other Page</h2>
<ul>
    <li>
        <h3>GET /api/other_pages/view/<input id="pageSlug" placeholder="{OtherPage.slug}" />.json</h3>
<pre>
/**
 * Returns a page
 * slug can be home, blvd, contact (for now)
 * ex. /api/other_pages/view/<strong>home</strong>.json
 */
</pre>
        <button class="pure-button pure-button-primary" id="ex7btn">Call it !</button> <button class="pure-button pure-button-primary clear">Clear !</button>
        <pre id="example7" style="font-size:90%; padding:10px 15px; margin:10px 0; background:rgb(230,230,230);"></pre>
    </li>
</ul>





<script type="text/javascript">
    $(function() {

        //GET /projects/index.json
        $('#ex1btn').on('click',function(){    
            $.ajax({
                url: "<?php echo $this->Html->url('/'); ?>"+"projects/index.json"+$("#queryString1").val(),
                context: $('#example1')
            }).done($.isDone);
        });  
        
        
        //GET /projects/view/{Project.id}.json
        $('#ex2btn').on('click',function(){    
            $.ajax({
                url: "<?php echo $this->Html->url('/'); ?>"+"projects/view/"+$('#projectId').val()+".json",
                context: $('#example2')
            }).done($.isDone);
        }); 
        
        
        //GET /artists/index.json
        $('#ex3btn').on('click',function(){    
            $.ajax({
                url: "<?php echo $this->Html->url('/'); ?>"+"artists/index.json",
                context: $('#example3')
            }).done($.isDone);
        });  
        
        
        //GET /artists/view/{Artist.slug}.json
        $('#ex4btn').on('click',function(){    
            $.ajax({
                url: "<?php echo $this->Html->url('/'); ?>"+"artists/view/"+$('#artistSlug').val()+".json",
                context: $('#example4')
            }).done($.isDone);
        });
        
        //GET /categories/index.json
        $('#ex5btn').on('click',function(){    
            $.ajax({
                url: "<?php echo $this->Html->url('/'); ?>"+"categories/index.json",
                context: $('#example5')
            }).done($.isDone);
        });
        
        //GET /offices/index.json
        $('#ex6btn').on('click',function(){    
            $.ajax({
                url: "<?php echo $this->Html->url('/'); ?>"+"offices/index.json",
                context: $('#example6')
            }).done($.isDone);
        }); 
        
        //GET /other_pages/view/{OtherPage.slug}.json
        $('#ex7btn').on('click',function(){    
            $.ajax({
                url: "<?php echo $this->Html->url('/'); ?>"+"other_pages/view/"+$('#pageSlug').val()+".json",
                context: $('#example7')
            }).done($.isDone);
        }); 
        
        
        $(".clear").each(function(index,button){
            $(this).on('click',function(){
                $(this).next().empty();
            });
        });
        
    });
    
    /**
     * output data JSON in context
     */
    $.isDone = function(data){
        var jsonPretty = JSON.stringify(data,null,2);  
        $(this).html($('<div/>').text(jsonPretty).html());
    }
    
</script>
