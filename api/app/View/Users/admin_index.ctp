<div class="users index">
	<h2><?php echo __('Administrators'); ?></h2>
	
	<?php echo $this->Html->link(__('New Administrator'), array('action' => 'add'),array('class'=>'pure-button pure-button-primary')); ?>
	<br />
	<br />
	
	
	<table cellpadding="0" cellspacing="0" class="pure-table">
	<thead>
	<tr>
    	<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('username'); ?></th>
		<th><?php echo $this->Paginator->sort('created'); ?></th>
		<th><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['created']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['slug'])); ?><br />
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['slug'])); ?><br />
			<?php //echo $this->Html->link(__('Delete'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete %s?', $user['User']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<div class="paging">
    <?php
        echo $this->Paginator->prev('< ' . __('previous').' ', array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ' '));
        echo $this->Paginator->next(' '.__('next') . ' >', array(), null, array('class' => 'next disabled'));
    ?>
    </div>
</div>