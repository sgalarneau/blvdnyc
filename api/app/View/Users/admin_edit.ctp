<div class="users form">
<?php echo $this->Form->create('User',array('type'=>'file',"class"=>'pure-form pure-form-stacked')); ?>
	<h2><?php echo __('Edit Administrator'); ?></h2>
	<?php
	    echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('username');
		echo $this->Form->input('pwd',array('type'=>'password','required' => false));
        echo $this->Form->input('pwd_repeat',array('type'=>'password','required' => false));
        //echo $this->Form->input('photo',array('type'=>'file'));
	    echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); 
    
        echo $this->Form->end(); ?>
</div>
