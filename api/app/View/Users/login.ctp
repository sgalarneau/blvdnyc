<div class="users form" style="width:150px; margin:50px auto; text-align: center;">
<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('User',array('class'=>'pure-form pure-form-stacked', 'autocomplete' => 'off')); ?>
    
        <h2><?php echo __('Login'); ?></h2>
        <?php echo $this->Form->input('username',array('default'=>''));
        echo $this->Form->input('password',array('default'=>''));
        
        echo $this->Form->submit(__('Login'),array('class'=>'pure-button pure-button-primary'))
        
    ?>
    
<?php echo $this->Form->end(); ?>
</div>