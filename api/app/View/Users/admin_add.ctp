<div class="users form">
<?php echo $this->Form->create('User',array('type'=>'file',"class"=>'pure-form pure-form-stacked', 'autocomplete' => 'off')); ?>

		<h2><?php echo __('Add Administrator'); ?></h2>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('username');
		echo $this->Form->input('pwd',array('type'=>'password'));
        echo $this->Form->input('pwd_repeat',array('type'=>'password'));
        //echo $this->Form->input('photo',array('type'=>'file'));
	?>
    
    <br />
    
	<?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>	
	
<?php echo $this->Form->end(); ?>
</div>
