<div class="offices index">
	<h2><?php echo __('Offices'); ?></h2>
	
	<?php echo $this->Html->link(__('New Office'), array('action' => 'add'),array('class'=>'pure-button pure-button-primary')); ?>
	<br />
	<br />
	<em><small>( drag to change order )</small></em>
    <br />
    <br />
	<table cellpadding="0" cellspacing="0" class="pure-table pure-table-striped">
    	<thead>
        	<tr>
        		<th>&nbsp;</th>
        		<th><?php echo __('Name Eng'); ?></th>
        		<th><?php echo __('Name Fre'); ?></th>
        		<th class="actions"><?php echo __('Actions'); ?></th>
        	</tr>
    	</thead>
    	<tbody id="sortable">
        	<?php foreach ($offices as $office): ?>
        	<tr data-officeid="<?php echo $office['Office']['id']; ?>">
        		<td>
                    <span class="handle ui-icon ui-icon-triangle-2-n-s" style="cursor:move; background-color:#ccc;">&nbsp;</span>
                </td>
        		<td><?php echo h($office['Office']['name_eng']); ?>&nbsp;</td>
        		<td><?php echo h($office['Office']['name_fre']); ?>&nbsp;</td>
        		<td class="actions">
        			<?php echo $this->Html->link(__('View'), array('action' => 'view', $office['Office']['id'])); ?>
        			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $office['Office']['id'])); ?>
        			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $office['Office']['id']), array(), __('Are you sure you want to delete %s?', $office['Office']['name_eng']." / ".$office['Office']['name_fre'])); ?>
        		</td>
        	</tr>
            <?php endforeach; ?>
    	</tbody>
	</table>
</div>


<script type="text/javascript">
    $(function(){
        $( "#sortable" ).sortable({
            handle: ".handle",
            stop: function() {
                var string = '?';                    
                $("#sortable tr").each(function(index,li){
                    string = string + this.getAttribute('data-officeid')+'='+index+'&';
                });
                string = string.slice(0, -1);
                
                var url = '<?php echo $this->Html->url('/'); ?>'+"admin/offices/order/"+string;
                
                $.ajax({
                    type: "POST",
                    url: url
                });
            }
        });
    });
</script>