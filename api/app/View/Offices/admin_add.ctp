<div class="offices form">
<?php echo $this->Form->create('Office',array("class"=>'pure-form pure-form-stacked')); ?>
	<h2><?php echo __('Add Office'); ?></h2>
	<?php
		echo $this->Form->input('name_eng');
		echo $this->Form->input('name_fre');
	?>
    <br />
    <?php
    echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); 

    echo $this->Form->end(); ?>
</div>