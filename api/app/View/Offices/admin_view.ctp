<div class="offices view">
<h2><?php echo __('Office'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($office['Office']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name Eng'); ?></dt>
		<dd>
			<?php echo h($office['Office']['name_eng']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name Fre'); ?></dt>
		<dd>
			<?php echo h($office['Office']['name_fre']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($office['Office']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($office['Office']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>

<div class="related">
    <h3><?php echo __('Related Directors'); ?></h3>
    <?php if (!empty($office['Artist'])): ?>
    <table cellpadding = "0" cellspacing = "0" class="pure-table pure-table-striped">
    <thead>
        <tr>
            <th><?php echo __('Name'); ?></th>
            <th><?php echo __('Description Eng'); ?></th>
            <th><?php echo __('Description Fre'); ?></th>
            <th><?php echo __('Modified'); ?></th>
            <th><?php echo __('Created'); ?></th>
        </tr>
    </thead>
    <?php foreach ($office['Artist'] as $artist): ?>
        <tr>
            <td><?php echo $artist['name']; ?></td>
            <td><?php echo $artist['description_eng']; ?></td>
            <td><?php echo $artist['description_fre']; ?></td>
            <td><?php echo $artist['modified']; ?></td>
            <td><?php echo $artist['created']; ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
    <?php endif; ?>
</div>


<div class="related">
    <h3><?php echo __('Related Projects'); ?></h3>
    <?php if (!empty($office['Project'])): ?>
    <table cellpadding = "0" cellspacing = "0" class="pure-table pure-table-striped">
    <thead>
    <tr>
        <th><?php echo __('Name Eng'); ?></th>
        <th><?php echo __('Name Fre'); ?></th>
        <th><?php echo __('Headline Eng'); ?></th>
        <th><?php echo __('Headline Fre'); ?></th>
        <th><?php echo __('Description Eng'); ?></th>
        <th><?php echo __('Description Fre'); ?></th>
        <th><?php echo __('Vimeo Image'); ?></th>
        <th><?php echo __('Modified'); ?></th>
        <th><?php echo __('Created'); ?></th>
    </tr>
    </thead>
    <?php foreach ($office['Project'] as $project): ?>
        <tr>
            <td><?php echo $project['name_eng']; ?></td>
            <td><?php echo $project['name_fre']; ?></td>
            <td><?php echo $project['headline_eng']; ?></td>
            <td><?php echo $project['headline_fre']; ?></td>
            <td><?php echo $project['description_eng']; ?></td>
            <td><?php echo $project['description_fre']; ?></td>
            <td><img width="200" src="<?php echo $project['vimeo_image']; ?>" /></td>
            <td><?php echo $project['modified']; ?></td>
            <td><?php echo $project['created']; ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
<?php endif; ?>
</div>
