<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<?php echo $this->Html->charset(); ?>
	<title>Interstate - Administration</title>
    
    
    
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/css/jquery-ui.min.css"> 
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.0/select2.css">
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/css/imgareaselect-default.css" />
    
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
    <link rel="stylesheet" href="<?php echo $this->Html->url('/') ?>css/api/admin.css">

    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.0/select2.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/tinymce/4.1.3/tinymce.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/js/jquery.imgareaselect.pack.js"></script>
</head>
<body>
    <div id="container">
        <div id="container-padded">
    		<div id="header" style="clear:both; width:100%; margin:10px auto; overflow:auto; text-align: center;">
                <img src="<?php echo $this->Html->url('/'); ?>img/logo2.png" alt="Interstate" style="" />
                
                <?php if($this->Session->read('Auth.User')){ ?>
                    <div id="logoutSection">
                        Hi <strong><?php echo $this->Session->read('Auth.User.name'); ?></strong><br />
                	    <small style="font-size: 90%;"><?php echo $this->Html->link('Logout',array('controller'=>'users','action'=>'logout','admin'=>false,'language'=>$this->Session->read('Config.language')),array('class'=>'pure-button')); ?></small>
        	        </div>
        	    <?php } ?>
        	    
    		</div>
    
       
            <?php if($this->Session->read('Auth.User')){ ?>
        		<div class="pure-menu pure-menu-open pure-menu-horizontal" style="clear:both; width:100%; margin-top:10px; text-align: center;">
                    <ul>
                        <li<?php if($this->params['controller']=='projects' && $this->params['action']!='admin_news' && $this->params['action']!='admin_add_news'){ echo ' class="pure-menu-selected"'; } ?>><?php echo $this->Html->link('Projects','/admin/projects/index'); ?></li>
                        <!-- <li<?php if($this->params['controller']=='projects' && ($this->params['action']=='admin_news' || $this->params['action']=='admin_add_news')){ echo ' class="pure-menu-selected"'; } ?>><?php echo $this->Html->link('News','/admin/projects/news'); ?></li> -->
                        <li<?php if($this->params['controller']=='artists'){ echo ' class="pure-menu-selected"'; } ?>><?php echo $this->Html->link('Directors','/admin/artists/index'); ?></li>
                        <li<?php if($this->params['controller']=='clients'){ echo ' class="pure-menu-selected"'; } ?>><?php echo $this->Html->link('Clients','/admin/clients/index'); ?></li>
                        <!-- <li<?php if($this->params['controller']=='agencies'){ echo ' class="pure-menu-selected"'; } ?>><?php echo $this->Html->link('Agencies','/admin/agencies/index'); ?></li> -->
                        <li<?php if($this->params['controller']=='categories'){ echo ' class="pure-menu-selected"'; } ?>><?php echo $this->Html->link('Categories','/admin/categories/index'); ?></li>
                        <!-- <li<?php if($this->params['controller']=='offices'){ echo ' class="pure-menu-selected"'; } ?>><?php echo $this->Html->link('Offices','/admin/offices/index'); ?></li> -->
                        <li<?php if($this->params['controller']=='other_pages'){ echo ' class="pure-menu-selected"'; } ?>><?php echo $this->Html->link('Other Pages','/admin/other_pages/index'); ?></li>
                        <li<?php if($this->params['controller']=='users'){ echo ' class="pure-menu-selected"'; } ?>><?php echo $this->Html->link('Administrators','/admin/users/index'); ?></li>
                    </ul>
                </div>
            <?php } ?>
    		
    		<div id="content">
    			<?php echo $this->Session->flash(); ?>
    			<?php echo $this->fetch('content'); ?>
    		</div>
    		
    		<br />
    		<br />
    		
    		<div id="footer">
        		<small>Interstate by BLVD<br />&copy; 2015</small><br />
        		<small><?php echo $this->Html->link('API', array('admin'=>false,'controller'=>'pages','action'=>'display','api')); ?></small>				
    		</div>
    	</div>
	</div>
</body>
<script type="text/javascript">
$(function(){
    if($("#flashMessage")){
       $("#flashMessage").delay(4000).slideUp();
    }
    
    if($("textarea").length!=0){
        tinymce.init({
            selector: "textarea",
            plugins: ['link textcolor'],
            menubar : false,
            toolbar: "bold italic | link unlink | forecolor",
            statusbar:false,
            width:"90%",
            forced_root_block : false,
            textcolor_map: [
                  "ca0813", "Red BLVD"
            ]
        });
    }
});
</script>
</html>
