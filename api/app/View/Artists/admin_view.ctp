<div class="artists view">
<h2><?php echo __('Director'); ?></h2>
	<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($artist['Artist']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<small><?php echo nl2br($artist['Artist']['description_eng']); ?></small>
			&nbsp;
		</dd>
		<!-- <dt><?php echo __('Description Fre'); ?></dt>
		<dd>
			<small><?php echo nl2br($artist['Artist']['description_fre']); ?></small>
			&nbsp;
		</dd> -->
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($artist['Artist']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($artist['Artist']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>



<div class="related">
	<h3><?php echo __('Related Projects'); ?></h3>
	<?php if (!empty($artist['Project'])): ?>
	<table cellpadding = "0" cellspacing = "0" class="pure-table pure-table-striped">
    <thead>
	<tr>
		<th><?php echo __('Name'); ?></th>
		<!-- <th><?php echo __('Name Fre'); ?></th> -->
		<th><?php echo __('Headline'); ?></th>
		<!-- <th><?php echo __('Headline Fre'); ?></th> -->
		<th><?php echo __('Description'); ?></th>
		<!-- <th><?php echo __('Description Fre'); ?></th> -->
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<?php foreach ($artist['Project'] as $project): ?>
		<tr>
			<td><?php echo $project['name_eng']; ?></td>
			<!-- <td><?php echo $project['name_fre']; ?></td> -->
			<td><?php echo $project['headline_eng']; ?></td>
			<!-- <td><?php echo $project['headline_fre']; ?></td> -->
			<td><?php echo $project['description_eng']; ?></td>
			<!-- <td><?php echo $project['description_fre']; ?></td> -->
			<td><?php echo $project['modified']; ?></td>
			<td><?php echo $project['created']; ?></td>
			<td class="actions">
            <?php echo $this->Html->link(__('View'), array('controller'=>"projects",'action' => 'view', $project['id'])); ?><br />
            <?php echo $this->Html->link(__('Edit'), array('controller'=>"projects",'action' => 'edit', $project['id'])); ?>
            <?php //echo $this->Html->link(__('Delete'), array('controller'=>"projects",'action' => 'delete', $project['id']), array(), __('Are you sure you want to delete %s?', $project['name_eng']."/".$project['name_fre'])); ?>
        </td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
