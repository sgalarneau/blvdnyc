<div class="artists index">
	<h2><?php echo __('Directors'); ?></h2>
	
	<?php echo $this->Html->link(__('New Director'), array('action' => 'add'),array('class'=>'pure-button pure-button-primary')); ?>
	
	<br />
	<br />
	
	
	<em><small>( drag to change order )</small></em>
	<br />
	<br />
	<table cellpadding="0" cellspacing="0" class="pure-table pure-table-striped">
    	<thead>
    	<tr>
    		<th>&nbsp;</th>
    		<th><?php echo __('Name'); ?></th>
    		<th><?php echo __('Description'); ?></th>
    		<!-- <th><?php echo __('Description Fre'); ?></th> -->
    		<th class="actions"><?php echo __('Actions'); ?></th>
    	</tr>
    	</thead>
    	<tbody id="sortable">
    	<?php foreach ($artists as $artist): ?>
    	<tr data-artistid="<?php echo $artist['Artist']['id']; ?>">
    		<td>
    		    <span class="handle ui-icon ui-icon-triangle-2-n-s" style="cursor:move; background-color:#ccc;">&nbsp;</span>
    		</td>
    		<td><?php echo h($artist['Artist']['name']); ?>&nbsp;</td>
    		<td title="<?php echo h($artist['Artist']['description_eng']); ?>"><?php echo substr($artist['Artist']['description_eng'],0,120); ?> ...</td>
    		<!-- <td title="<?php echo h($artist['Artist']['description_fre']); ?>"><?php echo substr($artist['Artist']['description_fre'],0,120); ?> ...</td> -->
    		<td class="actions">
    			<?php echo $this->Html->link(__('View'), array('action' => 'view', $artist['Artist']['id'])); ?>
    			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $artist['Artist']['id'])); ?>
    			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $artist['Artist']['id']), array(), __('Are you sure you want to delete %s?', $artist['Artist']['name'])); ?>
    		</td>
    	</tr>
    <?php endforeach; ?>
    	</tbody>
	</table>
</div>
    

<script type="text/javascript">
    $(function(){
        $( "#sortable" ).sortable({
            handle: ".handle",
            stop: function() {
                var string = '?';                    
                $("#sortable tr").each(function(index,li){
                    string = string + this.getAttribute('data-artistid')+'='+index+'&';
                });
                string = string.slice(0, -1);
                
                var url = '<?php echo $this->Html->url('/'); ?>'+"admin/artists/order/"+string;
                
                $.ajax({
                    type: "POST",
                    url: url
                });
            }
        });
        
        $(document).tooltip({
            track:true,
            content: function () {
                return $(this).prop('title');
            }
        });
        
    });
</script>