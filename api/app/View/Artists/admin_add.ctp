<div class="artists form">
<?php echo $this->Form->create('Artist',array('class'=>'pure-form pure-form-stacked')); ?>
	<h2><?php echo __('Add Director'); ?></h2>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('description_eng',array('label'=>'Description','style'=>'width:500px'));
		// echo $this->Form->input('description_fre',array('style'=>'width:500px'));
	?>
	
	<!-- <h2>Offices</h2>
    <?php
        echo $this->Form->input('Office',array('label'=>'Offices<br />'));
    ?> -->
	
    <br />
    <?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

<script type="text/javascript">
$(function() {
    $("#OfficeOffice").select2({
        width:'400'
    });
}); 
</script>