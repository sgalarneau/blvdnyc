
<div class="pure-g">
    <div class="artists form pure-u-1-2">
    <?php echo $this->Form->create('Artist',array('class'=>'pure-form pure-form-stacked')); ?>
    	<h2><?php echo __('Edit Director'); ?></h2>
    	<?php
    		echo $this->Form->input('id');
    		echo $this->Form->input('name');
    		echo $this->Form->input('description_eng',array('label'=>'Description','style'=>'width:80%'));
    		// echo $this->Form->input('description_fre',array('style'=>'width:80%'));
    	?>
    	
    	<!-- <h2>Offices</h2>
        <?php
            echo $this->Form->input('Office',array('label'=>'Offices<br />'));
        ?>
    	 -->
        <br />
        <?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>
    <?php echo $this->Form->end(); ?>
    </div>
</div>

<script type="text/javascript">
    $(function(){        
        $("#OfficeOffice").select2({
            width:'90%'
        });
    });
</script>