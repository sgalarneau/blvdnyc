<div class="agencies index">
	<h2><?php echo __('Agencies'); ?></h2>
	
	<?php echo $this->Html->link(__('New Agency'), array('action' => 'add'),array('class'=>'pure-button pure-button-primary')); ?>
    
    <br />
    <br />
	
	<table cellpadding="0" cellspacing="0" class="pure-table pure-table-striped">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($agencies as $agency): ?>
	<tr>
		
		<td><?php echo h($agency['Agency']['name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $agency['Agency']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $agency['Agency']['id'])); ?>
			<?php echo $this->Html->link(__('Delete'), array('action' => 'delete', $agency['Agency']['id']), array(), __('Are you sure you want to delete # %s?', $agency['Agency']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous').' ', array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(' '.__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
