<div class="agencies form">
<?php echo $this->Form->create('Agency',array('class'=>'pure-form pure-form-stacked')); ?>
    <h2><?php echo __('Add Agency'); ?></h2>
    <?php
        echo $this->Form->input('name');
	?>
	<br />
    <?php echo $this->Form->submit(__('Submit'),array('class'=>'pure-button pure-button-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>