-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Feb 02, 2015 at 08:19 PM
-- Server version: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `interstate_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `agencies`
--

CREATE TABLE `agencies` (
  `id` char(36) NOT NULL,
  `name` varchar(250) NOT NULL,
  `modified` date NOT NULL,
  `created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agencies`
--

INSERT INTO `agencies` (`id`, `name`, `modified`, `created`) VALUES
('54105015-dc2c-4ef5-8d7b-026c6631c4ed', 'agence 1', '2014-09-10', '2014-09-10'),
('5410501b-d3d8-4bd3-94b3-026c6631c4ed', 'agence 2', '2014-09-10', '2014-09-10'),
('5410a96f-8790-415c-8dca-20c16631c4ed', '{AGENCY}', '2014-09-10', '2014-09-10'),
('5411f6b6-eea0-420d-aba6-040b6631c4ed', 'Agenccccccc', '2014-09-11', '2014-09-11');

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description_eng` text NOT NULL,
  `description_fre` text NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(250) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`, `description_eng`, `description_fre`, `order`, `slug`, `modified`, `created`) VALUES
(1, 'Yann Mabille', '<span style="color: #ca0813;">Yann is cofounder of Interstate. He has over 20 years of experience in the VFX, gaming and advertising industry in France, London and New York.</span> <br /><br />Originally from a traditional illustration background, Yann was previously a creative pillar within the Mill where he spent 15 years as a VFX supervisor, head of 3D in NY, Creative Director and Director at Mill+ during the last 6 years. He brings an extensive knowledge of <span style="color: #ca0813;">VFX and production experience</span>. Yann has worked with artists such as Daft Punk and Beyonc&eacute;, and directed projects for Ford, Hallmark, Showtime Networks, Valspar. His attention to detail and unique vision, as well as his passion for photography and filmmaking have contributed to his success as an award winning director, while his acute artistic eye and expansion into app development have helped to move his creative palette beyond traditional filmmaking and into trans-media content creation.', '', 0, 'yann-mabille', '2015-01-29 15:29:17', '2014-09-15 14:10:41'),
(2, 'Danny Rosenbloom', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at rutrum nulla, in viverra risus. Praesent dignissim fermentum eros vel euismod. Duis efficitur tincidunt laoreet. Phasellus fermentum augue dui, id pretium est tincidunt quis. Sed condimentum laoreet odio eget varius. Duis rhoncus feugiat justo, eget vestibulum turpis mollis ac. Aenean ac sem ante. Praesent vel metus at augue rutrum laoreet sed et ligula. Morbi hendrerit nunc lorem, quis consectetur enim tristique sed. Nullam dictum gravida felis in molestie. Quisque pulvinar ultricies lacus, interdum congue tortor venenatis et. Morbi vitae ex id enim mollis hendrerit. Curabitur non aliquam mauris. Integer in neque justo. Maecenas blandit rhoncus massa et sodales. Aenean vestibulum, dolor vel ultricies dapibus, ante augue egestas erat, sed lacinia ligula nibh sed enim.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce at rutrum nulla, in viverra risus. Praesent dignissim fermentum eros vel euismod. Duis efficitur tincidunt laoreet. Phasellus fermentum augue dui, id pretium est tincidunt quis. Sed condimentum laoreet odio eget varius. Duis rhoncus feugiat justo, eget vestibulum turpis mollis ac. Aenean ac sem ante. Praesent vel metus at augue rutrum laoreet sed et ligula. Morbi hendrerit nunc lorem, quis consectetur enim tristique sed. Nullam dictum gravida felis in molestie. Quisque pulvinar ultricies lacus, interdum congue tortor venenatis et. Morbi vitae ex id enim mollis hendrerit. Curabitur non aliquam mauris. Integer in neque justo. Maecenas blandit rhoncus massa et sodales. Aenean vestibulum, dolor vel ultricies dapibus, ante augue egestas erat, sed lacinia ligula nibh sed enim.', 1, 'danny-rosenbloom', '2015-01-29 15:13:13', '2015-01-26 21:17:10'),
(3, 'Adam Levite', 'Adam started his career as a designer for Burton Snowboards where he quickly found international success and numerous design accolades. He became a favorite with leading edge brands and cultural institutions like MTV, SciFi, VH1, the Guggenheim Museum, Atlantic Records and Fine Line Features. His movie posters for Boogie Nights and Happiness among others are instantly recognizable as well as his iconic designs for the Undefeated sneaker brand.<br /><br />While forging a career as a designer and after a stint a Prologue in LA, Adam started experimenting with moving images and transitioned into commercial filmmaking and music videos. He was nominated for MVPA awards and Saatchi &amp; Saatchi''s Best New Directors Showcase and directed videos for many fantastic bands like Beck, The National, Interpol, Regina Spector and Tortoise. Adam&rsquo;s design background and thorough creative leadership contributed to his success as an award winning commercial director for some of the world best brands including Nike, Ford, Verizon, The New York Times, Toshiba, Rock Band, o2, History Channel, a&amp;e and many others.', '', 2, 'adam-levite', '2015-01-29 15:13:13', '2015-01-27 15:19:24');

-- --------------------------------------------------------

--
-- Table structure for table `artists_offices`
--

CREATE TABLE `artists_offices` (
  `id` char(36) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `artist_id` (`artist_id`),
  KEY `office_id` (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `artists_offices`
--

INSERT INTO `artists_offices` (`id`, `artist_id`, `office_id`) VALUES
('541ad87b-7da4-4b6a-ad79-04356631c4ed', 1, 1),
('54c6aed6-c8e8-4f8f-83fa-357d6631c4ed', 2, 1),
('54c7ac7c-52f8-4454-b108-02f46631c4ed', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `artist_reels`
--

CREATE TABLE `artist_reels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) NOT NULL,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `vimeo_link` varchar(250) NOT NULL,
  `vimeo_image` varchar(250) NOT NULL,
  `order` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `artist_id` (`artist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `artist_reels`
--

INSERT INTO `artist_reels` (`id`, `artist_id`, `name_eng`, `name_fre`, `vimeo_link`, `vimeo_image`, `order`, `modified`, `created`) VALUES
(1, 1, 'dfgdfgh', 'dfghdfgh', '67891150', 'http://i.vimeocdn.com/video/439973832_640.jpg', 1, '2014-07-30 15:47:41', '2014-07-30 15:46:27'),
(2, 1, 'sdfgsdfg', 'sdfgdsfg', '72681367', 'http://i.vimeocdn.com/video/446588089_640.jpg', 2, '2014-07-30 15:47:41', '2014-07-30 15:46:50'),
(4, 4, 'wert', 'wert', '82317888', 'http://i.vimeocdn.com/video/458675650_640.jpg', 0, '2014-08-01 11:28:41', '2014-08-01 11:28:41');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name_eng`, `name_fre`, `modified`, `created`) VALUES
(2, 'VFX', '', '2015-02-02 15:10:41', '2015-01-29 15:44:22'),
(3, 'Post', '', '2015-02-02 15:10:47', '2015-02-02 15:10:47');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` char(36) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`) VALUES
('54ca45ab-13b4-4d52-9330-088f6631c4ed', 'Adidas'),
('54ca45b4-b870-4942-ad73-046e6631c4ed', 'Cirque du Soleil'),
('54ca45c3-7f18-4af6-8b7a-08636631c4ed', 'Another long Client');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id`, `name_eng`, `name_fre`, `order`, `modified`, `created`) VALUES
(1, 'NYC', 'NYC', 0, '2015-01-26 21:12:16', '2014-09-10 20:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `other_pages`
--

CREATE TABLE `other_pages` (
  `id` char(36) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `content_text_eng` text NOT NULL,
  `content_text_fre` text NOT NULL,
  `home_headline_eng` varchar(250) NOT NULL,
  `home_headline_fre` varchar(250) NOT NULL,
  `home_mp4_video` varchar(250) NOT NULL,
  `home_ogv_video` varchar(250) NOT NULL,
  `home_background` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_pages`
--

INSERT INTO `other_pages` (`id`, `slug`, `content_text_eng`, `content_text_fre`, `home_headline_eng`, `home_headline_fre`, `home_mp4_video`, `home_ogv_video`, `home_background`) VALUES
('336a9e1c-32b2-11e4-8ee1-5733f73f56f2', 'interstate', '<table>\r\n<tbody>\r\n<tr>\r\n<td width="1000%"><span style="color: #ca0813;"><strong>INTERSTATE</strong> </span></td>\r\n</tr>\r\n<tr>\r\n<td>INTERSTATE is an inter-disciplinary director&rsquo;s group focused on the realization of great ideas across all platforms. With the ability to call on the many talents and resources available to us through our partnership with BLVD in Montreal (including immersive, interactive, editorial, VFX, original music and sound design), Interstate&rsquo;s directors and clients are empowered to collaborate and explore without restriction; to discover the best creative solutions without the limitations found in traditional production environments.</td>\r\n</tr>\r\n<tr>\r\n<td height="15%">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td><br /><span style="color: #ca0813;"> <strong>BLVD</strong> </span></td>\r\n</tr>\r\n<tr>\r\n<td>BLVD was born from the desire to offer bold, flexible and fully integrated digital services. It''s an all-in-one laboratory that combines creation, design and technology with exceptional artistic unity. Our multidisciplinary and forward-thinking team allows us to evolve freely within a wide range of technologies. We count on proximity, audacity, emotion and technical agility to create unique experiences.</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', '', '', '', '', ''),
('d4361b26-a7d0-11e4-b050-8791408e0a84', 'contact', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` char(36) NOT NULL,
  `client_id` char(36) NOT NULL,
  `agency_id` char(36) NOT NULL,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `picture` varchar(250) NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `mp4` varchar(250) NOT NULL,
  `ogv` varchar(250) NOT NULL,
  `headline_eng` varchar(250) NOT NULL,
  `headline_fre` varchar(250) NOT NULL,
  `date` varchar(250) NOT NULL,
  `description_eng` text NOT NULL,
  `description_fre` text NOT NULL,
  `awards_eng` text NOT NULL,
  `awards_fre` text NOT NULL,
  `credits_eng` text NOT NULL,
  `credits_fre` text NOT NULL,
  `vimeo_link_eng` varchar(250) NOT NULL,
  `vimeo_link_fre` varchar(250) NOT NULL,
  `vimeo_staff_pick` tinyint(1) NOT NULL DEFAULT '0',
  `new` tinyint(1) NOT NULL DEFAULT '0',
  `only_on_artists_page` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(250) NOT NULL,
  `isNews` tinyint(1) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `client_id` (`client_id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `client_id`, `agency_id`, `name_eng`, `name_fre`, `picture`, `thumbnail`, `mp4`, `ogv`, `headline_eng`, `headline_fre`, `date`, `description_eng`, `description_fre`, `awards_eng`, `awards_fre`, `credits_eng`, `credits_fre`, `vimeo_link_eng`, `vimeo_link_fre`, `vimeo_staff_pick`, `new`, `only_on_artists_page`, `slug`, `isNews`, `modified`, `created`) VALUES
('54ca4636-9a0c-436a-bc5f-045f6631c4ed', '54ca45c3-7f18-4af6-8b7a-08636631c4ed', '', 'Project Test with long name', '', 'fabc00f1d441d613c264d71ada7792f2.jpg', 'ae60c22832db4fb08bef66eef9ec6b5b.jpg', '', '', 'A long line for headline to check if everything is fine within the layout', '', '1422507600000', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce quis odio pretium, consectetur erat eu, faucibus massa. Morbi cursus nunc eu elit laoreet, sed maximus libero blandit. Morbi lacinia vitae libero ac porttitor. Donec pellentesque ultricies sem, non blandit ipsum aliquet eget. Nam sed ex in nisi consequat aliquet. Phasellus luctus enim nec nulla imperdiet posuere. Curabitur a lectus scelerisque, iaculis sapien a, auctor velit. Curabitur viverra pellentesque lectus, non volutpat dui eleifend id. Sed consectetur sagittis nisl non vehicula.</p>\r\n<p>Aenean ut pharetra turpis. Aenean condimentum nibh vitae ligula euismod, sed interdum nisi pulvinar. Suspendisse eu sagittis est. Praesent porta purus eu libero vestibulum eleifend. Vestibulum magna risus, tristique in mattis auctor, efficitur a eros. Integer mauris tellus, sodales sed elit sit amet, accumsan scelerisque tortor. Suspendisse eu arcu sed lacus lacinia volutpat a ut orci. Nullam a velit molestie, viverra velit lacinia, vehicula felis.</p>\r\n<p>Etiam dictum congue dictum. Integer id est at leo pellentesque semper. Proin sed nibh mauris. Suspendisse malesuada accumsan augue sed accumsan. Donec suscipit mauris eleifend, faucibus justo eget, pellentesque arcu. Donec ut pretium neque, sit amet consequat urna. Morbi sed suscipit ex, ut viverra ligula.</p>', '', '', '', '', '', '106621369', '', 1, 1, 0, 'project-test-with-long-name', 0, '2015-02-02 19:09:29', '2015-01-29 14:39:50'),
('54ca472c-6f14-4820-852c-06dd6631c4ed', '54ca45ab-13b4-4d52-9330-088f6631c4ed', '', 'Shoes 2015', '', '17748c013902718db8cf6f294e578fa0.jpg', 'a60081e923dcfd94bcc74b3e4d2f04f8.jpg', '', '', 'Yeah !!!! This Rocks !', '', '1422507600000', '<p>Ut quis tincidunt risus, non sollicitudin sapien. Sed ac fringilla neque. Sed enim nibh, sodales sit amet euismod eu, viverra sit amet purus. Integer accumsan mauris hendrerit placerat vestibulum. Praesent consequat sed nunc non ullamcorper. Maecenas mollis lectus eget metus rutrum vehicula. Phasellus dapibus varius lobortis. Ut pulvinar ipsum quam, nec mattis enim commodo eu. Ut ut placerat nulla. Sed blandit ultrices mi. Aliquam ac gravida arcu. Vivamus pretium leo et mauris laoreet, sit amet imperdiet elit porta.</p>\r\n<p>Integer consequat velit non tempus porta. Phasellus suscipit, lorem quis sodales efficitur, ante urna semper purus, in aliquet orci arcu at ex. Proin elementum magna id augue aliquam porta. Curabitur volutpat ex a odio condimentum, a efficitur diam sagittis. Curabitur quis pulvinar nisi, eu mattis mi. Nunc convallis dapibus urna, in commodo eros mattis lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras sit amet lorem vel dolor rutrum maximus. Cras mollis fermentum sagittis. Donec pellentesque sodales arcu sed tempor. Mauris quam lectus, tempus vel ipsum vitae, malesuada tincidunt orci.</p>', '', '', '', 'Animation: <span style="color: #ca0813;">John Doe</span><br />Post : <span style="color: #ca0813;">Another Guy</span><br />Sound : <span style="color: #ca0813;">Another Girl</span><br /><br />Making of : <a href="https://www.youtube.com/watch?v=N46HFCiXmyc" target="_blank">Video on youtube</a>', '', '117047469', '', 0, 1, 0, 'shoes-2015', 0, '2015-02-02 19:06:31', '2015-01-29 14:43:56'),
('54ca49b2-083c-40e9-b752-088f6631c4ed', '54ca45b4-b870-4942-ad73-046e6631c4ed', '', 'Circus Neon Light ', '', '407c21355e975f7e9599bb6aad34a8b6.jpg', 'a554a54c5e5aa18904712f02cf56a89b.jpg', '', '', '', '', '1422507600000', '<p>Ut quis tincidunt risus, non sollicitudin sapien. Sed ac fringilla neque. Sed enim nibh, sodales sit amet euismod eu, viverra sit amet purus. Integer accumsan mauris hendrerit placerat vestibulum. Praesent consequat sed nunc non ullamcorper. Maecenas mollis lectus eget metus rutrum vehicula. Phasellus dapibus varius lobortis. Ut pulvinar ipsum quam, nec mattis enim commodo eu. Ut ut placerat nulla. Sed blandit ultrices mi. Aliquam ac gravida arcu. Vivamus pretium leo et mauris laoreet, sit amet imperdiet elit porta.</p>\r\n<p>Integer consequat velit non tempus porta. Phasellus suscipit, lorem quis sodales efficitur, ante urna semper purus, in aliquet orci arcu at ex. Proin elementum magna id augue aliquam porta. Curabitur volutpat ex a odio condimentum, a efficitur diam sagittis. Curabitur quis pulvinar nisi, eu mattis mi. Nunc convallis dapibus urna, in commodo eros mattis lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras sit amet lorem vel dolor rutrum maximus. Cras mollis fermentum sagittis. Donec pellentesque sodales arcu sed tempor. Mauris quam lectus, tempus vel ipsum vitae, malesuada tincidunt orci.</p>', '', '', '', '', '', '105438077', '', 0, 0, 0, 'circus-neon-light', 0, '2015-01-29 14:54:42', '2015-01-29 14:54:42'),
('54ca4a0a-b978-45de-a1d2-046c6631c4ed', '', '', 'Another Project to test layout', '', '6360f52213e52f882c5386d4b79de745.jpg', '2f7b12dc45b3341496418878d5ccf125.jpg', '', '', 'Another headline', '', '1422507600000', '<p>Ut quis tincidunt risus, non sollicitudin sapien. Sed ac fringilla neque. Sed enim nibh, sodales sit amet euismod eu, viverra sit amet purus. Integer accumsan mauris hendrerit placerat vestibulum. Praesent consequat sed nunc non ullamcorper. Maecenas mollis lectus eget metus rutrum vehicula. Phasellus dapibus varius lobortis. Ut pulvinar ipsum quam, nec mattis enim commodo eu. Ut ut placerat nulla. Sed blandit ultrices mi. Aliquam ac gravida arcu. Vivamus pretium leo et mauris laoreet, sit amet imperdiet elit porta.</p>\r\n<p>Integer consequat velit non tempus porta. Phasellus suscipit, lorem quis sodales efficitur, ante urna semper purus, in aliquet orci arcu at ex. Proin elementum magna id augue aliquam porta. Curabitur volutpat ex a odio condimentum, a efficitur diam sagittis. Curabitur quis pulvinar nisi, eu mattis mi. Nunc convallis dapibus urna, in commodo eros mattis lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras sit amet lorem vel dolor rutrum maximus. Cras mollis fermentum sagittis. Donec pellentesque sodales arcu sed tempor. Mauris quam lectus, tempus vel ipsum vitae, malesuada tincidunt orci.</p>', '', '', '', '', '', '117047469', '', 0, 0, 0, 'another-project-to-test-layout', 0, '2015-01-29 14:57:11', '2015-01-29 14:56:10'),
('54ca4a96-f28c-4f6f-9d1e-045f6631c4ed', '54ca45b4-b870-4942-ad73-046e6631c4ed', '', 'Beautiful', '', 'd20211065de7edb2795f5355a8a0914c.jpg', 'fae7f56e1c417d40690666256506d791.jpg', '', '', 'Headline 4 life', '', '1422507600000', '<p>Ut quis tincidunt risus, non sollicitudin sapien. Sed ac fringilla neque. Sed enim nibh, sodales sit amet euismod eu, viverra sit amet purus. Integer accumsan mauris hendrerit placerat vestibulum. Praesent consequat sed nunc non ullamcorper. Maecenas mollis lectus eget metus rutrum vehicula. Phasellus dapibus varius lobortis. Ut pulvinar ipsum quam, nec mattis enim commodo eu. Ut ut placerat nulla. Sed blandit ultrices mi. Aliquam ac gravida arcu. Vivamus pretium leo et mauris laoreet, sit amet imperdiet elit porta.</p>\r\n<p>Integer consequat velit non tempus porta. Phasellus suscipit, lorem quis sodales efficitur, ante urna semper purus, in aliquet orci arcu at ex. Proin elementum magna id augue aliquam porta. Curabitur volutpat ex a odio condimentum, a efficitur diam sagittis. Curabitur quis pulvinar nisi, eu mattis mi. Nunc convallis dapibus urna, in commodo eros mattis lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras sit amet lorem vel dolor rutrum maximus. Cras mollis fermentum sagittis. Donec pellentesque sodales arcu sed tempor. Mauris quam lectus, tempus vel ipsum vitae, malesuada tincidunt orci.</p>', '', '', '', '', '', '105438077', '', 0, 0, 0, 'beautiful', 0, '2015-01-29 14:58:30', '2015-01-29 14:58:30'),
('54ca4ad1-b4d0-4898-a723-0ad86631c4ed', '', '', 'Yihaaa', '', '594c32c7173028fe95f045b2f31c5519.jpg', '528c92b6b3300d8b9d5ea3cc757c631f.jpg', '', '', 'Storm Escaping !!!!!!', '', '1422507600000', '<p>Ut quis tincidunt risus, non sollicitudin sapien. Sed ac fringilla neque. Sed enim nibh, sodales sit amet euismod eu, viverra sit amet purus. Integer accumsan mauris hendrerit placerat vestibulum. Praesent consequat sed nunc non ullamcorper. Maecenas mollis lectus eget metus rutrum vehicula. Phasellus dapibus varius lobortis. Ut pulvinar ipsum quam, nec mattis enim commodo eu. Ut ut placerat nulla. Sed blandit ultrices mi. Aliquam ac gravida arcu. Vivamus pretium leo et mauris laoreet, sit amet imperdiet elit porta.</p>\r\n<p>Integer consequat velit non tempus porta. Phasellus suscipit, lorem quis sodales efficitur, ante urna semper purus, in aliquet orci arcu at ex. Proin elementum magna id augue aliquam porta. Curabitur volutpat ex a odio condimentum, a efficitur diam sagittis. Curabitur quis pulvinar nisi, eu mattis mi. Nunc convallis dapibus urna, in commodo eros mattis lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras sit amet lorem vel dolor rutrum maximus. Cras mollis fermentum sagittis. Donec pellentesque sodales arcu sed tempor. Mauris quam lectus, tempus vel ipsum vitae, malesuada tincidunt orci.</p>', '', '', '', '', '', '117047469', '', 0, 0, 0, 'yihaaa', 0, '2015-01-29 14:59:29', '2015-01-29 14:59:29'),
('54ca4b1b-f4ac-4b04-809c-06416631c4ed', '54ca45ab-13b4-4d52-9330-088f6631c4ed', '', 'Heaven Lights', '', '40a010961c0162c30266df010e26f16b.jpg', 'b94863ca1e1dcf0387b3cb556263e384.jpg', '', '', 'Yeah thats from heaven', '', '1422507600000', '<p>Ut quis tincidunt risus, non sollicitudin sapien. Sed ac fringilla neque. Sed enim nibh, sodales sit amet euismod eu, viverra sit amet purus. Integer accumsan mauris hendrerit placerat vestibulum. Praesent consequat sed nunc non ullamcorper. Maecenas mollis lectus eget metus rutrum vehicula. Phasellus dapibus varius lobortis. Ut pulvinar ipsum quam, nec mattis enim commodo eu. Ut ut placerat nulla. Sed blandit ultrices mi. Aliquam ac gravida arcu. Vivamus pretium leo et mauris laoreet, sit amet imperdiet elit porta.</p>\r\n<p>Integer consequat velit non tempus porta. Phasellus suscipit, lorem quis sodales efficitur, ante urna semper purus, in aliquet orci arcu at ex. Proin elementum magna id augue aliquam porta. Curabitur volutpat ex a odio condimentum, a efficitur diam sagittis. Curabitur quis pulvinar nisi, eu mattis mi. Nunc convallis dapibus urna, in commodo eros mattis lacinia. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras sit amet lorem vel dolor rutrum maximus. Cras mollis fermentum sagittis. Donec pellentesque sodales arcu sed tempor. Mauris quam lectus, tempus vel ipsum vitae, malesuada tincidunt orci.</p>', '', '', '', '', '', '105438077', '', 0, 0, 0, 'heaven-lights', 0, '2015-01-29 15:00:43', '2015-01-29 15:00:43');

-- --------------------------------------------------------

--
-- Table structure for table `projects_artists`
--

CREATE TABLE `projects_artists` (
  `id` char(36) NOT NULL,
  `project_id` char(36) NOT NULL,
  `artist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `artist_id` (`artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects_artists`
--

INSERT INTO `projects_artists` (`id`, `project_id`, `artist_id`) VALUES
('54ca4636-f8c4-4daf-8568-045f6631c4ed', '54ca4636-9a0c-436a-bc5f-045f6631c4ed', 1),
('54ca472c-6bac-412c-93c0-06dd6631c4ed', '54ca472c-6f14-4820-852c-06dd6631c4ed', 2),
('54ca49b2-faf0-43f1-a0d4-088f6631c4ed', '54ca49b2-083c-40e9-b752-088f6631c4ed', 3),
('54ca4a25-94ac-4381-bb12-04606631c4ed', '54ca4a0a-b978-45de-a1d2-046c6631c4ed', 3),
('54ca4a96-1d3c-4752-bd4a-045f6631c4ed', '54ca4a96-f28c-4f6f-9d1e-045f6631c4ed', 1),
('54ca4ad1-2abc-4b35-ba95-0ad86631c4ed', '54ca4ad1-b4d0-4898-a723-0ad86631c4ed', 3),
('54cf9391-aa04-48e9-bf60-08a06631c4ed', '54ca4636-9a0c-436a-bc5f-045f6631c4ed', 3);

-- --------------------------------------------------------

--
-- Table structure for table `projects_categories`
--

CREATE TABLE `projects_categories` (
  `id` char(36) NOT NULL,
  `project_id` char(36) NOT NULL,
  `category_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects_categories`
--

INSERT INTO `projects_categories` (`id`, `project_id`, `category_id`) VALUES
('54ca5563-2b20-48e0-95c2-09b56631c4ed', '54ca472c-6f14-4820-852c-06dd6631c4ed', '2'),
('54cf9391-a430-4169-a0b7-08a06631c4ed', '54ca4636-9a0c-436a-bc5f-045f6631c4ed', '2'),
('54cf9391-cadc-4ed2-9ab5-08a06631c4ed', '54ca4636-9a0c-436a-bc5f-045f6631c4ed', '3');

-- --------------------------------------------------------

--
-- Table structure for table `projects_offices`
--

CREATE TABLE `projects_offices` (
  `id` char(36) NOT NULL,
  `project_id` char(36) NOT NULL,
  `office_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `office_id` (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `project_images`
--

CREATE TABLE `project_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` char(36) NOT NULL,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `description_eng` text NOT NULL,
  `description_fre` text NOT NULL,
  `filename` varchar(250) NOT NULL,
  `size` varchar(10) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `picture` varchar(250) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `slug`, `username`, `password`, `picture`, `created`, `modified`) VALUES
('53d8f80d-f434-4c6e-92b7-06f96631c4ed', 'Sebastien Galarneau', 'sebastien-galarneau', 'sebgalarneau@gmail.com', '9c99575011e42820eeaacdc55258c727ddbb5045', '0fdba4f39dff7c6862c881f15b01761c.jpg', '2014-07-30 09:50:05', '2014-07-30 09:50:05'),
('53ee0884-f9e4-4628-811d-02fd6631c4ed', 'Admin', 'admin', 'info@boogiestudio.com', '19b5f1d28817eec7925939f187ad97530f4ee699', '', '2014-08-15 15:17:56', '2014-08-15 15:17:56'),
('53f73edb-3d28-4c9f-baf7-06346631c4ed', 'Jean-Maxime', 'jean-maxime', 'jeanmaximecouillard@gmail.com', '4478c4cb065ad9920132e141bae9f6b958c23914', '', '2014-08-22 13:00:11', '2014-08-22 13:00:11');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
