-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Sep 10, 2014 at 11:06 PM
-- Server version: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `blvd_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `agencies`
--

CREATE TABLE `agencies` (
  `id` char(36) NOT NULL,
  `name` varchar(250) NOT NULL,
  `modified` date NOT NULL,
  `created` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `agencies`
--

INSERT INTO `agencies` (`id`, `name`, `modified`, `created`) VALUES
('54105015-dc2c-4ef5-8d7b-026c6631c4ed', 'agence 1', '2014-09-10', '2014-09-10'),
('5410501b-d3d8-4bd3-94b3-026c6631c4ed', 'agence 2', '2014-09-10', '2014-09-10'),
('5410a96f-8790-415c-8dca-20c16631c4ed', '{AGENCY}', '2014-09-10', '2014-09-10');

-- --------------------------------------------------------

--
-- Table structure for table `artists`
--

CREATE TABLE `artists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description_eng` text NOT NULL,
  `description_fre` text NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(250) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `artists`
--

INSERT INTO `artists` (`id`, `name`, `description_eng`, `description_fre`, `order`, `slug`, `modified`, `created`) VALUES
(1, 'Thibaut Duverneix', 'asdf da fadsf', 'asf asdf asdf', 0, 'thibaut-duverneix', '2014-08-27 18:05:02', '2014-08-27 18:05:02'),
(2, '{DIRECTOR 1}', '', '', 0, 'director-1', '2014-09-10 20:16:52', '2014-09-10 20:16:52'),
(3, '{DIRECTOR 2}', '', '', 0, 'director-2', '2014-09-10 20:16:57', '2014-09-10 20:16:57');

-- --------------------------------------------------------

--
-- Table structure for table `artists_offices`
--

CREATE TABLE `artists_offices` (
  `id` char(36) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `office_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `artist_id` (`artist_id`),
  KEY `office_id` (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `artist_reels`
--

CREATE TABLE `artist_reels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) NOT NULL,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `vimeo_link` varchar(250) NOT NULL,
  `vimeo_image` varchar(250) NOT NULL,
  `order` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `artist_id` (`artist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `artist_reels`
--

INSERT INTO `artist_reels` (`id`, `artist_id`, `name_eng`, `name_fre`, `vimeo_link`, `vimeo_image`, `order`, `modified`, `created`) VALUES
(1, 1, 'dfgdfgh', 'dfghdfgh', '67891150', 'http://i.vimeocdn.com/video/439973832_640.jpg', 1, '2014-07-30 15:47:41', '2014-07-30 15:46:27'),
(2, 1, 'sdfgsdfg', 'sdfgdsfg', '72681367', 'http://i.vimeocdn.com/video/446588089_640.jpg', 2, '2014-07-30 15:47:41', '2014-07-30 15:46:50'),
(4, 4, 'wert', 'wert', '82317888', 'http://i.vimeocdn.com/video/458675650_640.jpg', 0, '2014-08-01 11:28:41', '2014-08-01 11:28:41');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name_eng`, `name_fre`, `modified`, `created`) VALUES
(1, 'Sound Design', 'Design sonore', '2014-08-25 15:00:56', '2014-08-25 15:00:56'),
(2, 'VFX', 'Effets', '2014-08-25 15:01:05', '2014-08-25 15:01:05'),
(3, '{CATEGORY 1}', '{CATEGORY 1}', '2014-09-10 20:15:53', '2014-09-10 19:42:59'),
(4, '{CATEGORY 2}', '{CATEGORY 2}', '2014-09-10 20:16:05', '2014-09-10 20:16:05');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` char(36) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`) VALUES
('01188c12-81dd-4448-872d-4b0f307e57ce', 'Dell'),
('052d6f36-40db-465d-97c3-cbdcb9774b8c', 'NPD'),
('1111f60a-8d1b-408b-8cb2-06c04c234333', 'Valentine'),
('14325628-4dd5-48c5-933d-68265e683fcb', 'RÃ©no-DÃ©pot'),
('1b41fe02-3f95-42f2-8ddd-9e94b2d27052', 'Desjardins'),
('28f94d0c-95f1-4e49-9782-b7f78aa6d6f2', 'SCQ'),
('381c5271-721b-440c-bf7c-5f77be6ed210', 'OACIQ'),
('396af156-63bc-4033-b135-9e6b4949f4db', 'Loto-QuÃ©bec'),
('3a30b93f-5829-4c71-9fe7-02b1f693768e', 'Patrick Watson'),
('3f9339f9-8d51-4675-ab7f-b9f97bf6c72d', 'Subway'),
('44da670b-f246-4fae-a324-977f714ab575', 'Ubisoft'),
('53b1ce0b-d660-422c-89c7-0535c09d4313', 'Nissan'),
('5410a98d-0be0-4d06-9289-20c06631c4ed', '{CLIENT}'),
('5477618c-7217-485b-ad4f-8335134c67ea', 'Canac'),
('54e3cc06-cca3-46ca-a1ba-7df443da9e01', 'BoogieStudio'),
('55048b81-8af1-4f1f-8083-5f2ceeb0df1a', 'ChÃ¢telaine'),
('5a79a2f4-bb19-4c47-82e4-dd12f5d68ef3', 'Bell'),
('5c07bb29-b5d5-41dd-a81d-915aaaeafdb1', 'Videotron'),
('5cab4da5-fcaa-42be-a3b1-c837ca7c96c5', 'Uber Communication'),
('5d6a4630-cb58-4024-92e8-c83d5c071825', 'Mastercard'),
('5ed558f5-27d7-4440-887a-1892a396fa7e', 'Harley Davidson'),
('620fd9c8-7bd7-46a5-9dce-93560085eee1', 'Unipneu'),
('6474bb9f-0da7-40bb-a487-370b3b6792c5', 'Parkinson society'),
('6484f8b4-94e9-4bcc-ac57-1900f758d520', 'Defacto'),
('68ce755b-495b-4fcd-b5e2-5768eae16161', 'Cascade'),
('69589f81-28bf-4f1a-810c-acd0b0a4f17b', 'Telus'),
('6bcabfc5-6591-455f-9189-094d9697e08b', 'Ordre des ingÃ©nieurs du QuÃ©bec'),
('772130f7-4e37-4259-93c2-e9ba5c030fb9', 'Armstrong Cheese'),
('8404e077-6319-4493-a893-7e26011b581d', 'RDS'),
('86854bee-80e4-40e5-985f-098887ee34e0', 'MSSS'),
('88ab7454-81e9-4421-be26-00a9459f9327', 'STM'),
('8a770dc9-b876-4f33-ba6b-98fc8d559b20', 'IGA'),
('8cab57a8-f1f7-403e-b0e0-cfe93bdef054', 'Dairy farmers Canada'),
('8d3783ed-e451-4f8f-b5cf-7cb371cbc5f5', 'Familiprix'),
('8fb80860-2911-4acf-9581-35e9660a972d', 'Fenplast'),
('90ef0440-89fb-4e4a-bfe7-12d80a0c3b6b', 'Coeur de Pirate'),
('97310b44-3bfd-4eb7-bdc4-11f98623b767', 'Jour de la Terre'),
('9a7712d6-ffb1-4f5e-85f3-760a3ad3a533', 'Redbull'),
('9af4ea5b-a825-44b5-bbb1-affcf6d02148', 'SAIL'),
('9e3e5cbb-74c7-434b-a2fa-681217e720ee', 'Accès Pharma'),
('a42941eb-3f9f-4afc-bf8c-478b8d2ddd38', 'FORD'),
('a64496b0-a260-4893-88a6-240d9b71bf0c', 'Reine des sauces'),
('ae3c0b44-a9fb-4611-a9f3-315c733f9097', 'CrÃ©a'),
('b50cde9c-e873-4b41-ba9e-916e27a2e98c', 'SAAQ'),
('ba9a9694-03a1-4813-9d97-bde849e3c2f1', 'Adidas'),
('bb033033-a0e0-48fd-9bc3-56f4306e6681', 'Artic Gardens'),
('bd85f5b8-5dfe-4330-8bbf-076bd9236aaa', 'SociÃ©tÃ© de sauvetage'),
('c0ccdeaf-2162-497e-83b4-391b2245ed09', 'New-Brunswick'),
('c234d970-b361-402c-89e6-a77ce7700d9d', 'Belairdirect'),
('c62e7f30-cace-48ba-a6f9-9a21925bf7f8', 'Benetton'),
('caa3a4bb-8663-4b0c-bd64-657f53fc8b21', 'General electric'),
('cba60cb1-0fd4-4384-9f05-c613d37ca5a4', 'Mondial de l''automobile'),
('d561e678-7ca6-4c30-9a05-0702f62ae03c', 'BMR'),
('d72f3b1b-0179-4e03-9c56-b91eaa27dbff', 'Alcan'),
('d80c67a4-d5c8-43d8-b902-9c2f47a2edb7', 'NeoStrata'),
('d927380f-bbc5-450d-857f-e8b63abb7142', 'Krispy Kernels'),
('dbc22c79-23bf-47d8-84a2-0d4a3db2baae', 'Napa'),
('e4ca7409-6a57-473b-92df-daef73bd2420', 'Suzuki'),
('e555f86d-4ff4-4159-aaa8-fa233602ef52', 'Ultramar'),
('f616af73-b53f-4ae3-8503-250977133eb6', 'Montreal'),
('f71d1bca-6230-4978-84c1-a502d88c3680', 'FIQ'),
('ffd86595-2d71-447c-98e1-8e719448f459', 'Cage aux Sports');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id`, `name_eng`, `name_fre`, `order`, `modified`, `created`) VALUES
(1, '{OFFICE 1}', '{OFFICE 1}', 0, '2014-09-10 20:15:10', '2014-09-10 20:15:10'),
(2, '{OFFICE 2}', '{OFFICE 2}', 0, '2014-09-10 20:15:19', '2014-09-10 20:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `other_pages`
--

CREATE TABLE `other_pages` (
  `id` char(36) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `content_text_eng` text NOT NULL,
  `content_text_fre` text NOT NULL,
  `home_headline_eng` varchar(250) NOT NULL,
  `home_headline_fre` varchar(250) NOT NULL,
  `home_mp4_video` varchar(250) NOT NULL,
  `home_ogv_video` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_pages`
--

INSERT INTO `other_pages` (`id`, `slug`, `content_text_eng`, `content_text_fre`, `home_headline_eng`, `home_headline_fre`, `home_mp4_video`, `home_ogv_video`) VALUES
('336a9e1c-32b2-11e4-8ee1-5733f73f56f2', 'blvd', 'dfgh dfgh dfgh dfgh <br />dfgh <br />d<span style="color: #ca0813;">f</span><br /><span style="color: #ca0813;">gh </span><br /><br /><span style="color: #ca0813;">&nbsp;dfgh</span>', 'dfg dfgh dfgh dfgh <span style="color: #ca0813;">fre fre fre</span>', '', '', '', ''),
('336aaab0-32b2-11e4-8ee1-5733f73f56f2', 'contact', '', '', '', '', '', ''),
('919b90ac-32aa-11e4-8ee1-5733f73f56f2', 'home', '', '', 'One stp wrkshp.', 'Un arret dans l''usine.', 'db45f4b605e424652c0bca2611059fc4.mp4', '885c00df8034e0d611a706d419a85696.ogv');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` char(36) NOT NULL,
  `client_id` char(36) NOT NULL,
  `agency_id` char(36) NOT NULL,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `picture` varchar(250) NOT NULL,
  `thumbnail` varchar(250) NOT NULL,
  `mp4` varchar(250) NOT NULL,
  `ogv` varchar(250) NOT NULL,
  `headline_eng` varchar(250) NOT NULL,
  `headline_fre` varchar(250) NOT NULL,
  `date` varchar(250) NOT NULL,
  `description_eng` text NOT NULL,
  `description_fre` text NOT NULL,
  `awards_eng` text NOT NULL,
  `awards_fre` text NOT NULL,
  `credits_eng` text NOT NULL,
  `credits_fre` text NOT NULL,
  `vimeo_link_eng` varchar(250) NOT NULL,
  `vimeo_link_fre` varchar(250) NOT NULL,
  `vimeo_staff_pick` tinyint(1) NOT NULL DEFAULT '0',
  `new` tinyint(1) NOT NULL DEFAULT '0',
  `only_on_artists_page` tinyint(1) NOT NULL DEFAULT '0',
  `slug` varchar(250) NOT NULL,
  `isNews` tinyint(1) NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `client_id` (`client_id`),
  KEY `agency_id` (`agency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `client_id`, `agency_id`, `name_eng`, `name_fre`, `picture`, `thumbnail`, `mp4`, `ogv`, `headline_eng`, `headline_fre`, `date`, `description_eng`, `description_fre`, `awards_eng`, `awards_fre`, `credits_eng`, `credits_fre`, `vimeo_link_eng`, `vimeo_link_fre`, `vimeo_staff_pick`, `new`, `only_on_artists_page`, `slug`, `isNews`, `modified`, `created`) VALUES
('53f743a6-8efc-4863-b06e-04016631c4ed', '', '', 'rttywt', 'tryww', '', 'e41ec1e61ecc9d5e5fe1001535f5e6e8.jpg', '', '', '', '', '1408680000000', 'wrywe wet r <a href="google.com">wet</a>', 'we rt<a href="google.com"> wet wr</a>', '', '', '', '', '', '', 0, 0, 0, 'rttywt', 1, '2014-08-29 20:40:29', '2014-08-22 13:20:38'),
('540dabbf-5dc4-4702-b8bf-03426631c4ed', 'ba9a9694-03a1-4813-9d97-bde849e3c2f1', '', 'Name', 'Nom', '7093fa594825afa6b510a78de7777247.jpg', 'e638a0e18c7dd359eb0cb9cc48cedd76.jpg', '08261e727f7a0e56d07d796a8bd7d559.mp4', '9ae83026d1e4478404e47b0db0ba878d.ogv', 'Headline Eng', 'Headline Fre', '1410148800000', 'Eng .laksdh fklash dflkashd flkashdf lkashjd flhsdal', 'Fre&nbsp; aksdhfkasdjhfl kasdhjfl kashdflkah sdflkhas dlfkjh alksjdfh', '', '', '', '', '72615472', '', 1, 1, 0, 'name', 0, '2014-09-08 13:59:41', '2014-09-08 13:14:39'),
('540daf27-4568-4f3c-acc7-03416631c4ed', 'c234d970-b361-402c-89e6-a77ce7700d9d', '', 'another name', 'un autre nom', '61e6e523a6de3892900417781003d586.jpg', '436e987a4b5dfb83e179dd988e49c636.jpg', '53b6b451650060ea7d4560e9fe96e176.mp4', 'fdf3a49048ae714abce102a2718cf71f.ogv', 'eng headlein', 'fre heallinge', '1410148800000', '', '', '', '', '', '', '72615472', '', 1, 1, 0, 'another-name', 0, '2014-09-08 14:00:01', '2014-09-08 13:29:11'),
('540f5ac1-7724-49c2-8a77-24556631c4ed', 'ba9a9694-03a1-4813-9d97-bde849e3c2f1', '', 'sdfgsg', 'fre sdfgsfd', '35106f33ab6bd17566183bb1dc4af720.jpg', 'b35415cb7e62925ea101ff15b20a0f98.jpg', '', '', '', '', '1410235200000', '', '', '', '', '', '', '90686414', '90686414', 0, 0, 0, 'sdfgsg', 0, '2014-09-10 13:33:23', '2014-09-09 19:53:37'),
('54104d34-6b30-4f7a-9e1d-02706631c4ed', '', '54105015-dc2c-4ef5-8d7b-026c6631c4ed', 'asdf', 'asdfasdfasdf', '25ca3b62ea8f41f17c88a772f2ebdaf9.jpg', '99e8c0670110f1a5aaf3c37ed24efbf8.jpg', '', '', '', '', '1410321600000', '', '', '', '', '', '', '', '', 0, 0, 0, 'asdf', 0, '2014-09-10 13:20:37', '2014-09-10 13:08:04'),
('5410ada2-5d64-4513-bce1-20c36631c4ed', '5410a98d-0be0-4d06-9289-20c06631c4ed', '5410a96f-8790-415c-8dca-20c16631c4ed', '{CAMPAIGN/PROJECT NAME}', 'nom du project', '150ba62ab3e8a34f2fd0c0e5b35599b4.jpg', '08402dfdb5cb3b72cc88987720fb2ae7.jpg', '', '', '{HEADLINE}', 'headline fre', '1410321600000', '{DESCRIPTION}<br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed metus magna, rhoncus vel tincidunt quis, bibendum in magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus nisl eu velit blandit pretium. Donec est orci, ullamcorper sit amet eros eu, interdum tempor velit. Nunc feugiat tempus nisl, nec mollis erat porttitor at. Phasellus ornare, turpis vitae fringilla vestibulum, lorem ante finibus urna, eget vehicula ex lectus at est.', '', '{AWARDS}<br />Lorem ipsum <span style="color: #ca0813;">dolor sit amet<br /></span>consectetur adipiscing <span style="color: #ca0813;">elit.<br /></span>Sed metus magna<span style="color: #ca0813;">, rhoncus vel tincidunt quis</span>', '', '{CREDITS}<br />Lorem ipsum <span style="color: #ca0813;">dolor sit amet<br /></span>consectetur adipiscing <span style="color: #ca0813;">elit.<br /></span>Sed metus magna<span style="color: #ca0813;">, rhoncus vel tincidunt quis</span>', '', '', '', 0, 0, 0, 'campaign-project-name', 0, '2014-09-10 20:17:15', '2014-09-10 19:59:30');

-- --------------------------------------------------------

--
-- Table structure for table `projects_artists`
--

CREATE TABLE `projects_artists` (
  `id` char(36) NOT NULL,
  `project_id` char(36) NOT NULL,
  `artist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `artist_id` (`artist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects_artists`
--

INSERT INTO `projects_artists` (`id`, `project_id`, `artist_id`) VALUES
('540dabbf-7a64-47ec-8559-03426631c4ed', '540dabbf-5dc4-4702-b8bf-03426631c4ed', 1),
('54104f75-ed18-46b4-b6f5-03246631c4ed', '54104d34-6b30-4f7a-9e1d-02706631c4ed', 1),
('5410b1cb-df04-474d-80ee-20c16631c4ed', '5410ada2-5d64-4513-bce1-20c36631c4ed', 2),
('5410b1cb-f674-43b8-be7e-20c16631c4ed', '5410ada2-5d64-4513-bce1-20c36631c4ed', 3);

-- --------------------------------------------------------

--
-- Table structure for table `projects_categories`
--

CREATE TABLE `projects_categories` (
  `id` char(36) NOT NULL,
  `project_id` char(36) NOT NULL,
  `category_id` char(36) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects_categories`
--

INSERT INTO `projects_categories` (`id`, `project_id`, `category_id`) VALUES
('5410aea9-8f74-442e-9734-20d46631c4ed', '5410ada2-5d64-4513-bce1-20c36631c4ed', '3'),
('5410b1a0-0848-4746-a1b8-20c06631c4ed', '5410ada2-5d64-4513-bce1-20c36631c4ed', '4');

-- --------------------------------------------------------

--
-- Table structure for table `projects_offices`
--

CREATE TABLE `projects_offices` (
  `id` char(36) NOT NULL,
  `project_id` char(36) NOT NULL,
  `office_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `office_id` (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `projects_offices`
--

INSERT INTO `projects_offices` (`id`, `project_id`, `office_id`) VALUES
('5410b16c-4938-4176-b2af-20c86631c4ed', '5410ada2-5d64-4513-bce1-20c36631c4ed', 1),
('5410b16c-6044-435b-b2cd-20c86631c4ed', '5410ada2-5d64-4513-bce1-20c36631c4ed', 2);

-- --------------------------------------------------------

--
-- Table structure for table `project_images`
--

CREATE TABLE `project_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` char(36) NOT NULL,
  `name_eng` varchar(250) NOT NULL,
  `name_fre` varchar(250) NOT NULL,
  `description_eng` text NOT NULL,
  `description_fre` text NOT NULL,
  `filename` varchar(250) NOT NULL,
  `size` varchar(10) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `project_images`
--

INSERT INTO `project_images` (`id`, `project_id`, `name_eng`, `name_fre`, `description_eng`, `description_fre`, `filename`, `size`, `order`, `created`) VALUES
(1, '5410ada2-5d64-4513-bce1-20c36631c4ed', '', '', '', '', 'b7591b0bb7e80f8d414e2ffe76fa0e06.jpg', 'square', 0, '2014-09-10 20:05:28'),
(2, '5410ada2-5d64-4513-bce1-20c36631c4ed', '', '', '', '', '666f1310668448a457eff52cbd6c0e4a.jpg', 'square', 0, '2014-09-10 20:05:58'),
(3, '5410ada2-5d64-4513-bce1-20c36631c4ed', '', '', '', '', '3fe1797896b1c4ba5d199cf3509c742e.jpg', 'wide', 0, '2014-09-10 20:06:42');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `picture` varchar(250) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `slug`, `username`, `password`, `picture`, `created`, `modified`) VALUES
('53d8f80d-f434-4c6e-92b7-06f96631c4ed', 'Sebastien Galarneau', 'sebastien-galarneau', 'sebgalarneau@gmail.com', '9c99575011e42820eeaacdc55258c727ddbb5045', '0fdba4f39dff7c6862c881f15b01761c.jpg', '2014-07-30 09:50:05', '2014-07-30 09:50:05'),
('53ee0884-f9e4-4628-811d-02fd6631c4ed', 'Admin', 'admin', 'info@boogiestudio.com', '19b5f1d28817eec7925939f187ad97530f4ee699', '', '2014-08-15 15:17:56', '2014-08-15 15:17:56'),
('53f73edb-3d28-4c9f-baf7-06346631c4ed', 'Jean-Maxime', 'jean-maxime', 'jeanmaximecouillard@gmail.com', '4478c4cb065ad9920132e141bae9f6b958c23914', '', '2014-08-22 13:00:11', '2014-08-22 13:00:11');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
