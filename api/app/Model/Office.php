<?php
App::uses('AppModel', 'Model');
/**
 * Office Model
 *
 * @property Artist $Artist
 * @property Project $Project
 */
class Office extends AppModel {

    public $actsAs = array(
        'Containable'
    );
    public $order = 'order';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name_eng' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name_fre' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Artist' => array(
			'className' => 'Artist',
			'joinTable' => 'artists_offices',
			'foreignKey' => 'office_id',
			'associationForeignKey' => 'artist_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => 'Artist.order ASC',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		),
		'Project' => array(
			'className' => 'Project',
			'joinTable' => 'projects_offices',
			'foreignKey' => 'office_id',
			'associationForeignKey' => 'project_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
