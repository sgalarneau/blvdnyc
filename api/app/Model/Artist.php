<?php
App::uses('AppModel', 'Model');
/**
 * Artist Model
 *
 * @property ArtistReel $ArtistReel
 */
class Artist extends AppModel {
    
    public $order = 'order';
    
    
    public $actsAs = array(
        'Containable',
        'Sluggable.Sluggable' => array(
            'field'     => 'name',  // Field that will be slugged
            'slug'      => 'slug',  // Field that will be used for the slug
            'lowercase' => true,    // Do we lowercase the slug ?
            'separator' => '-',     //
            'overwrite' => true    // Does the slug is auto generated when field is saved no matter what
        )
    );
    
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)/*,
		'description_eng' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'description_fre' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'order' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */

    public $hasAndBelongsToMany = array(
        'Project' => array(
            'className' => 'Project',
            'joinTable' => 'projects_artists',
            'foreignKey' => 'artist_id',
            'associationForeignKey' => 'project_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => 'Project.date DESC',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        ),
        'Office' => array(
            'className' => 'Office',
            'joinTable' => 'artists_offices',
            'foreignKey' => 'artist_id',
            'associationForeignKey' => 'office_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        )
    );

}
