<?php
App::uses('AppModel', 'Model');

class OtherPage extends AppModel {
    
     public $order = 'slug ASC';
     
     public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        
        $urlMP4 = Router::url('/files/',true);
        $this->virtualFields['mp4_url'] = sprintf(
            'CONCAT("%s",%s.home_mp4_video)',$urlMP4 ,$this->alias
        );
        $urlOGV = Router::url('/files/',true);
        $this->virtualFields['ogv_url'] = sprintf(
            'CONCAT("%s",%s.home_ogv_video)',$urlOGV ,$this->alias
        );
        
        $urlBackground = Router::url('/files/',true);
        $this->virtualFields['background_url'] = sprintf(
            'CONCAT("%s",%s.home_background)',$urlBackground ,$this->alias
        );
     }

}
