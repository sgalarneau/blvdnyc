<?php
App::uses('AppModel', 'Model');
/**
 * Project Model
 *
 * @property Client $Client
 */
class Project extends AppModel {
    
    
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        
        $urlThumbnail = Router::url('/img/p/',true);
        
        $this->virtualFields['thumbnail_url'] = sprintf(
            'CONCAT("%s",%s.thumbnail)',$urlThumbnail ,$this->alias
        );
        
        $urlBackground = Router::url('/img/p/background_',true);
        $this->virtualFields['background_url'] = sprintf(
            'CONCAT("%s",%s.picture)',$urlBackground ,$this->alias
        );
        
        $urlMP4 = Router::url('/img/p/',true);
        $this->virtualFields['mp4_url'] = sprintf(
            'CONCAT("%s",%s.mp4)',$urlMP4 ,$this->alias
        );
        
        $urlOGV = Router::url('/img/p/',true);
        $this->virtualFields['ogv_url'] = sprintf(
            'CONCAT("%s",%s.ogv)',$urlOGV ,$this->alias
        );
        
    }
    
    public $actsAs = array(
        'Containable',
        'Sluggable.Sluggable' => array(
            'field'     => 'name_eng',  // Field that will be slugged
            'slug'      => 'slug',  // Field that will be used for the slug
            'lowercase' => true,    // Do we lowercase the slug ?
            'separator' => '-',     //
            'overwrite' => true    // Does the slug is auto generated when field is saved no matter what
        ));
    
    public $order = "Project.date DESC";
    
    
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'client_id' => array(
			'uuid' => array(
				'rule' => array('uuid'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		// 'agency_id' => array(
            // 'uuid' => array(
                // 'rule' => array('uuid'),
                // //'message' => 'Your custom message here',
                // 'allowEmpty' => true,
                // //'required' => false,
                // //'last' => false, // Stop validation after this rule
                // //'on' => 'create', // Limit validation to 'create' or 'update' operations
            // ),
        // ),      
		'name_eng' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'slug_not_makeable'=>array(
    			'rule'    => array('slugUnique'),
                'message' => 'Project name must be unique.',
                'on' => 'create'
            )
		),
		// 'name_fre' => array(
			// 'notEmpty' => array(
				// 'rule' => array('notEmpty'),
				// //'message' => 'Your custom message here',
				// //'allowEmpty' => false,
				// //'required' => false,
				// //'last' => false, // Stop validation after this rule
				// //'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		// ),
        'headline_eng' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                //'message' => 'Your custom message here',
                'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        // 'healine_fre' => array(
            // 'notEmpty' => array(
                // 'rule' => array('notEmpty'),
                // //'message' => 'Your custom message here',
                // 'allowEmpty' => true,
                // //'required' => false,
                // //'last' => false, // Stop validation after this rule
                // //'on' => 'create', // Limit validation to 'create' or 'update' operations
            // ),
        // ),
		'description_eng' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		// 'description_fre' => array(
			// 'notEmpty' => array(
				// 'rule' => array('notEmpty'),
				// //'message' => 'Your custom message here',
				// 'allowEmpty' => true,
				// //'required' => false,
				// //'last' => false, // Stop validation after this rule
				// //'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		// ),
		'vimeo_link' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'vimeo_image' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
    
    
    
    public function slugUnique($check) {
        // $check will have value: array('promotion_code' => 'some-value')
        // $limit will have value: 25
        $value = array_values($check);
        $value = $value[0];
        
        
        $settings['separator'] = '-';
        $settings['length'] = '250';
        
        //slug it
        $string = strtolower($value);
        $string = preg_replace('/[^a-z0-9_]/i', $settings['separator'], $string);
        $string = preg_replace('/' . preg_quote($settings['separator']) . '[' . preg_quote($settings['separator']) . ']*/', $settings['separator'], $string);
        if (strlen($string) > $settings['length'])
        {
        $string = substr($string, 0, $settings['length']);
        }
        $string = preg_replace('/' . preg_quote($settings['separator']) . '$/', '', $string);
        $string = preg_replace('/^' . preg_quote($settings['separator']) . '/', '', $string);
        
        
        
        $exisintgSlug = $this->find('count', array(
            'conditions' => array("Project.slug"=>$string),
            'recursive' => -1
        ));
        
        if($exisintgSlug){
            return false;
        }else{
            return true;
        }
    }

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Client' => array(
			'className' => 'Client',
			'foreignKey' => 'client_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Agency' => array(
            'className' => 'Agency',
            'foreignKey' => 'agency_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);
    
    public $hasMany = array(
        'ProjectImage'=>  array(
            'order' => 'order ASC'
        )
    );
    
    public $hasAndBelongsToMany = array(
        'Category' => array(
            'className' => 'Category',
            'joinTable' => 'projects_categories',
            'foreignKey' => 'project_id',
            'associationForeignKey' => 'category_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        ),
        'Artist' => array(
            'className' => 'Artist',
            'joinTable' => 'projects_artists',
            'foreignKey' => 'project_id',
            'associationForeignKey' => 'artist_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        ),
        'Office' => array(
            'className' => 'Office',
            'joinTable' => 'projects_offices',
            'foreignKey' => 'project_id',
            'associationForeignKey' => 'office_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        )
    );
    
  
}
