<?php
App::uses('AppModel', 'Model');
/**
 * ProjectImage Model
 *
 * @property Project $Project
 */
class ProjectImage extends AppModel {

    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
              
        $urlSquareSmall = Router::url('/img/p/small_',true);
        $this->virtualFields['image_square_small'] = sprintf(
            'CONCAT("%s",%s.filename)',$urlSquareSmall ,$this->alias
        );
        $urlSquareLarge = Router::url('/img/p/large_',true);
        $this->virtualFields['image_square_large'] = sprintf(
            'CONCAT("%s",%s.filename)',$urlSquareLarge ,$this->alias
        );
        $urlSquareWide = Router::url('/img/p/',true);
        $this->virtualFields['image_wide'] = sprintf(
            'CONCAT("%s",%s.filename)',$urlSquareWide ,$this->alias
        );
                
    }


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Project' => array(
			'className' => 'Project',
			'foreignKey' => 'project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
