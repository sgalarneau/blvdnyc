<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 */
class User extends AppModel {
    
    /**
     * Model act as Sluggable for URL persistance
     */
    public $actsAs = array(
        'Sluggable.Sluggable' => array(
            'field'     => 'name',  // Field that will be slugged
            'slug'      => 'slug',  // Field that will be used for the slug
            'lowercase' => true,    // Do we lowercase the slug ?
            'separator' => '-',     //
            'overwrite' => true    // Does the slug is auto generated when field is saved no matter what
        )
    );
    
        
    /**
     * Hash password before save
     */    
    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['pwd'])) {  
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['pwd']
            );
        }
        return true;
    }
    public function validate_passwords() { //password match check
        return $this->data[$this->alias]['pwd'] === $this->data[$this->alias]['pwd_repeat'];
    }
   
    
    
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'username' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Must enter a valid email'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'pwd' => array(
            'length' => array(
                'rule'      => array('between', 5, 20),
                'message'   => 'Your password must be between 5 and 20 characters.',
                'on'        => 'create',  // we only need this validation on create
            ),
        ),
    
        // if we have a password entered, we need it to match pwd_repeat (both create and update)
        // we no longer need the length validation
        'pwd_repeat' => array(
            'compare' => array(
                'rule'    => array('validate_passwords'),
                'message' => 'Please confirm the password',
            ),
        )
	);
}
