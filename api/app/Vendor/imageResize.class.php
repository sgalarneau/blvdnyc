<?php
class SimpleImage {
   
   var $image;
   var $image_type;
 
   function load($filename) {
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      }
   }
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=100, $permissions=null) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
      }   
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image);
      }   
   }
   function getWidth() {
      return imagesx($this->image);
   }
   function getHeight() {
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100; 
      $this->resize($width,$height);
   }
   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;   
   }

	function saveThumbnail($path_filename,$width,$height,$quality=100,$zoom=0.7){	
		$ratio_orig = $this->getWidth()/$this->getHeight();
		
		if ($width/$height > $ratio_orig) {
	       $new_height = $width/$ratio_orig;
	       $new_width = $width;
	    } else {
	       $new_width = $height*$ratio_orig;
	       $new_height = $height;
	    }
		
		$cropPercent = $zoom;
		$cropWidth   = $this->getWidth()*$cropPercent;
		$cropHeight  = $this->getHeight()*$cropPercent;
		
		
		$c1 = array("x"=>($this->getWidth()-$cropWidth)/2, "y"=>($this->getHeight()-$cropHeight)/2);
		
		$thumb = imagecreatetruecolor($width, $height);
		imagecopyresampled($thumb, $this->image, 0, 0, $c1['x'], $c1['y'], $new_width, $new_height, $cropWidth, $cropHeight);
		imagejpeg($thumb,$path_filename,$quality);
		imagedestroy($thumb);
   }
    
    
   function crop($x1,$y1,$x2,$y2){
       $width = $x2-$x1;  
       $height = $y2-$y1;  
       $new_image = imagecreatetruecolor($width, $height);
       //imagecopyresampled ( $new_image , $this->image , 0 , 0 , $x1 , $y1 , $width , $height , $this->getWidth() , $this->getHeight());
       
       // imagecopyresampled() will take a rectangular area from 
       // src_image of width src_w and height src_h at position 
       // (src_x,src_y) and place it in a rectangular area of 
       // dst_image of width dst_w and height dst_h at position 
       // (dst_x,dst_y).
       
       //imagecopyresampled ( resource $dst_image , resource $src_image , int $dst_x , int $dst_y , int $src_x , int $src_y , int $dst_w , int $dst_h , int $src_w , int $src_h )
       
       //imagecopyresampled ( $new_image , $this->image , 0 , 0 , $x1 ,$y1 , $x2-$x1 , $y2-$y1 ,$this->getWidth(), $this->getHeight());
       imagecopyresampled($new_image, $this->image, 0, 0, $x1, $y1, $width, $height, $width, $height);
       
       $this->image = $new_image;       
   } 
    
}
?>