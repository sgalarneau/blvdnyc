BLVD website
=======

New BLVD website, launched september 18th 2014.

## External dependencies

- Facebook API (to open share dialog)
- Prerender.io (to render SEO friendly version of pages, config in .htaccess)

## Grunt tools

You must have `node.js` and npm `installed`. Terminal must be in the root directory (where `Gruntfile.js` is located).

### Download dependencies

Run the following command:

```
npm install
```

### Build CSS

Use the following command and make any change to any `less/*.less` file to rebuild `css/styles.css` and `css/styles.min.css`:

```
grunt watch
```

You may also use LiveReload plugin to make development easier : 

- [LiveReload for Firefox](https://addons.mozilla.org/en-US/firefox/addon/livereload/)
- [LiveReload for Chrome](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en)

### Minify JS and CSS

Use the following command :

```
grunt build
```

and these files will be rebuilt :

- `css/styles.min.css` (with its intermediary file `css/styles.css`)
- `js/blvd.min.js` (with its intermediary file `js/blvd.annotate.js`)
