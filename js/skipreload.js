function locationDecorator($rootScope, $delegate, $injector) {

	$delegate.skipReload = function() {
		skipping = true;

		var $route = $injector.get('$route');
		var $route = $injector.get('$route');
		var lastRoute = $route.current;

		var un = $rootScope.$on('$locationChangeSuccess', function(e, next, last) {
			$route.current = lastRoute;
			un();
		});
		return $delegate;
	};

	return $delegate;
}

angular.module('skipreload', []).config(['$provide', function($provide) {
	$provide.decorator('$location', locationDecorator);
}]);
