'use strict';

/* Controllers */

app.controller('BlvdCtrl', function($scope, $http, $sce, gettextCatalog, API) {

	// Load API
	$scope.api = new API("other_pages", "interstate");
	$scope.api.load();

});

app.controller('ContactCtrl', function($scope, $http, $sce, gettextCatalog, API) {

	//Load API
	$scope.api = new API("other_pages", "contact");
	$scope.api.load();
});

/*app.controller('HomeCtrl', function($scope, $rootScope, $http, $sce, gettextCatalog, API) {

	// Load API
	$scope.api = new API("projects");
	$scope.api.setParams({
		only_new : true
	});

	$scope.getType = function(item) {
		return item.hasOwnProperty('Project') ? "project" : "news";
	};

	$scope.isProject = function(item) {
		return item.hasOwnProperty('Project');
	};

	$scope.isNews = function(item) {
		return item.hasOwnProperty('News');
	};

	// On news click
	$scope.onNewsClick = function(news) {
		$scope.news = news;
	};

	// On news click
	$scope.onNewsClose = function(news) {
		$scope.news = null;
	};

	// On scroll down click
	$rootScope.onScrollDown = function() {
		$("html, body").animate({
			scrollTop : $("header").offset().top
		}, 500);
	};
	
	$rootScope.onToggleClick = function() {
		$("#main-menu").toggleClass("nav-collapse");
	};
	
	$rootScope.onMainNavClick = function(){
		$("#main-menu").addClass("nav-collapse");
	};
});*/

app.controller('ProjectCtrl', function($scope, $http, $routeParams, $sce, $location, $window, gettextCatalog, API) {

	// Load API
	$scope.api = new API("projects", $routeParams.id);
	$scope.api.load();

	// On vimeo play click
	$scope.onVimeoPlay = function(vimeoId) {
		$scope.vimeoId = vimeoId;
		$scope.currentProjectUrl = $sce.trustAsResourceUrl('//player.vimeo.com/video/' + vimeoId + '?byline=0&amp;title=0&amp;portrait=0&amp;badge=0&amp;color=ca0813&amp;autoplay=1" width="500" height="281" frameborder="0"');
	};

	// On vimeo close click
	$scope.onClose = function() {
		if ($scope.vimeoId != null) {
			$scope.vimeoId = null;
			$scope.currentProjectUrl = null;
			//$(".vimeo iframe").attr("src", "");
		} else {
			$("#project").fadeOut(150,function(){
				$window.history.back();
			});
			// $location.path("/projects");
			
		}
	};

	// Share facebook
	$scope.fbShare = function(slug) {
		var u = window.fullurl + "projects/" + slug;
		FB.ui({
			method : 'share',
			href : u,
		}, function(response) {
		});
	};

	// Share google plus
	$scope.gShare = function(slug) {
		var u = window.fullurl + "projects/" + slug;
		window.open("https://plus.google.com/share?url=" + u, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
		return false;
	};

	// Share google plus
	$scope.tShare = function(slug) {
		var u = window.fullurl + "projects/" + slug;
		window.open('http://twitter.com/share?url=' + u, 'twitterwindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 225) + ', left=' + $(window).width() / 2 + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
		return false;
	};
	
	
});

app.controller('ProjectListCtrl', function($scope, $http, $routeParams, $timeout, gettextCatalog, API) {

	// Load API
	$scope.api = new API("projects", $routeParams.id);
	$scope.api.setParams({
		limit : 12
	});
	$scope.api.load();
	//$scope.api.load("offices", "offices");
	$scope.api.load("categories", "categories");
	$scope.api.load("artists", "artists");

	// Filters array
	$scope.categories = [];
	$scope.artists = [];
	//$scope.offices = [];
	$scope.showFilters = false;

	// Debounce timeout
	var debounceTimeout;

	// On filter toggle click
	$scope.onFilterToogle = function(type, id) {
		
		$(".project").hide();
		// Manage arrays
		var index = -1;
		if (( index = $scope[type].indexOf(id)) == -1) {
			$scope[type].push(id);
		} else {
			$scope[type].splice(index, 1);
		}

		// Apply filter
		$scope.debounceFilter();
	};

	// On filter show toggle click
	$scope.onFiltersShowToggle = function() {
		$scope.showFilters = !$scope.showFilters;
	};

	// Check if filter is active (used in view)
	$scope.filterActive = function(type, id) {
		return jQuery.inArray(id, $scope[type]) > -1;
	};

	// Debounce function (to avoid multiple triggers in a too short time lapse)
	$scope.debounceFilter = function() {
		if (debounceTimeout) {
			clearTimeout(debounceTimeout);
		}
		debounceTimeout = setTimeout($scope.filter, 150);
	};

	// Apply filters
	$scope.filter = function() {

		// Create filters params to pass to the API
		var filters = {};
		filters["categories[]"] = $scope.categories;
		filters["artists[]"] = $scope.artists;
		//filters["offices[]"] = $scope.offices;


		$timeout(function() {
			$scope.api.clear();
			$scope.$apply();
			$scope.api.setFilters(filters);
			$scope.api.load();
		}, 150);


	};
});

app.controller('DirectorCtrl', function($scope, $http, $routeParams, $timeout, $location, gettextCatalog, API) {

	// Load a random director
	$scope.randomDirector = function(slug) {
		var randOffice = Math.floor(Math.random() * $scope.api.items.length);
		var randDirector = Math.floor(Math.random() * $scope.api.items[randOffice].Artist.length);
		if ($scope.api.items[randOffice].Artist.length > 0) {
			$scope.api.load("artist", "artists", $scope.api.items[randOffice].Artist[randDirector].slug);
		}
	};

	// Load API
	$scope.api = new API("offices");
	// $scope.api.load($scope.randomDirector);
	$scope.api.load();
	$scope.artistHide = true;

	// On director click
	$scope.onDirectorClick = function(slug) {

		var path = '/directors/' + slug;
		$location.path(path);
		$scope.loadDirector(slug);
		$location.skipReload().path(path);
		$location.skipReload().path(path).replace();
		history.pushState(null, null, path);
	};

	// Load director
	$scope.loadDirector = function(slug) {

		$timeout(function() {
			$scope.api.artist = null;
			$scope.artistHide = false;
			$scope.api.load("artist", "artists", slug, function() {
				$("html, body").animate({
					scrollTop : ($("#directors").offset().top + $("#directors").height() + 170)
				}, 600);
			});
		}, ($scope.artistHide ? 0 : 500));

		$scope.artistHide = true;
	};

	$scope.onCloseClick = function(slug) {
		$scope.artistHide = true;
		$timeout(function() {
			$scope.api.artist = null;
		}, 500);
	};

	if ($routeParams.id) {
		$scope.loadDirector($routeParams.id);
	}

});
