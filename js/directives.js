'use strict';

/**
 * Bind resize to an handling function. Keeps a div fullscreen. Not used anymore
 */
app.directive('resize', function($window, $rootScope) {

	return function($scope, element, attrs) {

		var w = angular.element($window);
		var c = attrs.controller;

		$scope.getWindowDimensions = function() {
			return {
				'h' : w.height(),
				'w' : w.width()
			};
		};

		$scope.$watch($scope.getWindowDimensions, function(newValue, oldValue) {
			$scope.windowHeight = newValue.h;
			$scope.windowWidth = newValue.w;
			$scope.rebuild();
		}, true);

		$scope.rebuild = function() {

			$(element).width($scope.windowWidth);

			if ($scope.controller == c) {
				$(element).height($scope.windowHeight);
			} else {
				$(element).height(0);
			}

		};

		w.bind('resize', function() {
			if (!$scope.$$phase) {
				$scope.$apply();
			} else {
				$scope.rebuild();
			}
		});

	};

});

/**
 * Bind resize to an handling function. This is used for the front video.
 */
app.directive('frontvideo', function($window, $rootScope) {

	return function($scope, element, attrs) {

		var isTouch = ('ontouchstart' in document.documentElement) ? true : false;
		var w = angular.element($window);
		var c = attrs.controller;
		var v = $("#frontvideo", element);
		var mp4 = $scope.api.home.home.mp4_url;
		var ogv = $scope.api.home.home.ogv_url;
		var jp = false;
		var bg = $scope.api.home.home.background_url;

		// Set bg
		$(element).css("background-image", 'url(' + bg + ')');

		var options = {
			ready : function() {
				$(this).jPlayer("setMedia", {
					m4v : mp4,
					ogv : ogv,
				});
			},
			canplaythrough : function() {
				$(this).addClass("jp-ready");
				$(this).jPlayer("play");
				jp = $("video", v);
				$scope.rebuild();
				//$("#animatedlogo").animateSprite("stopLoop");
			},
			loadedmetadata : function() {
				v.show();
			},
			loop : true,
			solution : "html",
			supplied : "m4v, ogv",
		};

		v.hide();

		if (!isTouch) {
			v.jPlayer(options);
		} else {
			//$("#animatedlogo").animateSprite("stopLoop");
		}

		// Animate logo
		/*$("#animatedlogo").animateSprite({
			columns : false,
			totalFrames : animData.length,
			duration : (animData.length) / 24 * 1000,
			loop : true,
			complete : function() {
				// alert("animation End");
			}
		});*/

		//$("#animatedlogo").animateSprite("setJSONmap", animData);

		$scope.getWindowDimensions = function() {
			return {
				'h' : w.height(),
				'w' : w.width()
			};
		};

		$scope.$watch($scope.getWindowDimensions, function(newValue, oldValue) {
			$scope.windowHeight = newValue.h;
			$scope.windowWidth = newValue.w;
			$scope.rebuild();
		}, true);

		$scope.rebuild = function() {

			// Scale container
			$(element).width($scope.windowWidth);
			if ($scope.controller == c) {
				$(element).height($scope.windowHeight);
				v.jPlayer("play");
			} else {
				$(element).height(0);
				v.jPlayer("pause");
			}

			// Scale video
			if (jp) {

				var wratio = $scope.windowHeight / $scope.windowWidth;
				var ratio = 9 / 16;

				// Scale the image
				if (wratio > ratio) {
					// Portrait
					jp.height($scope.windowHeight);
					jp.width($scope.windowHeight / ratio);
				} else {
					// Paysage
					jp.width($scope.windowWidth);
					jp.height($scope.windowWidth * ratio);
				}

				// Center the image
				jp.css('left', ($scope.windowWidth - jp.width()) / 2);
				jp.css('top', ($scope.windowHeight - jp.height()) / 2);

			} else {

				// $(element).height(0);
				v.hide();
			}
		};

		w.bind('resize', function() {
			if (!$scope.$$phase) {
				$scope.$apply();
			} else {
				$scope.rebuild();
			}
		});

	};

});

/**
 * This is an hack to "ungroup" staggered animations. Without it, two animations
 * are playing at the same time. It simply add the specified class with a
 * tiemout, forcing the staggering animation to group. Used on front page.
 */
app.directive('discreteclass', function($window, $rootScope, $timeout) {

	return {
		link : function($scope, element, attrs) {
			var c = attrs.discreteclass;
			$timeout(function() {
				element.addClass(c);
			});
		}
	};

});

app.directive('fadeoutscroll', function($window, $rootScope) {

	return function($scope, element, attrs) {

		var w = angular.element($window);
		var e = angular.element(element);
		var c = attrs.controller;

		$scope.lastBuildScroll = 0;

		var rebuildScroll = function() {

			$scope.scrollTop = w.scrollTop();
			$scope.positionTop = e.position().top;
			$scope.windowHeight = w.height();

			$scope.lastBuildScroll = $scope.scrollTop;
			var value = 1 - ($scope.scrollTop - $scope.positionTop + $scope.windowHeight) / $scope.windowHeight;

			$(element).css("opacity", value);
		};

		w.bind('scroll', function() {
			rebuildScroll();
		});
	};

});


app.directive('slideshow', function($window, $rootScope) {
	return function($scope, element, attrs) {
		if ($scope.$last){
			
			//$('#slideshow').removeData("flexslider");
			
			//$('#slideshow').get(0).removeAttribute('ng-repeat');
			$('#slideshow').flexslider({animation: "fade"});
		}
	};
});

/**
 * Animate menu in on scroll
 */
/*app.directive('menuin', function($window, $rootScope) {

	return function($scope, element, attrs) {

		var w = angular.element($window);
		var e = angular.element(element);
		var c = attrs.controller;

		var rebuildScroll = function() {

			if ($scope.shown == true) {
				return;
			}

			$scope.scrollTop = w.scrollTop();
			$scope.positionTop = e.position().top;
			$scope.shown = false;

			if ($rootScope.controller != "HomeCtrl" || $scope.scrollTop > $("#header").height()) {
				$(e).addClass("menuin");
				$scope.shown = true;
			}

		};

		w.bind('scroll', function() {
			rebuildScroll();
		});
	};

});*/

app.directive('mapify', function($window) {
	// directive link function
	var link = function($scope, element, attrs) {
		var map, infoWindow;
		var markers = [];

		var lat = attrs.mapifyLat;
		var lng = attrs.mapifyLng;

		var mapStyle = [{
			"stylers" : [{
				"hue" : "#000000"
			}, {
				"invert_lightness" : true
			}, {
				"saturation" : -100
			}, {
				"lightness" : 33
			}, {
				"gamma" : 0.5
			}]
		}, {
			"featureType" : "water",
			"elementType" : "geometry",
			"stylers" : [{
				"color" : "#333333"
			}]
		}, {
			"featureType" : "landscape",
			"elementType" : "geometry",
			"stylers" : [{
				"color" : "#252525"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry",
			"stylers" : [{
				"color" : "#000000"
			}]
		}];

		// map config
		var mapOptions = {
			center : new google.maps.LatLng(Number(Number(lat)+Number(0.001106)), lng),
			zoom : 15,
			mapTypeId : google.maps.MapTypeId.ROADMAP,
			styles : mapStyle,
			disableDefaultUI : true,
			scrollwheel : false,
		};

		if ($($window).width() < 768) {
			mapOptions.draggable = false;
		}
	
		// init the map
		function initMap() {
			if (map === void 0) {

				map = new google.maps.Map(element[0], mapOptions);
			}
			// console.log(map);
		}

		// place a marker
		function setMarker(map, position, title, content) {
			var marker;
			var markerOptions = {
				position : position,
				map : map,
				title : title,
				icon : window.basepath + 'images/pin.png'
			};

			marker = new google.maps.Marker(markerOptions);
			markers.push(marker); // add marker to array

			/*
			 * google.maps.event.addListener(marker, 'click', function() { //
			 * close window if not undefined if (infoWindow !== void 0) {
			 * infoWindow.close(); } // create new window var infoWindowOptions = {
			 * content : content }; infoWindow = new
			 * google.maps.InfoWindow(infoWindowOptions); infoWindow.open(map,
			 * marker); });
			 */
		}

		// show the map and place some markers
		initMap();
		setMarker(map, new google.maps.LatLng(lat, lng), '', '');
	};

	return {
		restrict : 'A',
		template : '<div id="gmaps"></div>',
		replace : true,
		link : link
	};
});
