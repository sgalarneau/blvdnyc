'use strict';

// http://scotch.io/tutorials/javascript/single-page-apps-with-angularjs-routing-and-templating

var modules = ['ngRoute', 'ngAnimate', 'ngSanitize', 'gettext', 'infinite-scroll', 'pmkr.partition', 'pmkr.filterStabilize', 'skipreload'];
var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

var app = angular.module('app', modules);
// var basepath = window.location.pathname;

// Avoid `console` errors in browsers that lack a console.
var console = (window.console = window.console || {});

app.config(function($routeProvider, $locationProvider) {

	// Enable hashbang and HTML4 mode
	$locationProvider.html5Mode(true).hashPrefix('!');
	// $locationProvider.hashPrefix('!');

	/*$routeProvider.when('/', {
		controller : 'HomeCtrl',
		templateUrl : basepath + 'views/home.html'
	});*/
	
	$routeProvider.when('/', {
		controller : 'ProjectListCtrl',
		templateUrl : basepath + 'views/projects_list.html'
	});

	/*$routeProvider.when('/projects', {
		controller : 'ProjectListCtrl',
		templateUrl : basepath + 'views/projects_list.html'
	});*/

	$routeProvider.when('/projects/:id', {
		controller : 'ProjectCtrl',
		templateUrl : basepath + 'views/project.html'
	});

	$routeProvider.when('/directors', {
		controller : 'DirectorCtrl',
		reloadOnSearch : false,
		templateUrl : basepath + 'views/directors.html'
	});

	$routeProvider.when('/directors/:id', {
		controller : 'DirectorCtrl',
		reloadOnSearch : false,
		templateUrl : basepath + 'views/directors.html'
	});

	$routeProvider.when('/interstate', {
		controller : 'BlvdCtrl',
		templateUrl : basepath + 'views/interstate.html'
	});

	$routeProvider.when('/contact', {
		controller : 'ContactCtrl',
		templateUrl : basepath + 'views/contact.html'
	});

	$routeProvider.otherwise({
		redirectTo : '/'
	});
});

// Launch application and some jQuery tricks
app.run(function(gettextCatalog, $rootScope, $window, $sce, $location, API) {

	// On change route start, scroll to top
	$rootScope.$on('$routeChangeStart', function(ev, data) {
		// Scroll to top
		$("html, body").animate({
			scrollTop : 0
		}, 500, 'easeInOutQuad', function() {
			$(window).trigger("scroll");
		});
	});

	// On change route end, trigger a few events
	$rootScope.$on('$routeChangeSuccess', function(ev, data) {
		// Pass the controller to the "template" and trigger resize
		if (data.$$route && data.$$route.controller) {
			$rootScope.controller = data.$$route.controller;
			$($window).trigger("resize");
			$($window).trigger("scroll");
		}
		$window.ga('send', 'pageview', {
			page : $location.path()
		});
	});

	// Weird thing to allow unsafe HTML with angular
	$rootScope.trustedHTML = function(html_code) {
		return $sce.trustAsHtml(html_code);
	};

	// Global function to handle dates
	$rootScope.formatDate = function(unixTimestamp) {
		var dt = new Date(parseInt(unixTimestamp));
		var day = dt.getDate();
		var month = months[dt.getMonth()];
		var translatedMonth = gettextCatalog.getString(month);
		var year = dt.getYear() + 1900;
		return day + " " + translatedMonth + " " + year;
	};

	// Simple function to use JS to change path
	$rootScope.go = function(path) {
		$location.path(path);
	};

	// Set language
	$rootScope.setLang = function(lang) {
		//gettextCatalog.setCurrentLanguage(lang);
		//$rootScope.lang = (lang == "fr") ? "_fre" : "_eng";
		gettextCatalog.setCurrentLanguage(lang);
		$rootScope.lang = "_eng";
	};

	// Switch language
	$rootScope.switchLang = function() {
		/*if ($rootScope.lang == "_fre") {
			$rootScope.setLang("en")
		} else {
			$rootScope.setLang("fr");
		}*/
	};

	// Default language
	//var userLang = (navigator.language || navigator.userLanguage).split("-");
	var userLang = "en";
	//var selectLang = userLang[0] == "en" ? "en" : "fr";
	$rootScope.setLang(userLang);

	// Fast click lib : this remove the 300ms delay on iOs
	FastClick.attach(document.body);

	$rootScope.basepath = window.basepath;
	// $rootScope.api = new API();
	// $rootScope.api.load("home", "other_pages", "home");

	// Responsive toggle
	/*$("#responsive-content .toggle").click(function() {
		$("#main-menu").toggleClass("nav-collapse");
	});
	$("#main-menu").on('click', 'a', function() {
		$("#main-menu").addClass("nav-collapse");
	});*/


	// Sticky header
	/*$(window).scroll(function() {
		var h = $("#header");
		if (h.length > 0) {
			if ($(this).scrollTop() > h.offset().top) {
				h.addClass("f-nav");
			} else {
				h.removeClass("f-nav");					
			}
		}
	});*/

	// Smooth show of footer on home page
	/*$(window).smartscroll(function() {
		var f = $("#footer");
		if ($(this).scrollTop() > $(window).height() / 2) {
			f.addClass("front-show");
		} else {
			f.removeClass("front-show");
		}
	});*/

	// List item video rollover
	$(document).on("mouseenter", ".video-hover", function(e) {

		$(this).addClass("hover")
		var mp4 = $(this).data("mp4");
		var ogv = $(this).data("ogv");

		var options = {
			ready : function() {
				$(this).jPlayer("setMedia", {
					m4v : mp4,
					ogv : ogv,
				});
			},
			canplay : function() {
				var el = $(this).parents('.project');
				el.addClass("jp-ready");
				if (el.hasClass("hover")) {
					$(this).jPlayer("play");
				}
			},
			loop : true,
			size : {
				width : "100%",
				height : "100%"
			},
			solution : "html", // Flash with an HTML5 fallback.
			supplied : "m4v, ogv",
		};

		var jp;
		if ($(".jp", this).length > 0) {
			jp = $(".jp", this).first();
			jp.jPlayer("play");
		} else {
			jp = $('<div class="jp"></div>');
			// $('a', this).prepend(jp);
			jp.insertAfter($('.project-image', this));
			jp.jPlayer(options);
		}

	}).on("mouseleave", ".video-hover", function(e) {

		$(this).removeClass("hover");

		// Stop video
		if ($(".jp", this).length > 0) {
			var jp = $(".jp", this).first();
			jp.jPlayer("pause");
		}

	});
	
	
	$rootScope.onToggleClick = function() {
		$("#main-menu").toggleClass("nav-collapse");
	};
	$rootScope.onMainNavClick = function(){
		$("#main-menu").addClass("nav-collapse");
	};
	
});

app.animation('.slide', function() {
	var NgHideClassName = 'ng-hide';
	return {
		beforeAddClass : function(element, className, done) {
			if (className === NgHideClassName) {
				jQuery(element).slideUp(done);
			}
		},
		removeClass : function(element, className, done) {
			if (className === NgHideClassName) {
				jQuery(element).hide().slideDown(done);
			}
		}
	}
});
