(function($) {

	// Store a reference to the original remove method.
	var originalMethod = $.fn.animateSprite;

	// var map = [];

	var methods = {

		frame : function(frameNumber) {
			// frame: number of the frame to be displayed
			// console.log( "frame id " + frameNumber, map[frameNumber]);
			var $this = $(this);
			var settings = $this.data('animateSprite').settings;
			var map = settings.map;
			var sizeFactor = 1;

			if (frameNumber >= map.length) frameNumber = 0;
			if (typeof map[frameNumber] == "undefined") console.log(frameNumber, map, this);

			if (frameNumber == 1 && settings.loop == false) originalMethod.apply(this, ["stopAnimation"]);

			this.each(function() {
				$this.css("background-position", (-map[frameNumber].x * sizeFactor) + 'px ' + (-map[frameNumber].y * sizeFactor) + 'px');
				$this.css('width', map[frameNumber].width * sizeFactor);
				$this.css('height', map[frameNumber].height * sizeFactor);
				$this.css('marginLeft', map[frameNumber].offsetX * sizeFactor);
				$this.css('marginTop', map[frameNumber].offsetY * sizeFactor);
			});
		},

		setJSONmap : function(data) {
			// frame: number of the frame to be displayed
			// map = data;
			originalMethod.apply(this, ["stopAnimation"]);
			$.extend($(this).data('animateSprite').settings, {
				map : data
			});
			methods.frame.apply(this, [0]);
		},

		stopLoop : function(data) {
			if ($(this).data('animateSprite')) {
				$(this).data('animateSprite').settings.loop = false;
			}
		},
	};

	// Define overriding method.
	$.fn.animateSprite = function() {

		// Override
		if (arguments[0] == "frame") {
			methods.frame.apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (arguments[0] == "resize") {
			methods.frame.apply(this, [0]);
		} else if (arguments[0] == "setJSONmap") {
			methods.setJSONmap.apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (arguments[0] == "stopLoop") {
			methods.stopLoop.apply(this, [0]);
		} else {
			originalMethod.apply(this, arguments);
		}
	}

})(jQuery);