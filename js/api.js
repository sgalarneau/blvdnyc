// API constructor function to encapsulate HTTP and pagination logic
app.factory('API', function($http, $timeout) {

	var API = function(contentType, id) {
		this.items = [];
		this.allProjects = [];
		this.busy = false;
		this.ended = false;
		this.page = 1;
		this.url = API.prototype.definePage(contentType, id);
		this.params = {};
		this.filters = {};
	};

	API.prototype.definePage = function(contentType, id) {

		if (contentType == "projects") {
			if (id == null) {
				return "projects/index.json";
			} else {
				return "projects/view/" + id + ".json";
			}
		} else if (contentType == "offices") {
			return "offices/index.json";
		} else if (contentType == "categories") {
			return "categories/index.json";
		} else if (contentType == "artists") {
			if (id == null) {
				return "artists/index.json";
			} else {
				return "artists/view/" + id + ".json";
			}
		} else if (contentType == "other_pages") {
			return "other_pages/view/" + id + ".json";
		}

	};

	/**
	 * Used to load main content (with infinite scroll capabilities)
	 */
	API.prototype.setFilters = function(filters) {
		this.ended = false;
		this.page = 1;
		this.filters = filters;
	};

	/**
	 * Used to load main content (with infinite scroll capabilities)
	 */
	API.prototype.setParams = function(params) {
		this.params = params;
	};

	/**
	 * Used to load main content (with infinite scroll capabilities)
	 */
	API.prototype.nextPage = function(callback) {

		if (this.busy || this.ended) {
			return;
		}

		this.busy = true;

		var params = $.extend({
			"page" : this.page,
			"limit" : 10
		}, this.params, this.filters);

		$http({
			method : 'GET',
			url : basepath + 'api/' + this.url,
			params : params,
			timeout : 1000,
		// traditional: true,
		}).success(function(data, status, headers, config) {
			
			if(this.url ==="projects/index.json" && this.allProjects.length === 0){
				
				var apiData = this;
								
				$http({
					method : 'GET',
					url : basepath + 'api/projects/index.json?only_new=true',
					params : params,
					timeout : 1000,
				// traditional: true,
				}).success(function(data2, status2, headers2, config2) {
					
					apiData.allProjects = apiData.items.concat(data2);
					if (Object.prototype.toString.call(data) == '[object Array]') {
						apiData.items = apiData.items.concat(data);
					} else {
						apiData.items = data;
					}
					// console.log(this);
					apiData.page++;
					apiData.busy = false;
					if (callback != null) callback();
					
				});
				
				
				
			}else{
				
				if (Object.prototype.toString.call(data) == '[object Array]') {
				this.items = this.items.concat(data);
				} else {
					this.items = data;
				}
				// console.log(this);
				this.page++;
				this.busy = false;
				if (callback != null) callback();
				
			}
			
			
		}.bind(this)).error(function(data, status, headers, config) {
			this.ended = true;
			this.busy = false;
		}.bind(this));

	};

	/**
	 * Used to load additional data, such as artists, studios or categories on
	 * projects list
	 */
	API.prototype.loadData = function(target, contentType, id, callback) {

		var url = this.definePage(contentType, id);
		
		$http({
			method : 'GET',
			url : basepath + 'api/' + url,
			params : {
				"page" : this.page,
				"limit" : 10
			},
		}).success(function(data, status, headers, config) {
			this[target] = data
			if (callback != null) callback();
		}.bind(this)).error(function(data, status, headers, config) {
			// Error
		}.bind(this));

	};

	API.prototype.load = function(target, contentType, id, callback) {
		
		if (typeof target == "undefined") {
			this.nextPage();
		} else if (typeof target == "function") {
			this.nextPage(target);
		} else {
			this.loadData(target, contentType, id, callback);
		}
	};

	API.prototype.clear = function() {
		this.ended = false;
		this.page = 1;
		this.items = [];
	};

	return API;
});