angular.module("gettext").run(['gettextCatalog', function(gettextCatalog) {
	gettextCatalog.setStrings('nl', {
		"Mark all as complete" : "Markeer als afgewerkt",
		"{{$count}} item left" : ["{{$count}} item te gaan", "{{$count}} items te gaan"],
		"All" : "Alle",
		"Active" : "Actief",
		"Completed" : "Afgewerkt",
		"Clear completed ({{completedCount}})" : "Wis afgewerkte taken ({{completedCount}})",
		"Double-click to edit a todo" : "Dubbelklik om een todo te bewerken",
		"What needs to be done?" : "Wat moet gedaan worden?"
	});
	gettextCatalog.setStrings('fr', {
		"Spaces" : "Espaces",
		"January" : "Janvier",
		"February" : "Février",
		"March" : "Mars",
		"April" : "Avril",
		"May" : "Mai",
		"June" : "Juin",
		"July" : "Juillet",
		"August" : "Août",
		"September" : "Septembre",
		"October" : "Octobre",
		"November" : "Novembre",
		"December" : "Décembre",
		"By category" : "Par catégories",
		"By artists" : "Par artistes",
		"By studios" : "Par studios",
		"Projects" : "Projets",
		"Directors" : "Réalisateurs",
		"Phone" : "Téléphone",
		"Email" : "Courriel",
		"United-States" : "États-Unis",
		"Tags&nbsp;:" : "Catégories&nbsp;:",
		"Awards&nbsp;:" : "Prix&nbsp;:",
		"Director&nbsp;:" : "Réalisateur&nbsp;:",
		"Filter the projects" : "Filtrer les projets",
		"Montreal Head Office": "Bureau Montréal",
		"New-York City Office" : "Bureau New-York",
		"Hispanic Markets" : "Marché hispanophone",
		"Quebec Advertising" : "Publicité Québec",
		"Gaming Industry" : "Industrie du jeu"
	});
}]);