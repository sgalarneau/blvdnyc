<?php

// Inspired by https://api.drupal.org/api/drupal/developer%21globals.php/global/base_path/7

// Define the variable as global
global $base_path, $base_url;

// Define is https is used
$is_https = isset ( $_SERVER ['HTTPS'] ) && strtolower ( $_SERVER ['HTTPS'] ) == 'on';

// Create base URL.
$http_protocol = $is_https ? 'https' : 'http';
$base_root = $http_protocol . '://' . $_SERVER ['HTTP_HOST'];

$base_url = $base_root;

// $_SERVER['SCRIPT_NAME'] can, in contrast to $_SERVER['PHP_SELF'], not
// be modified by a visitor.
if ($dir = rtrim ( dirname ( $_SERVER ['SCRIPT_NAME'] ), '\/' )) {
	$base_path = $dir;
	$base_url .= $base_path;
	$base_path .= '/';
} else {
	$base_path = '/';
}

// Function used to build path with basepath included
function url($path, $absolute = FALSE) {
	global $base_path, $base_url;
	return $absolute ? $base_url . $base_path . $path : $base_path . $path;
}

?>