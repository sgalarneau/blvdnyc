<?php

$metas = array(
	"title" => "Interstate",
	"description" => "Founded as the US manifestation of the Montreal-based integrated production company BLVD by Managing Partner Danny Rosenbloom (Psyop, BNS) and Creative Director Yann Mabille (The Mill, Mill+), INTERSTATE is an inter-disciplinary director’s group that is focused on exploring and implementing great ideas into exceptional experiences on any platform.
With the ability to call on the many talents and resources available through BLVD in Montreal (including immersive, interactive, editorial, VFX, original music and sound design), Interstate’s directors and clients are empowered to collaborate and explore without restriction; to discover the best creative solutions without limitations found in traditional production environments.",
	"image" => url("images/logo_share.jpg", TRUE),
	"vimeo" => "",
	"url" => url(arg(), TRUE),
);


// Add metatags for projects
if(arg(0) == "projects") {

	// Define API url
	$obj = getDataFromAPI("api/projects/view/" . arg(1) . ".json");

	if(!empty($obj)) {
		$metas["title"] = $obj["Project"]["name_eng"];
		$metas["description"] = $obj["Project"]["description_eng"];
		$metas["image"] = $obj["Project"]["thumbnail_url"];
		$metas["vimeo"] = !empty($obj["Project"]["vimeo_link_eng"]) ? $obj["Project"]["vimeo_link_eng"] : $obj["Project"]["vimeo_link_fre"];
	}
}

// Drupal like function to get argument pbt index
function arg($index = NULL) {

	global $base_path, $base_url;

	$path = $_SERVER["REQUEST_URI"];
	if(strpos($path, $base_path) == 0) {
		$path = substr($path, count($base_path));
	}

	$arguments = explode('/', $path);

	if(isset($arguments[$index])) {
		return $arguments[$index];
	} else {
		return implode('/', $arguments);
	}

}

// Function to curl data from API
function getDataFromAPI($url) {

	// Initiate curl
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, url($url, TRUE));

	// Execute
	$result = curl_exec($ch);

	// Closing
	curl_close($ch);

	// Create object
	return json_decode($result, true);
}
?>