<?php
	include 'inc/basepath.php'; 
	include_once 'inc/metabridge.php';
	$obj = getDataFromAPI("api/projects/index.json?limit=100000000");
    $artists = getDataFromAPI("api/artists/index.json");
	header("Content-Type: text/xml;charset=UTF-8;");  
?>
<?= '<?xml version="1.0" encoding="UTF-8"?>' ?>

<?= '<?xml-stylesheet type="text/xsl" href="' . url('sitemap.xsl', TRUE) . '"?>' ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<url><loc><?= url("", TRUE) ?></loc><changefreq>daily</changefreq><priority>1.0</priority></url>
<url><loc><?= url("directors", TRUE) ?></loc><changefreq>weekly</changefreq><priority>1.0</priority></url>
<url><loc><?= url("interstate", TRUE) ?></loc><changefreq>weekly</changefreq><priority>1.0</priority></url>
<url><loc><?= url("contact", TRUE) ?></loc><changefreq>weekly</changefreq><priority>1.0</priority></url>
<?php foreach ($artists as $key => $value) { ?>
<url><loc><?= url('directors/'. $value["Artist"]["slug"], TRUE) ?></loc><changefreq>weekly</changefreq><priority>0.5</priority></url>   
<?php } ?>
<?php foreach ($obj as $key => $value) { ?>
<url><loc><?= url('projects/'. $value["Project"]["slug"], TRUE) ?></loc><changefreq>weekly</changefreq><priority>0.5</priority></url>	
<?php } ?>

</urlset>
