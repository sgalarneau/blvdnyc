<?php
include 'inc/basepath.php';
include_once 'inc/metabridge.php';
define ( FORCE_MINIFIED, FALSE );
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link rel="shortcut icon" href="<?= url('favicon.ico', TRUE) ?>?v=2" type="image/vnd.microsoft.icon" />
<title>Interstate :: New York</title>
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, minimum-scale=1">
<meta name="description" content="<?= $metas["description"] ?>" />
<meta name="keywords" content="keywords" />
<meta property="og:site_name" content="Interstate" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?= $metas["title"] ?>" />
<meta property="og:image" content="<?= $metas["image"] ?>" />
<? if(!empty($metas["vimeo"])): ?>
<meta property="og:video" content="http://vimeo.com/moogaloop.swf?clip_id=<?= $metas["vimeo"] ?>&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=1&amp;color=00ADEF&amp;fullscreen=1&amp;autoplay=0&amp;loop=0">
<meta property="og:video:height" content="360" />
<meta property="og:video:width" content="640" />
<meta property="og:video:type" content="application/x-shockwave-flash" />
<? endif ?>
<meta property="og:url" content="<?= $metas["url"] ?>" />
<meta name="fragment" content="!">
<base href="<?= $base_path ?>">
<? if(strrpos($_SERVER["HTTP_HOST"], 'localhost') === FALSE || FORCE_MINIFIED) : ?>
<link rel="stylesheet" href="<?= url("css/styles.min.css") ?>">
<? else : ?>
<link rel="stylesheet" href="<?= url("css/styles.css") ?>">
<? endif; ?>
<script type="text/javascript">
	window.basepath = "<?= $base_path ?>";
	window.fullurl = "<?= url("", TRUE) ?>";
</script>
<script src="<?= url("js/vendor/jquery-2.1.1.min.js") ?>"></script>
<script src="<?= url("js/vendor/jquery.easing.1.3.js") ?>"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDk03SFjFLtCpYToFFebmpstk0z7xibB4U"></script>


<script type="text/javascript" src="<?= url("js/vendor/jquery.flexslider.js") ?>"></script>
<link rel="stylesheet" href="<?= url("css/flexslider.css") ?>">
<link rel="stylesheet" href="<?= url("css/flexslider.custom.css") ?>">

<script src="<?= url("js/vendor/jquery.jplayer.min.js") ?>"></script>
<script src="<?= url("js/vendor/angular/angular.min.js") ?>"></script>
<script src="<?= url("js/vendor/angular/angular-route.min.js") ?>"></script>
<script src="<?= url("js/vendor/angular/angular-animate.min.js") ?>"></script>
<script src="<?= url("js/vendor/angular/angular-sanitize.min.js") ?>"></script>
<script src="<?= url("js/vendor/angular-gettext.min.js") ?>"></script>
<script src="<?= url("js/vendor/ng-infinite-scroll.min.js") ?>"></script>
<? if(strrpos($_SERVER["HTTP_HOST"], 'localhost') === FALSE || FORCE_MINIFIED) : ?>
<script src="<?= url("js/blvd.min.js") ?>"></script>
<? else : ?>
<script src="<?= url("js/vendor/filter-stabilize.js") ?>"></script>
<script src="<?= url("js/vendor/partition.js") ?>"></script>
<script src="<?= url("js/vendor/jquery.animateSprite.min.js") ?>"></script>
<script src="<?= url("js/vendor/jquery.animateSprite.jm.js") ?>"></script>
<script src="<?= url("js/vendor/fastclick.js") ?>"></script>
<script src="<?= url("js/debounce.js") ?>"></script>
<script src="<?= url("js/animdata.js") ?>"></script>
<script src="<?= url("js/skipreload.js") ?>"></script>
<script src="<?= url("js/app.js") ?>"></script>
<script src="<?= url("js/api.js") ?>"></script>
<script src="<?= url("js/controllers.js") ?>"></script>
<script src="<?= url("js/directives.js") ?>"></script>
<script src="<?= url("js/translations.js") ?>"></script>
<? endif; ?>

<link rel="canonical" href="http://blvd-interstate.com" />
</head>
<body class="{{controller}}" ng-app="app">
	<div id="home-intro" class="home-only" controller="HomeCtrl" ng-if="api.home.home" frontvideo>
		<div id="frontvideo"></div>
		<div class="wrapper">
			<!--div id="animatedlogo" style="background-image: url('<?= url("images/loader_100.png')") ?>"></div-->
			<img class="blvd-logo" src="<?= url("images/Interstate_logo.png") ?>" />
			<!--div class="tagline">{{api.home.home["home_headline" + lang]}}</div-->
		</div>
		<div class="scrolldown" ng-click="onScrollDown()" fadeoutscroll>
			<div class="arrow">&nbsp;</div>
		</div>
	</div>
	<header class="header f-nav" id="header" role="banner" ng-if="controller">
		<div class="wrapper">
			<div class="container">
				<div id="responsive-content">
					<div class="logo">
						<a ng-click="onMainNavClick();" href="<?= url("") ?>" translate>
							<img src="<?= url("images/logo2.png") ?>" alt="Home" class="header__logo-image" />
						</a>
					</div>
					<div class="toggle" ng-click="onToggleClick()">Toggle</div>
				</div>
				<nav id="main-menu" class="nav-collapse menuin" role="navigation" tabindex="-1" menuin>
					<ul class="links inline clearfix">
						<li class="text-link first" ng-click="go('/')" ng-class="{'active' : controller == 'ProjectListCtrl' }">
							<div class="btn">
								<div class="linetop"></div>
								<a ng-click="onMainNavClick()" href="<?= url("") ?>" translate>Projects</a>
								<div class="linebottom"></div>
							</div>
						</li>
						<li class="text-link second" ng-click="go('/directors')" ng-class="{'active' : controller == 'DirectorCtrl' }">
							<div class="btn">
								<div class="linetop"></div>
								<a ng-click="onMainNavClick()" href="<?= url("#!/directors") ?>" translate>Directors</a>
								<div class="linebottom"></div>
							</div>
						</li>
						<li class="logo-link">
							<a ng-click="onMainNavClick();go('/')" href="<?= url("") ?>" translate>
								<img src="<?= url("images/logo2.png") ?>" alt="Home" class="header__logo-image" />
							</a>
						</li>
						<li ng-click="onMainNavClick()" class="text-link third" ng-click="go('/interstate')" ng-class="{'active' : controller == 'BlvdCtrl' }">
							<div class="btn">
								<div class="linetop"></div>
								<a href="<?= url("#!/interstate") ?>" translate>About</a>
								<div class="linebottom"></div>
							</div>
						</li>
						<li ng-click="onMainNavClick()" class="text-link last" ng-click="go('/contact')" ng-class="{'active' : controller == 'ContactCtrl' }">
							<div class="btn">
								<div class="linetop"></div>
								<a href="<?= url("#!/contact") ?>">Contact</a>
								<div class="linebottom"></div>
							</div>
						</li>
						<!--li ng-click="onMainNavClick()" class="text-link language" ng-click="switchLang()">
							<div class="btn">
								<span class="a">{{lang == "_eng" ? "fr" : "en"}}</span>
							</div>
						</li-->
					</ul>
				</nav>
			</div>
			<!--div id="language-switcher">
				<ul>
					<li ng-click="setLang('fr')" class="first">FR</li>
					<li ng-click="setLang('en')">EN</li>
				</ul>
			</div-->
		</div>
	</header>
	<div id="main">
		<div ng-view ng-cloak class="view ng-cloak" ng-animate-children></div>
	</div>
	<footer id="footer" class="front-show">
		
		<div id="footer-social">
			<a class="icon twitter" href="https://twitter.com/BLVDInterstate" target="_blank">Twitter</a>
			<a class="icon fb" href="https://www.facebook.com/blvdInterstate" target="_blank">Facebook</a>
			<!--a class="icon vimeo" href="https://vimeo.com/" target="_blank">Vimeo</a> -->
		</div>
	
	</footer>
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-59645924-1', 'auto');
      ga('send', 'pageview');
    
    </script>
	<div id="fb-root"></div>
	<script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1542994632626906',
          xfbml      : true,
          version    : 'v2.2'
        });
      };
    
      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    </script>
</body>
</html>